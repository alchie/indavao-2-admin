<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate_meta extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('realestate_meta');
        $this->load->model( array('Realestate_meta_model') );
        
        $this->template_data->set('main_page', 'realestate_meta' ); 
        $this->template_data->set('sub_page', 'realestate_meta' ); 
        $this->template_data->set('page_title', 'Meta Values' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 're_m_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Realestate_meta_model;
				$pagination = new $this->Realestate_meta_model;
				
				$list->setJoin('realestate_data realestate_data','realestate_meta.re_id = realestate_data.re_id');
				$list->setSelect('realestate_meta.*');
				$list->setSelect('realestate_data.re_title as re_title');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('realestate_data realestate_data','realestate_meta.re_id = realestate_data.re_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'realestate_meta',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Realestate_meta_model;
				$item->setReMId( $this->input->post('re_m_id'), true );

				$item->setJoin('realestate_data realestate_data','realestate_meta.re_id = realestate_data.re_id');
				$item->setSelect('realestate_meta.*');
				$item->setSelect('realestate_data.re_title as re_title');

				echo json_encode( array(
							'id' => $this->input->post('re_m_id'),
							'table' => 'realestate_meta',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_m_id'),
							'table' => 'realestate_meta',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Realestate_meta_model;
				$item->setReMId( $this->input->post('re_m_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByReMId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_meta ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_meta ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_m_id'),
							'table' => 'realestate_meta',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Realestate_meta_model->setReMId( $this->input->post('re_m_id') );
				$data = $this->Realestate_meta_model->getByReMId();
		
				
				if( $this->Realestate_meta_model->deleteByReMId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_realestate_meta ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Realestate_meta_model;

					if( $container->insert() ) {
						$results['id'] = $container->getReMId();
						$results['results'] = $container->getByReMId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'realestate_meta',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('re_id', 'lang:realestate_meta_re_id', 'required');
			$this->form_validation->set_rules('meta_key', 'lang:realestate_meta_meta_key', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('re_m_id', 'lang:realestate_meta_re_m_id', 'required');
			$this->form_validation->set_rules('re_id', 'lang:realestate_meta_re_id', 'required');
			$this->form_validation->set_rules('meta_key', 'lang:realestate_meta_meta_key', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Realestate_meta_model;
			if( $this->input->post('re_m_id') !== FALSE ) {
				$container->setReMId( $this->input->post('re_m_id'), FALSE, TRUE );
			}

			if( $this->input->post('re_id') !== FALSE ) {
				$container->setReId( $this->input->post('re_id'), FALSE, TRUE );
			}

			if( $this->input->post('meta_key') !== FALSE ) {
				$container->setMetaKey( $this->input->post('meta_key'), FALSE, TRUE );
			}

			if( $this->input->post('meta_value') !== FALSE ) {
				$container->setMetaValue( $this->input->post('meta_value'), FALSE, TRUE );
			}

			if( $this->input->post('re_m_active') !== FALSE ) {
				$container->setReMActive( $this->input->post('re_m_active'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByReMId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('realestate_data realestate_data','realestate_meta.re_id = realestate_data.re_id');
				$container->setSelect('realestate_meta.*');
				$container->setSelect('realestate_data.re_title as re_title');


			$results['id'] = $container->getReMId();
			$results['results'] = $container->getByReMId();
		}

	    return $results;
	}
	
}
/* End of file realestate_meta.php */
/* Location: ./application/controllers/realestate_meta.php */
