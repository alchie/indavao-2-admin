<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate_data extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('realestate_data');
        $this->load->model( array('Realestate_data_model') );
        
        $this->template_data->set('main_page', 'content' ); 
        $this->template_data->set('sub_page', 'realestate_data' ); 
        $this->template_data->set('page_title', 'Real Estate' ); 

    }
    
    public function index()
	{
        $this->load->view('realestate_data', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 're_added';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Realestate_data_model;
				$pagination = new $this->Realestate_data_model;
				
				$list->setJoin('attributes attributes','realestate_data.re_type = attributes.attr_name');
				$list->setSelect('realestate_data.*');
				$list->setSelect('attributes.attr_label as attr_label');

				$list->setJoin('realestate_data realestate_data2','realestate_data.re_parent = realestate_data2.re_id');
				$list->setSelect('realestate_data.*');
				$list->setSelect('realestate_data2.re_title as re_title2');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('attributes attributes','realestate_data.re_type = attributes.attr_name');
				$pagination->setJoin('realestate_data realestate_data2','realestate_data.re_parent = realestate_data2.re_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'realestate_data',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Realestate_data_model;
				$item->setReId( $this->input->post('re_id'), true );

				$item->setJoin('attributes attributes','realestate_data.re_type = attributes.attr_name');
				$item->setSelect('realestate_data.*');
				$item->setSelect('attributes.attr_label as attr_label');

				$item->setJoin('realestate_data realestate_data2','realestate_data.re_parent = realestate_data2.re_id');
				$item->setSelect('realestate_data.*');
				$item->setSelect('realestate_data2.re_title as re_title2');

				echo json_encode( array(
							'id' => $this->input->post('re_id'),
							'table' => 'realestate_data',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_data ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_id'),
							'table' => 'realestate_data',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Realestate_data_model;
				$item->setReId( $this->input->post('re_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByReId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_data ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_data ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_data ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_id'),
							'table' => 'realestate_data',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Realestate_data_model->setReId( $this->input->post('re_id') );
				$data = $this->Realestate_data_model->getByReId();
		
				
				if( $this->Realestate_data_model->deleteByReId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_realestate_data ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Realestate_data_model;

					if( $container->insert() ) {
						$results['id'] = $container->getReId();
						$results['results'] = $container->getByReId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'realestate_data',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('re_title', 'lang:realestate_data_re_title', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('re_id', 'lang:realestate_data_re_id', 'required');
			$this->form_validation->set_rules('re_title', 'lang:realestate_data_re_title', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Realestate_data_model;
			if( $this->input->post('re_id') !== FALSE ) {
				$container->setReId( $this->input->post('re_id'), FALSE, TRUE );
			}

			if( $this->input->post('re_title') !== FALSE ) {
				$container->setReTitle( $this->input->post('re_title'), FALSE, TRUE );
			}

			if( $this->input->post('re_slug') !== FALSE ) {
				$container->setReSlug( url_title( $this->input->post('re_slug'), '-', TRUE ), FALSE, TRUE );
				if($container->getReSlug() == '') {
					$container->setReSlug( url_title( $this->input->post('re_title') , '-', TRUE), FALSE, TRUE );
				}
			}

			if( $this->input->post('re_type') !== FALSE ) {
				$container->setReType( $this->input->post('re_type'), FALSE, TRUE );
			}

			if( $this->input->post('re_active') !== FALSE ) {
				$container->setReActive( $this->input->post('re_active'), FALSE, TRUE );
			}
			else {
				$container->setReActive( '0', FALSE, TRUE );
			}

			if( $this->input->post('re_added') !== FALSE ) {
				$container->setReAdded( $this->input->post('re_added'), FALSE, TRUE );
			}

			if( $this->input->post('re_parent') !== FALSE ) {
				$container->setReParent( $this->input->post('re_parent'), FALSE, TRUE );
			}

			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			if( $this->input->post('re_status') !== FALSE ) {
				$container->setReStatus( $this->input->post('re_status'), FALSE, TRUE );
			}
			else {
				$container->setReStatus( 'draft', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			
				if($container->getReSlug() == '') {
					$container->setReSlug( url_title( $this->input->post('re_title') , '-', TRUE), FALSE, TRUE );
				}

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByReId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('attributes attributes','realestate_data.re_type = attributes.attr_name');
				$container->setSelect('realestate_data.*');
				$container->setSelect('attributes.attr_label as attr_label');

				$container->setJoin('realestate_data realestate_data2','realestate_data.re_parent = realestate_data2.re_id');
				$container->setSelect('realestate_data.*');
				$container->setSelect('realestate_data2.re_title as re_title2');


			$results['id'] = $container->getReId();
			$results['results'] = $container->getByReId();
		}

	    return $results;
	}
	
}
/* End of file realestate_data.php */
/* Location: ./application/controllers/realestate_data.php */
