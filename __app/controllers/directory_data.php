<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory_data extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('directory_data');
        $this->load->model( array('Directory_data_model') );
        
        $this->template_data->set('main_page', 'content' ); 
        $this->template_data->set('sub_page', 'directory_data' ); 
        $this->template_data->set('page_title', 'Directory' ); 

    }
    
    public function index()
	{
        $this->load->view('directory_data', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'dir_name';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Directory_data_model;
				$pagination = new $this->Directory_data_model;
				
				$list->setJoin('users users','directory_data.user_id = users.user_id');
				$list->setSelect('directory_data.*');
				$list->setSelect('users.name as name');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('users users','directory_data.user_id = users.user_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'directory_data',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Directory_data_model;
				$item->setDirId( $this->input->post('dir_id'), true );

				$item->setJoin('users users','directory_data.user_id = users.user_id');
				$item->setSelect('directory_data.*');
				$item->setSelect('users.name as name');

				echo json_encode( array(
							'id' => $this->input->post('dir_id'),
							'table' => 'directory_data',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_directory_data ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_id'),
							'table' => 'directory_data',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Directory_data_model;
				$item->setDirId( $this->input->post('dir_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByDirId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_directory_data ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_directory_data ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_directory_data ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_id'),
							'table' => 'directory_data',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Directory_data_model->setDirId( $this->input->post('dir_id') );
				$data = $this->Directory_data_model->getByDirId();
		
				
				if( $this->Directory_data_model->deleteByDirId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_directory_data ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Directory_data_model;

					if( $container->insert() ) {
						$results['id'] = $container->getDirId();
						$results['results'] = $container->getByDirId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'directory_data',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('dir_name', 'lang:directory_data_dir_name', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('dir_id', 'lang:directory_data_dir_id', 'required');
			$this->form_validation->set_rules('dir_name', 'lang:directory_data_dir_name', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Directory_data_model;
			if( $this->input->post('dir_id') !== FALSE ) {
				$container->setDirId( $this->input->post('dir_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_name') !== FALSE ) {
				$container->setDirName( $this->input->post('dir_name'), FALSE, TRUE );
			}

			if( $this->input->post('dir_slug') !== FALSE ) {
				$container->setDirSlug( url_title( $this->input->post('dir_slug'), '-', TRUE ), FALSE, TRUE );
				if($container->getDirSlug() == '') {
					$container->setDirSlug( url_title( $this->input->post('dir_name') , '-', TRUE), FALSE, TRUE );
				}
			}

			if( $this->input->post('dir_branch') !== FALSE ) {
				$container->setDirBranch( $this->input->post('dir_branch'), FALSE, TRUE );
			}
			else {
				$container->setDirBranch( '0', FALSE, TRUE );
			}

			if( $this->input->post('dir_active') !== FALSE ) {
				$container->setDirActive( $this->input->post('dir_active'), FALSE, TRUE );
			}
			else {
				$container->setDirActive( '0', FALSE, TRUE );
			}

			if( $this->input->post('dir_verified') !== FALSE ) {
				$container->setDirVerified( $this->input->post('dir_verified'), FALSE, TRUE );
			}
			else {
				$container->setDirVerified( '0', FALSE, TRUE );
			}

			if( $this->input->post('dir_added') !== FALSE ) {
				$container->setDirAdded( $this->input->post('dir_added'), FALSE, TRUE );
			}

			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_status') !== FALSE ) {
				$container->setDirStatus( $this->input->post('dir_status'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			
				if($container->getDirSlug() == '') {
					$container->setDirSlug( url_title( $this->input->post('dir_name') , '-', TRUE), FALSE, TRUE );
				}

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByDirId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('users users','directory_data.user_id = users.user_id');
				$container->setSelect('directory_data.*');
				$container->setSelect('users.name as name');


			$results['id'] = $container->getDirId();
			$results['results'] = $container->getByDirId();
		}

	    return $results;
	}
	
}
/* End of file directory_data.php */
/* Location: ./application/controllers/directory_data.php */
