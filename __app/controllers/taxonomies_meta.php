<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taxonomies_meta extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('taxonomies_meta');
        $this->load->model( array('Taxonomies_meta_model') );
        
        $this->template_data->set('main_page', 'taxonomies_meta' ); 
        $this->template_data->set('sub_page', 'taxonomies_meta' ); 
        $this->template_data->set('page_title', 'Meta' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'meta_key';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Taxonomies_meta_model;
				$pagination = new $this->Taxonomies_meta_model;
				
				$list->setJoin('taxonomies taxonomies','taxonomies_meta.tax_id = taxonomies.tax_id');
				$list->setSelect('taxonomies_meta.*');
				$list->setSelect('taxonomies.tax_name as tax_name');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('taxonomies taxonomies','taxonomies_meta.tax_id = taxonomies.tax_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'taxonomies_meta',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Taxonomies_meta_model;
				$item->setTaxMId( $this->input->post('tax_m_id'), true );

				$item->setJoin('taxonomies taxonomies','taxonomies_meta.tax_id = taxonomies.tax_id');
				$item->setSelect('taxonomies_meta.*');
				$item->setSelect('taxonomies.tax_name as tax_name');

				echo json_encode( array(
							'id' => $this->input->post('tax_m_id'),
							'table' => 'taxonomies_meta',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tax_m_id'),
							'table' => 'taxonomies_meta',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Taxonomies_meta_model;
				$item->setTaxMId( $this->input->post('tax_m_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByTaxMId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies_meta ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies_meta ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tax_m_id'),
							'table' => 'taxonomies_meta',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Taxonomies_meta_model->setTaxMId( $this->input->post('tax_m_id') );
				$data = $this->Taxonomies_meta_model->getByTaxMId();
		
				
				if( $this->Taxonomies_meta_model->deleteByTaxMId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies_meta ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Taxonomies_meta_model;

					if( $container->insert() ) {
						$results['id'] = $container->getTaxMId();
						$results['results'] = $container->getByTaxMId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'taxonomies_meta',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('tax_id', 'lang:taxonomies_meta_tax_id', 'required');
			$this->form_validation->set_rules('meta_key', 'lang:taxonomies_meta_meta_key', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('tax_m_id', 'lang:taxonomies_meta_tax_m_id', 'required');
			$this->form_validation->set_rules('tax_id', 'lang:taxonomies_meta_tax_id', 'required');
			$this->form_validation->set_rules('meta_key', 'lang:taxonomies_meta_meta_key', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Taxonomies_meta_model;
			if( $this->input->post('tax_m_id') !== FALSE ) {
				$container->setTaxMId( $this->input->post('tax_m_id'), FALSE, TRUE );
			}

			if( $this->input->post('tax_id') !== FALSE ) {
				$container->setTaxId( $this->input->post('tax_id'), FALSE, TRUE );
			}

			if( $this->input->post('meta_key') !== FALSE ) {
				$container->setMetaKey( $this->input->post('meta_key'), FALSE, TRUE );
			}

			if( $this->input->post('meta_value') !== FALSE ) {
				$container->setMetaValue( $this->input->post('meta_value'), FALSE, TRUE );
			}

			if( $this->input->post('tax_m_active') !== FALSE ) {
				$container->setTaxMActive( $this->input->post('tax_m_active'), FALSE, TRUE );
			}
			else {
				$container->setTaxMActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByTaxMId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('taxonomies taxonomies','taxonomies_meta.tax_id = taxonomies.tax_id');
				$container->setSelect('taxonomies_meta.*');
				$container->setSelect('taxonomies.tax_name as tax_name');


			$results['id'] = $container->getTaxMId();
			$results['results'] = $container->getByTaxMId();
		}

	    return $results;
	}
	
}
/* End of file taxonomies_meta.php */
/* Location: ./application/controllers/taxonomies_meta.php */
