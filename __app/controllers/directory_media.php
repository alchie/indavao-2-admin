<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory_media extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('directory_media');
        $this->load->model( array('Directory_media_model') );
        
        $this->template_data->set('main_page', 'directory_media' ); 
        $this->template_data->set('sub_page', 'directory_media' ); 
        $this->template_data->set('page_title', 'Media' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'dir_med_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Directory_media_model;
				$pagination = new $this->Directory_media_model;
				
				$list->setJoin('directory_data directory_data','directory_media.dir_id = directory_data.dir_id');
				$list->setSelect('directory_media.*');
				$list->setSelect('directory_data.dir_name as dir_name');

				$list->setJoin('media_uploads media_uploads','directory_media.media_id = media_uploads.media_id');
				$list->setSelect('directory_media.*');
				$list->setSelect('media_uploads.file_name as file_name');

				$list->setJoin('media_uploads media_uploads2','directory_media.media_thumb = media_uploads2.media_id');
				$list->setSelect('directory_media.*');
				$list->setSelect('media_uploads2.file_name as file_name2');

				$list->setJoin('attributes attributes','directory_media.dir_med_group = attributes.attr_name');
				$list->setSelect('directory_media.*');
				$list->setSelect('attributes.attr_label as attr_label');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('directory_data directory_data','directory_media.dir_id = directory_data.dir_id');
				$pagination->setJoin('media_uploads media_uploads','directory_media.media_id = media_uploads.media_id');
				$pagination->setJoin('media_uploads media_uploads2','directory_media.media_thumb = media_uploads2.media_id');
				$pagination->setJoin('attributes attributes','directory_media.dir_med_group = attributes.attr_name');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'directory_media',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Directory_media_model;
				$item->setDirMedId( $this->input->post('dir_med_id'), true );

				$item->setJoin('directory_data directory_data','directory_media.dir_id = directory_data.dir_id');
				$item->setSelect('directory_media.*');
				$item->setSelect('directory_data.dir_name as dir_name');

				$item->setJoin('media_uploads media_uploads','directory_media.media_id = media_uploads.media_id');
				$item->setSelect('directory_media.*');
				$item->setSelect('media_uploads.file_name as file_name');

				$item->setJoin('media_uploads media_uploads2','directory_media.media_thumb = media_uploads2.media_id');
				$item->setSelect('directory_media.*');
				$item->setSelect('media_uploads2.file_name as file_name2');

				$item->setJoin('attributes attributes','directory_media.dir_med_group = attributes.attr_name');
				$item->setSelect('directory_media.*');
				$item->setSelect('attributes.attr_label as attr_label');

				echo json_encode( array(
							'id' => $this->input->post('dir_med_id'),
							'table' => 'directory_media',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_directory_media ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_med_id'),
							'table' => 'directory_media',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Directory_media_model;
				$item->setDirMedId( $this->input->post('dir_med_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByDirMedId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_directory_media ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_directory_media ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_directory_media ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_med_id'),
							'table' => 'directory_media',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Directory_media_model->setDirMedId( $this->input->post('dir_med_id') );
				$data = $this->Directory_media_model->getByDirMedId();
		
				
				if( $this->Directory_media_model->deleteByDirMedId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_directory_media ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Directory_media_model;

					if( $container->insert() ) {
						$results['id'] = $container->getDirMedId();
						$results['results'] = $container->getByDirMedId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'directory_media',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('dir_id', 'lang:directory_media_dir_id', 'required');
			$this->form_validation->set_rules('media_id', 'lang:directory_media_media_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('dir_med_id', 'lang:directory_media_dir_med_id', 'required');
			$this->form_validation->set_rules('dir_id', 'lang:directory_media_dir_id', 'required');
			$this->form_validation->set_rules('media_id', 'lang:directory_media_media_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Directory_media_model;
			if( $this->input->post('dir_med_id') !== FALSE ) {
				$container->setDirMedId( $this->input->post('dir_med_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_id') !== FALSE ) {
				$container->setDirId( $this->input->post('dir_id'), FALSE, TRUE );
			}

			if( $this->input->post('media_id') !== FALSE ) {
				$container->setMediaId( $this->input->post('media_id'), FALSE, TRUE );
			}

			if( $this->input->post('media_thumb') !== FALSE ) {
				$container->setMediaThumb( $this->input->post('media_thumb'), FALSE, TRUE );
			}

			if( $this->input->post('media_type') !== FALSE ) {
				$container->setMediaType( $this->input->post('media_type'), FALSE, TRUE );
			}

			if( $this->input->post('media_caption') !== FALSE ) {
				$container->setMediaCaption( $this->input->post('media_caption'), FALSE, TRUE );
			}

			if( $this->input->post('media_link') !== FALSE ) {
				$container->setMediaLink( $this->input->post('media_link'), FALSE, TRUE );
			}

			if( $this->input->post('media_name') !== FALSE ) {
				$container->setMediaName( $this->input->post('media_name'), FALSE, TRUE );
			}

			if( $this->input->post('dir_med_group') !== FALSE ) {
				$container->setDirMedGroup( $this->input->post('dir_med_group'), FALSE, TRUE );
			}

			if( $this->input->post('dir_med_order') !== FALSE ) {
				$container->setDirMedOrder( $this->input->post('dir_med_order'), FALSE, TRUE );
			}
			else {
				$container->setDirMedOrder( '0', FALSE, TRUE );
			}

			if( $this->input->post('dir_med_active') !== FALSE ) {
				$container->setDirMedActive( $this->input->post('dir_med_active'), FALSE, TRUE );
			}
			else {
				$container->setDirMedActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByDirMedId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('directory_data directory_data','directory_media.dir_id = directory_data.dir_id');
				$container->setSelect('directory_media.*');
				$container->setSelect('directory_data.dir_name as dir_name');

				$container->setJoin('media_uploads media_uploads','directory_media.media_id = media_uploads.media_id');
				$container->setSelect('directory_media.*');
				$container->setSelect('media_uploads.file_name as file_name');

				$container->setJoin('media_uploads media_uploads2','directory_media.media_thumb = media_uploads2.media_id');
				$container->setSelect('directory_media.*');
				$container->setSelect('media_uploads2.file_name as file_name2');

				$container->setJoin('attributes attributes','directory_media.dir_med_group = attributes.attr_name');
				$container->setSelect('directory_media.*');
				$container->setSelect('attributes.attr_label as attr_label');


			$results['id'] = $container->getDirMedId();
			$results['results'] = $container->getByDirMedId();
		}

	    return $results;
	}
	
}
/* End of file directory_media.php */
/* Location: ./application/controllers/directory_media.php */
