<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory_details extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('directory_details');
        $this->load->model( array('Directory_details_model') );
        
        $this->template_data->set('main_page', 'directory_details' ); 
        $this->template_data->set('sub_page', 'directory_details' ); 
        $this->template_data->set('page_title', 'Details' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'dir_d_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Directory_details_model;
				$pagination = new $this->Directory_details_model;
				
				$list->setJoin('directory_data directory_data','directory_details.dir_id = directory_data.dir_id');
				$list->setSelect('directory_details.*');
				$list->setSelect('directory_data.dir_name as dir_name');

				$list->setJoin('attributes attributes','directory_details.dir_d_key = attributes.attr_name');
				$list->setSelect('directory_details.*');
				$list->setSelect('attributes.attr_label as attr_label');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('directory_data directory_data','directory_details.dir_id = directory_data.dir_id');
				$pagination->setJoin('attributes attributes','directory_details.dir_d_key = attributes.attr_name');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'directory_details',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Directory_details_model;
				$item->setDirDId( $this->input->post('dir_d_id'), true );

				$item->setJoin('directory_data directory_data','directory_details.dir_id = directory_data.dir_id');
				$item->setSelect('directory_details.*');
				$item->setSelect('directory_data.dir_name as dir_name');

				$item->setJoin('attributes attributes','directory_details.dir_d_key = attributes.attr_name');
				$item->setSelect('directory_details.*');
				$item->setSelect('attributes.attr_label as attr_label');

				echo json_encode( array(
							'id' => $this->input->post('dir_d_id'),
							'table' => 'directory_details',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_directory_details ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_d_id'),
							'table' => 'directory_details',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Directory_details_model;
				$item->setDirDId( $this->input->post('dir_d_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByDirDId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_directory_details ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_directory_details ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_directory_details ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_d_id'),
							'table' => 'directory_details',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Directory_details_model->setDirDId( $this->input->post('dir_d_id') );
				$data = $this->Directory_details_model->getByDirDId();
		
				
				if( $this->Directory_details_model->deleteByDirDId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_directory_details ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Directory_details_model;

					if( $container->insert() ) {
						$results['id'] = $container->getDirDId();
						$results['results'] = $container->getByDirDId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'directory_details',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('dir_id', 'lang:directory_details_dir_id', 'required');
			$this->form_validation->set_rules('dir_d_key', 'lang:directory_details_dir_d_key', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('dir_d_id', 'lang:directory_details_dir_d_id', 'required');
			$this->form_validation->set_rules('dir_id', 'lang:directory_details_dir_id', 'required');
			$this->form_validation->set_rules('dir_d_key', 'lang:directory_details_dir_d_key', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Directory_details_model;
			if( $this->input->post('dir_d_id') !== FALSE ) {
				$container->setDirDId( $this->input->post('dir_d_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_id') !== FALSE ) {
				$container->setDirId( $this->input->post('dir_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_d_key') !== FALSE ) {
				$container->setDirDKey( $this->input->post('dir_d_key'), FALSE, TRUE );
			}

			if( $this->input->post('dir_d_value') !== FALSE ) {
				$container->setDirDValue( $this->input->post('dir_d_value'), FALSE, TRUE );
			}

			if( $this->input->post('dir_d_label') !== FALSE ) {
				$container->setDirDLabel( $this->input->post('dir_d_label'), FALSE, TRUE );
			}

			if( $this->input->post('dir_d_active') !== FALSE ) {
				$container->setDirDActive( $this->input->post('dir_d_active'), FALSE, TRUE );
			}
			else {
				$container->setDirDActive( '0', FALSE, TRUE );
			}

			if( $this->input->post('dir_d_order') !== FALSE ) {
				$container->setDirDOrder( $this->input->post('dir_d_order'), FALSE, TRUE );
			}
			else {
				$container->setDirDOrder( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByDirDId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('directory_data directory_data','directory_details.dir_id = directory_data.dir_id');
				$container->setSelect('directory_details.*');
				$container->setSelect('directory_data.dir_name as dir_name');

				$container->setJoin('attributes attributes','directory_details.dir_d_key = attributes.attr_name');
				$container->setSelect('directory_details.*');
				$container->setSelect('attributes.attr_label as attr_label');


			$results['id'] = $container->getDirDId();
			$results['results'] = $container->getByDirDId();
		}

	    return $results;
	}
	
}
/* End of file directory_details.php */
/* Location: ./application/controllers/directory_details.php */
