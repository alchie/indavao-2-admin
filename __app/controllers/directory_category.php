<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory_category extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('directory_category');
        $this->load->model( array('Directory_category_model') );
        
        $this->template_data->set('main_page', 'directory_category' ); 
        $this->template_data->set('sub_page', 'directory_category' ); 
        $this->template_data->set('page_title', 'Categories' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'dir_cat_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Directory_category_model;
				$pagination = new $this->Directory_category_model;
				
				$list->setJoin('directory_data directory_data','directory_category.dir_id = directory_data.dir_id');
				$list->setSelect('directory_category.*');
				$list->setSelect('directory_data.dir_name as dir_name');

				$list->setJoin('taxonomies taxonomies','directory_category.tax_id = taxonomies.tax_id');
				$list->setSelect('directory_category.*');
				$list->setSelect('taxonomies.tax_label as tax_label');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('directory_data directory_data','directory_category.dir_id = directory_data.dir_id');
				$pagination->setJoin('taxonomies taxonomies','directory_category.tax_id = taxonomies.tax_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'directory_category',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Directory_category_model;
				$item->setDirCatId( $this->input->post('dir_cat_id'), true );

				$item->setJoin('directory_data directory_data','directory_category.dir_id = directory_data.dir_id');
				$item->setSelect('directory_category.*');
				$item->setSelect('directory_data.dir_name as dir_name');

				$item->setJoin('taxonomies taxonomies','directory_category.tax_id = taxonomies.tax_id');
				$item->setSelect('directory_category.*');
				$item->setSelect('taxonomies.tax_label as tax_label');

				echo json_encode( array(
							'id' => $this->input->post('dir_cat_id'),
							'table' => 'directory_category',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_directory_category ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_cat_id'),
							'table' => 'directory_category',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Directory_category_model;
				$item->setDirCatId( $this->input->post('dir_cat_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByDirCatId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_directory_category ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_directory_category ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_directory_category ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_cat_id'),
							'table' => 'directory_category',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Directory_category_model->setDirCatId( $this->input->post('dir_cat_id') );
				$data = $this->Directory_category_model->getByDirCatId();
		
				
				if( $this->Directory_category_model->deleteByDirCatId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_directory_category ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Directory_category_model;

					if( $container->insert() ) {
						$results['id'] = $container->getDirCatId();
						$results['results'] = $container->getByDirCatId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'directory_category',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('dir_id', 'lang:directory_category_dir_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('dir_cat_id', 'lang:directory_category_dir_cat_id', 'required');
			$this->form_validation->set_rules('dir_id', 'lang:directory_category_dir_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Directory_category_model;
			if( $this->input->post('dir_cat_id') !== FALSE ) {
				$container->setDirCatId( $this->input->post('dir_cat_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_id') !== FALSE ) {
				$container->setDirId( $this->input->post('dir_id'), FALSE, TRUE );
			}

			if( $this->input->post('tax_id') !== FALSE ) {
				$container->setTaxId( $this->input->post('tax_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_cat_active') !== FALSE ) {
				$container->setDirCatActive( $this->input->post('dir_cat_active'), FALSE, TRUE );
			}
			else {
				$container->setDirCatActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByDirCatId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('directory_data directory_data','directory_category.dir_id = directory_data.dir_id');
				$container->setSelect('directory_category.*');
				$container->setSelect('directory_data.dir_name as dir_name');

				$container->setJoin('taxonomies taxonomies','directory_category.tax_id = taxonomies.tax_id');
				$container->setSelect('directory_category.*');
				$container->setSelect('taxonomies.tax_label as tax_label');


			$results['id'] = $container->getDirCatId();
			$results['results'] = $container->getByDirCatId();
		}

	    return $results;
	}
	
}
/* End of file directory_category.php */
/* Location: ./application/controllers/directory_category.php */
