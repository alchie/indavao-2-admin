<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('locations');
        $this->load->model( array('Locations_model') );
        
        $this->template_data->set('main_page', 'taxonomies' ); 
        $this->template_data->set('sub_page', 'locations' ); 
        $this->template_data->set('page_title', 'Locations' ); 

    }
    
    public function index()
	{
        $this->load->view('locations', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'loc_name';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Locations_model;
				$pagination = new $this->Locations_model;
				
				$list->setJoin('attributes attributes','locations.loc_type = attributes.attr_name');
				$list->setSelect('locations.*');
				$list->setSelect('attributes.attr_label as attr_label');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('attributes attributes','locations.loc_type = attributes.attr_name');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'locations',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Locations_model;
				$item->setLocId( $this->input->post('loc_id'), true );

				$item->setJoin('attributes attributes','locations.loc_type = attributes.attr_name');
				$item->setSelect('locations.*');
				$item->setSelect('attributes.attr_label as attr_label');

				echo json_encode( array(
							'id' => $this->input->post('loc_id'),
							'table' => 'locations',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_locations ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('loc_id'),
							'table' => 'locations',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Locations_model;
				$item->setLocId( $this->input->post('loc_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByLocId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_locations ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_locations ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_locations ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('loc_id'),
							'table' => 'locations',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Locations_model->setLocId( $this->input->post('loc_id') );
				$data = $this->Locations_model->getByLocId();
		
				
				if( $this->Locations_model->deleteByLocId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_locations ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Locations_model;

					if( $container->insert() ) {
						$results['id'] = $container->getLocId();
						$results['results'] = $container->getByLocId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'locations',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('loc_name', 'lang:locations_loc_name', 'required');
			$this->form_validation->set_rules('loc_type', 'lang:locations_loc_type', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('loc_id', 'lang:locations_loc_id', 'required');
			$this->form_validation->set_rules('loc_name', 'lang:locations_loc_name', 'required');
			$this->form_validation->set_rules('loc_type', 'lang:locations_loc_type', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Locations_model;
			if( $this->input->post('loc_id') !== FALSE ) {
				$container->setLocId( $this->input->post('loc_id'), FALSE, TRUE );
			}

			if( $this->input->post('loc_name') !== FALSE ) {
				$container->setLocName( $this->input->post('loc_name'), FALSE, TRUE );
			}

			if( $this->input->post('loc_slug') !== FALSE ) {
				$container->setLocSlug( url_title( $this->input->post('loc_slug'), '-', TRUE ), FALSE, TRUE );
				if($container->getLocSlug() == '') {
					$container->setLocSlug( url_title( $this->input->post('loc_name') , '-', TRUE), FALSE, TRUE );
				}
			}

			if( $this->input->post('loc_type') !== FALSE ) {
				$container->setLocType( $this->input->post('loc_type'), FALSE, TRUE );
			}

			if( $this->input->post('loc_order') !== FALSE ) {
				$container->setLocOrder( $this->input->post('loc_order'), FALSE, TRUE );
			}
			else {
				$container->setLocOrder( '0', FALSE, TRUE );
			}

			if( $this->input->post('loc_active') !== FALSE ) {
				$container->setLocActive( $this->input->post('loc_active'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			
				if($container->getLocSlug() == '') {
					$container->setLocSlug( url_title( $this->input->post('loc_name') , '-', TRUE), FALSE, TRUE );
				}

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByLocId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('attributes attributes','locations.loc_type = attributes.attr_name');
				$container->setSelect('locations.*');
				$container->setSelect('attributes.attr_label as attr_label');


			$results['id'] = $container->getLocId();
			$results['results'] = $container->getByLocId();
		}

	    return $results;
	}
	
}
/* End of file locations.php */
/* Location: ./application/controllers/locations.php */
