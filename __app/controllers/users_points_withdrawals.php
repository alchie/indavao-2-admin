<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_points_withdrawals extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('users_points_withdrawals');
        $this->load->model( array('Users_points_withdrawals_model') );
        
        $this->template_data->set('main_page', 'users_points_withdrawals' ); 
        $this->template_data->set('sub_page', 'users_points_withdrawals' ); 
        $this->template_data->set('page_title', 'Withdrawals' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'date_withdrawn';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Users_points_withdrawals_model;
				$pagination = new $this->Users_points_withdrawals_model;
				
				$list->setJoin('users users','users_points_withdrawals.user_id = users.user_id');
				$list->setSelect('users_points_withdrawals.*');
				$list->setSelect('users.name as name');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('users users','users_points_withdrawals.user_id = users.user_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'users_points_withdrawals',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Users_points_withdrawals_model;
				$item->setWithdrawalId( $this->input->post('withdrawal_id'), true );

				$item->setJoin('users users','users_points_withdrawals.user_id = users.user_id');
				$item->setSelect('users_points_withdrawals.*');
				$item->setSelect('users.name as name');

				echo json_encode( array(
							'id' => $this->input->post('withdrawal_id'),
							'table' => 'users_points_withdrawals',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_users_points_withdrawals ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('withdrawal_id'),
							'table' => 'users_points_withdrawals',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Users_points_withdrawals_model;
				$item->setWithdrawalId( $this->input->post('withdrawal_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByWithdrawalId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_users_points_withdrawals ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_users_points_withdrawals ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_users_points_withdrawals ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('withdrawal_id'),
							'table' => 'users_points_withdrawals',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Users_points_withdrawals_model->setWithdrawalId( $this->input->post('withdrawal_id') );
				$data = $this->Users_points_withdrawals_model->getByWithdrawalId();
		
				
				if( $this->Users_points_withdrawals_model->deleteByWithdrawalId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_users_points_withdrawals ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Users_points_withdrawals_model;

					if( $container->insert() ) {
						$results['id'] = $container->getWithdrawalId();
						$results['results'] = $container->getByWithdrawalId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'users_points_withdrawals',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('user_id', 'lang:users_points_withdrawals_user_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('withdrawal_id', 'lang:users_points_withdrawals_withdrawal_id', 'required');
			$this->form_validation->set_rules('user_id', 'lang:users_points_withdrawals_user_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Users_points_withdrawals_model;
			if( $this->input->post('withdrawal_id') !== FALSE ) {
				$container->setWithdrawalId( $this->input->post('withdrawal_id'), FALSE, TRUE );
			}

			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			if( $this->input->post('date_withdrawn') !== FALSE ) {
				$container->setDateWithdrawn( $this->input->post('date_withdrawn'), FALSE, TRUE );
			}

			if( $this->input->post('points_withdrawn') !== FALSE ) {
				$container->setPointsWithdrawn( $this->input->post('points_withdrawn'), FALSE, TRUE );
			}

			if( $this->input->post('withdrawal_reason') !== FALSE ) {
				$container->setWithdrawalReason( $this->input->post('withdrawal_reason'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByWithdrawalId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('users users','users_points_withdrawals.user_id = users.user_id');
				$container->setSelect('users_points_withdrawals.*');
				$container->setSelect('users.name as name');


			$results['id'] = $container->getWithdrawalId();
			$results['results'] = $container->getByWithdrawalId();
		}

	    return $results;
	}
	
}
/* End of file users_points_withdrawals.php */
/* Location: ./application/controllers/users_points_withdrawals.php */
