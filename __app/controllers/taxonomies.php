<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taxonomies extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('taxonomies');
        $this->load->model( array('Taxonomies_model') );
        
        $this->template_data->set('main_page', 'taxonomies' ); 
        $this->template_data->set('sub_page', 'taxonomies' ); 
        $this->template_data->set('page_title', 'Taxonomies' ); 

    }
    
    public function index()
	{
        $this->load->view('taxonomies', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'tax_name';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Taxonomies_model;
				$pagination = new $this->Taxonomies_model;
				
				$list->setJoin('attributes attributes','taxonomies.tax_type = attributes.attr_name');
				$list->setSelect('taxonomies.*');
				$list->setSelect('attributes.attr_label as attr_label');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('attributes attributes','taxonomies.tax_type = attributes.attr_name');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'taxonomies',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Taxonomies_model;
				$item->setTaxId( $this->input->post('tax_id'), true );

				$item->setJoin('attributes attributes','taxonomies.tax_type = attributes.attr_name');
				$item->setSelect('taxonomies.*');
				$item->setSelect('attributes.attr_label as attr_label');

				echo json_encode( array(
							'id' => $this->input->post('tax_id'),
							'table' => 'taxonomies',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tax_id'),
							'table' => 'taxonomies',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Taxonomies_model;
				$item->setTaxId( $this->input->post('tax_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByTaxId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tax_id'),
							'table' => 'taxonomies',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Taxonomies_model->setTaxId( $this->input->post('tax_id') );
				$data = $this->Taxonomies_model->getByTaxId();
		
				
				if( $this->Taxonomies_model->deleteByTaxId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_taxonomies ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Taxonomies_model;

					if( $container->insert() ) {
						$results['id'] = $container->getTaxId();
						$results['results'] = $container->getByTaxId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'taxonomies',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('tax_label', 'lang:taxonomies_tax_label', 'required');
			$this->form_validation->set_rules('tax_type', 'lang:taxonomies_tax_type', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('tax_id', 'lang:taxonomies_tax_id', 'required');
			$this->form_validation->set_rules('tax_label', 'lang:taxonomies_tax_label', 'required');
			$this->form_validation->set_rules('tax_type', 'lang:taxonomies_tax_type', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Taxonomies_model;
			if( $this->input->post('tax_id') !== FALSE ) {
				$container->setTaxId( $this->input->post('tax_id'), FALSE, TRUE );
			}

			if( $this->input->post('tax_name') !== FALSE ) {
				$container->setTaxName( url_title( $this->input->post('tax_name'), '-', TRUE ), FALSE, TRUE );
				if($container->getTaxName() == '') {
					$container->setTaxName( url_title( $this->input->post('tax_label') , '-', TRUE), FALSE, TRUE );
				}
			}

			if( $this->input->post('tax_label') !== FALSE ) {
				$container->setTaxLabel( $this->input->post('tax_label'), FALSE, TRUE );
			}

			if( $this->input->post('tax_type') !== FALSE ) {
				$container->setTaxType( $this->input->post('tax_type'), FALSE, TRUE );
			}

			if( $this->input->post('tax_active') !== FALSE ) {
				$container->setTaxActive( $this->input->post('tax_active'), FALSE, TRUE );
			}
			else {
				$container->setTaxActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			
				if($container->getTaxName() == '') {
					$container->setTaxName( url_title( $this->input->post('tax_label') , '-', TRUE), FALSE, TRUE );
				}

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByTaxId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('attributes attributes','taxonomies.tax_type = attributes.attr_name');
				$container->setSelect('taxonomies.*');
				$container->setSelect('attributes.attr_label as attr_label');


			$results['id'] = $container->getTaxId();
			$results['results'] = $container->getByTaxId();
		}

	    return $results;
	}
	
}
/* End of file taxonomies.php */
/* Location: ./application/controllers/taxonomies.php */
