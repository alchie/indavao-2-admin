<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory_location extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('directory_location');
        $this->load->model( array('Directory_location_model') );
        
        $this->template_data->set('main_page', 'directory_location' ); 
        $this->template_data->set('sub_page', 'directory_location' ); 
        $this->template_data->set('page_title', 'Locations' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'dir_loc_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Directory_location_model;
				$pagination = new $this->Directory_location_model;
				
				$list->setJoin('directory_data directory_data','directory_location.dir_id = directory_data.dir_id');
				$list->setSelect('directory_location.*');
				$list->setSelect('directory_data.dir_name as dir_name');

				$list->setJoin('locations locations','directory_location.loc_id = locations.loc_id');
				$list->setSelect('directory_location.*');
				$list->setSelect('locations.loc_name as loc_name');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('directory_data directory_data','directory_location.dir_id = directory_data.dir_id');
				$pagination->setJoin('locations locations','directory_location.loc_id = locations.loc_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'directory_location',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Directory_location_model;
				$item->setDirLocId( $this->input->post('dir_loc_id'), true );

				$item->setJoin('directory_data directory_data','directory_location.dir_id = directory_data.dir_id');
				$item->setSelect('directory_location.*');
				$item->setSelect('directory_data.dir_name as dir_name');

				$item->setJoin('locations locations','directory_location.loc_id = locations.loc_id');
				$item->setSelect('directory_location.*');
				$item->setSelect('locations.loc_name as loc_name');

				echo json_encode( array(
							'id' => $this->input->post('dir_loc_id'),
							'table' => 'directory_location',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_directory_location ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_loc_id'),
							'table' => 'directory_location',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Directory_location_model;
				$item->setDirLocId( $this->input->post('dir_loc_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByDirLocId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_directory_location ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_directory_location ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_directory_location ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_loc_id'),
							'table' => 'directory_location',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Directory_location_model->setDirLocId( $this->input->post('dir_loc_id') );
				$data = $this->Directory_location_model->getByDirLocId();
		
				
				if( $this->Directory_location_model->deleteByDirLocId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_directory_location ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Directory_location_model;

					if( $container->insert() ) {
						$results['id'] = $container->getDirLocId();
						$results['results'] = $container->getByDirLocId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'directory_location',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('dir_id', 'lang:directory_location_dir_id', 'required');
			$this->form_validation->set_rules('loc_id', 'lang:directory_location_loc_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('dir_loc_id', 'lang:directory_location_dir_loc_id', 'required');
			$this->form_validation->set_rules('dir_id', 'lang:directory_location_dir_id', 'required');
			$this->form_validation->set_rules('loc_id', 'lang:directory_location_loc_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Directory_location_model;
			if( $this->input->post('dir_loc_id') !== FALSE ) {
				$container->setDirLocId( $this->input->post('dir_loc_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_id') !== FALSE ) {
				$container->setDirId( $this->input->post('dir_id'), FALSE, TRUE );
			}

			if( $this->input->post('loc_id') !== FALSE ) {
				$container->setLocId( $this->input->post('loc_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_loc_primary') !== FALSE ) {
				$container->setDirLocPrimary( $this->input->post('dir_loc_primary'), FALSE, TRUE );
			}
			else {
				$container->setDirLocPrimary( '0', FALSE, TRUE );
			}

			if( $this->input->post('dir_loc_active') !== FALSE ) {
				$container->setDirLocActive( $this->input->post('dir_loc_active'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByDirLocId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('directory_data directory_data','directory_location.dir_id = directory_data.dir_id');
				$container->setSelect('directory_location.*');
				$container->setSelect('directory_data.dir_name as dir_name');

				$container->setJoin('locations locations','directory_location.loc_id = locations.loc_id');
				$container->setSelect('directory_location.*');
				$container->setSelect('locations.loc_name as loc_name');


			$results['id'] = $container->getDirLocId();
			$results['results'] = $container->getByDirLocId();
		}

	    return $results;
	}
	
}
/* End of file directory_location.php */
/* Location: ./application/controllers/directory_location.php */
