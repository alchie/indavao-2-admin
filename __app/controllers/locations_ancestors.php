<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations_ancestors extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('locations_ancestors');
        $this->load->model( array('Locations_ancestors_model') );
        
        $this->template_data->set('main_page', 'locations_ancestors' ); 
        $this->template_data->set('sub_page', 'locations_ancestors' ); 
        $this->template_data->set('page_title', 'Ancestors' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'loc_anc_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Locations_ancestors_model;
				$pagination = new $this->Locations_ancestors_model;
				
				$list->setJoin('locations locations','locations_ancestors.loc_id = locations.loc_id');
				$list->setSelect('locations_ancestors.*');
				$list->setSelect('locations.loc_name as loc_name');

				$list->setJoin('locations locations_2','locations_ancestors.loc_parent = locations_2.loc_id');
				$list->setSelect('locations_ancestors.*');
				$list->setSelect('locations_2.loc_name as loc_name_2');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('locations locations','locations_ancestors.loc_id = locations.loc_id');
				$pagination->setJoin('locations locations_2','locations_ancestors.loc_parent = locations_2.loc_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'locations_ancestors',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Locations_ancestors_model;
				$item->setLocAncId( $this->input->post('loc_anc_id'), true );

				$item->setJoin('locations locations','locations_ancestors.loc_id = locations.loc_id');
				$item->setSelect('locations_ancestors.*');
				$item->setSelect('locations.loc_name as loc_name');

				$item->setJoin('locations locations_2','locations_ancestors.loc_parent = locations_2.loc_id');
				$item->setSelect('locations_ancestors.*');
				$item->setSelect('locations_2.loc_name as loc_name_2');

				echo json_encode( array(
							'id' => $this->input->post('loc_anc_id'),
							'table' => 'locations_ancestors',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_locations_ancestors ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('loc_anc_id'),
							'table' => 'locations_ancestors',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Locations_ancestors_model;
				$item->setLocAncId( $this->input->post('loc_anc_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByLocAncId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_locations_ancestors ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_locations_ancestors ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_locations_ancestors ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('loc_anc_id'),
							'table' => 'locations_ancestors',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Locations_ancestors_model->setLocAncId( $this->input->post('loc_anc_id') );
				$data = $this->Locations_ancestors_model->getByLocAncId();
		
				
				if( $this->Locations_ancestors_model->deleteByLocAncId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_locations_ancestors ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Locations_ancestors_model;

					if( $container->insert() ) {
						$results['id'] = $container->getLocAncId();
						$results['results'] = $container->getByLocAncId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'locations_ancestors',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('loc_id', 'lang:locations_ancestors_loc_id', 'required');
			$this->form_validation->set_rules('loc_parent', 'lang:locations_ancestors_loc_parent', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('loc_anc_id', 'lang:locations_ancestors_loc_anc_id', 'required');
			$this->form_validation->set_rules('loc_id', 'lang:locations_ancestors_loc_id', 'required');
			$this->form_validation->set_rules('loc_parent', 'lang:locations_ancestors_loc_parent', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Locations_ancestors_model;
			if( $this->input->post('loc_anc_id') !== FALSE ) {
				$container->setLocAncId( $this->input->post('loc_anc_id'), FALSE, TRUE );
			}

			if( $this->input->post('loc_id') !== FALSE ) {
				$container->setLocId( $this->input->post('loc_id'), FALSE, TRUE );
			}

			if( $this->input->post('loc_parent') !== FALSE ) {
				$container->setLocParent( $this->input->post('loc_parent'), FALSE, TRUE );
			}

			if( $this->input->post('loc_anc_active') !== FALSE ) {
				$container->setLocAncActive( $this->input->post('loc_anc_active'), FALSE, TRUE );
			}
			else {
				$container->setLocAncActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByLocAncId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('locations locations','locations_ancestors.loc_id = locations.loc_id');
				$container->setSelect('locations_ancestors.*');
				$container->setSelect('locations.loc_name as loc_name');

				$container->setJoin('locations locations_2','locations_ancestors.loc_parent = locations_2.loc_id');
				$container->setSelect('locations_ancestors.*');
				$container->setSelect('locations_2.loc_name as loc_name_2');


			$results['id'] = $container->getLocAncId();
			$results['results'] = $container->getByLocAncId();
		}

	    return $results;
	}
	
}
/* End of file locations_ancestors.php */
/* Location: ./application/controllers/locations_ancestors.php */
