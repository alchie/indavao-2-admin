<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate_media extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('realestate_media');
        $this->load->model( array('Realestate_media_model') );
        
        $this->template_data->set('main_page', 'realestate_media' ); 
        $this->template_data->set('sub_page', 'realestate_media' ); 
        $this->template_data->set('page_title', 'Real Estate Media' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 're_med_order';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Realestate_media_model;
				$pagination = new $this->Realestate_media_model;
				
				$list->setJoin('realestate_data realestate_data','realestate_media.re_id = realestate_data.re_id');
				$list->setSelect('realestate_media.*');
				$list->setSelect('realestate_data.re_title as re_title_m');

				$list->setJoin('media_uploads media_uploads','realestate_media.media_id = media_uploads.media_id');
				$list->setSelect('realestate_media.*');
				$list->setSelect('media_uploads.file_name as file_name');

				$list->setJoin('media_uploads media_uploads2','realestate_media.media_thumb = media_uploads2.media_id');
				$list->setSelect('realestate_media.*');
				$list->setSelect('media_uploads2.file_name as file_name2');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('realestate_data realestate_data','realestate_media.re_id = realestate_data.re_id');
				$pagination->setJoin('media_uploads media_uploads','realestate_media.media_id = media_uploads.media_id');
				$pagination->setJoin('media_uploads media_uploads2','realestate_media.media_thumb = media_uploads2.media_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'realestate_media',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Realestate_media_model;
				$item->setReMedId( $this->input->post('re_med_id'), true );

				$item->setJoin('realestate_data realestate_data','realestate_media.re_id = realestate_data.re_id');
				$item->setSelect('realestate_media.*');
				$item->setSelect('realestate_data.re_title as re_title_m');

				$item->setJoin('media_uploads media_uploads','realestate_media.media_id = media_uploads.media_id');
				$item->setSelect('realestate_media.*');
				$item->setSelect('media_uploads.file_name as file_name');

				$item->setJoin('media_uploads media_uploads2','realestate_media.media_thumb = media_uploads2.media_id');
				$item->setSelect('realestate_media.*');
				$item->setSelect('media_uploads2.file_name as file_name2');

				echo json_encode( array(
							'id' => $this->input->post('re_med_id'),
							'table' => 'realestate_media',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_media ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_med_id'),
							'table' => 'realestate_media',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Realestate_media_model;
				$item->setReMedId( $this->input->post('re_med_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByReMedId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_media ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_media ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_media ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_med_id'),
							'table' => 'realestate_media',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Realestate_media_model->setReMedId( $this->input->post('re_med_id') );
				$data = $this->Realestate_media_model->getByReMedId();
		
				
				if( $this->Realestate_media_model->deleteByReMedId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_realestate_media ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Realestate_media_model;

					if( $container->insert() ) {
						$results['id'] = $container->getReMedId();
						$results['results'] = $container->getByReMedId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'realestate_media',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('re_id', 'lang:realestate_media_re_id', 'required');
			$this->form_validation->set_rules('media_id', 'lang:realestate_media_media_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('re_med_id', 'lang:realestate_media_re_med_id', 'required');
			$this->form_validation->set_rules('re_id', 'lang:realestate_media_re_id', 'required');
			$this->form_validation->set_rules('media_id', 'lang:realestate_media_media_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Realestate_media_model;
			if( $this->input->post('re_med_id') !== FALSE ) {
				$container->setReMedId( $this->input->post('re_med_id'), FALSE, TRUE );
			}

			if( $this->input->post('re_id') !== FALSE ) {
				$container->setReId( $this->input->post('re_id'), FALSE, TRUE );
			}

			if( $this->input->post('media_id') !== FALSE ) {
				$container->setMediaId( $this->input->post('media_id'), FALSE, TRUE );
			}

			if( $this->input->post('media_thumb') !== FALSE ) {
				$container->setMediaThumb( $this->input->post('media_thumb'), FALSE, TRUE );
			}

			if( $this->input->post('media_type') !== FALSE ) {
				$container->setMediaType( $this->input->post('media_type'), FALSE, TRUE );
			}

			if( $this->input->post('media_caption') !== FALSE ) {
				$container->setMediaCaption( $this->input->post('media_caption'), FALSE, TRUE );
			}

			if( $this->input->post('media_link') !== FALSE ) {
				$container->setMediaLink( $this->input->post('media_link'), FALSE, TRUE );
			}

			if( $this->input->post('media_name') !== FALSE ) {
				$container->setMediaName( $this->input->post('media_name'), FALSE, TRUE );
			}

			if( $this->input->post('re_med_group') !== FALSE ) {
				$container->setReMedGroup( $this->input->post('re_med_group'), FALSE, TRUE );
			}

			if( $this->input->post('re_med_order') !== FALSE ) {
				$container->setReMedOrder( $this->input->post('re_med_order'), FALSE, TRUE );
			}
			else {
				$container->setReMedOrder( '0', FALSE, TRUE );
			}

			if( $this->input->post('re_med_active') !== FALSE ) {
				$container->setReMedActive( $this->input->post('re_med_active'), FALSE, TRUE );
			}
			else {
				$container->setReMedActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByReMedId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('realestate_data realestate_data','realestate_media.re_id = realestate_data.re_id');
				$container->setSelect('realestate_media.*');
				$container->setSelect('realestate_data.re_title as re_title_m');

				$container->setJoin('media_uploads media_uploads','realestate_media.media_id = media_uploads.media_id');
				$container->setSelect('realestate_media.*');
				$container->setSelect('media_uploads.file_name as file_name');

				$container->setJoin('media_uploads media_uploads2','realestate_media.media_thumb = media_uploads2.media_id');
				$container->setSelect('realestate_media.*');
				$container->setSelect('media_uploads2.file_name as file_name2');


			$results['id'] = $container->getReMedId();
			$results['results'] = $container->getByReMedId();
		}

	    return $results;
	}
	
}
/* End of file realestate_media.php */
/* Location: ./application/controllers/realestate_media.php */
