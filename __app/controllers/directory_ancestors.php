<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory_ancestors extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('directory_ancestors');
        $this->load->model( array('Directory_ancestors_model') );
        
        $this->template_data->set('main_page', 'directory_ancestors' ); 
        $this->template_data->set('sub_page', 'directory_ancestors' ); 
        $this->template_data->set('page_title', 'Ancestors' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'dir_anc_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Directory_ancestors_model;
				$pagination = new $this->Directory_ancestors_model;
				
				$list->setJoin('directory_data directory_data','directory_ancestors.dir_id = directory_data.dir_id');
				$list->setSelect('directory_ancestors.*');
				$list->setSelect('directory_data.dir_name as dir_name');

				$list->setJoin('directory_data directory_data2','directory_ancestors.ancestor_id = directory_data2.dir_id');
				$list->setSelect('directory_ancestors.*');
				$list->setSelect('directory_data2.dir_name as dir_name2');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('directory_data directory_data','directory_ancestors.dir_id = directory_data.dir_id');
				$pagination->setJoin('directory_data directory_data2','directory_ancestors.ancestor_id = directory_data2.dir_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'directory_ancestors',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Directory_ancestors_model;
				$item->setDirAncId( $this->input->post('dir_anc_id'), true );

				$item->setJoin('directory_data directory_data','directory_ancestors.dir_id = directory_data.dir_id');
				$item->setSelect('directory_ancestors.*');
				$item->setSelect('directory_data.dir_name as dir_name');

				$item->setJoin('directory_data directory_data2','directory_ancestors.ancestor_id = directory_data2.dir_id');
				$item->setSelect('directory_ancestors.*');
				$item->setSelect('directory_data2.dir_name as dir_name2');

				echo json_encode( array(
							'id' => $this->input->post('dir_anc_id'),
							'table' => 'directory_ancestors',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_directory_ancestors ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_anc_id'),
							'table' => 'directory_ancestors',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Directory_ancestors_model;
				$item->setDirAncId( $this->input->post('dir_anc_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByDirAncId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_directory_ancestors ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_directory_ancestors ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_directory_ancestors ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('dir_anc_id'),
							'table' => 'directory_ancestors',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Directory_ancestors_model->setDirAncId( $this->input->post('dir_anc_id') );
				$data = $this->Directory_ancestors_model->getByDirAncId();
		
				
				if( $this->Directory_ancestors_model->deleteByDirAncId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_directory_ancestors ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Directory_ancestors_model;

					if( $container->insert() ) {
						$results['id'] = $container->getDirAncId();
						$results['results'] = $container->getByDirAncId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'directory_ancestors',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('dir_id', 'lang:directory_ancestors_dir_id', 'required');
			$this->form_validation->set_rules('ancestor_id', 'lang:directory_ancestors_ancestor_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('dir_anc_id', 'lang:directory_ancestors_dir_anc_id', 'required');
			$this->form_validation->set_rules('dir_id', 'lang:directory_ancestors_dir_id', 'required');
			$this->form_validation->set_rules('ancestor_id', 'lang:directory_ancestors_ancestor_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Directory_ancestors_model;
			if( $this->input->post('dir_anc_id') !== FALSE ) {
				$container->setDirAncId( $this->input->post('dir_anc_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_id') !== FALSE ) {
				$container->setDirId( $this->input->post('dir_id'), FALSE, TRUE );
			}

			if( $this->input->post('ancestor_id') !== FALSE ) {
				$container->setAncestorId( $this->input->post('ancestor_id'), FALSE, TRUE );
			}

			if( $this->input->post('dir_anc_active') !== FALSE ) {
				$container->setDirAncActive( $this->input->post('dir_anc_active'), FALSE, TRUE );
			}
			else {
				$container->setDirAncActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByDirAncId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('directory_data directory_data','directory_ancestors.dir_id = directory_data.dir_id');
				$container->setSelect('directory_ancestors.*');
				$container->setSelect('directory_data.dir_name as dir_name');

				$container->setJoin('directory_data directory_data2','directory_ancestors.ancestor_id = directory_data2.dir_id');
				$container->setSelect('directory_ancestors.*');
				$container->setSelect('directory_data2.dir_name as dir_name2');


			$results['id'] = $container->getDirAncId();
			$results['results'] = $container->getByDirAncId();
		}

	    return $results;
	}
	
}
/* End of file directory_ancestors.php */
/* Location: ./application/controllers/directory_ancestors.php */
