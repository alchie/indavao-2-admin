<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tags extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('tags');
        $this->load->model( array('Tags_model') );
        
        $this->template_data->set('main_page', 'taxonomies' ); 
        $this->template_data->set('sub_page', 'tags' ); 
        $this->template_data->set('page_title', 'Tags' ); 

    }
    
    public function index()
	{
        $this->load->view('tags', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'tag_name';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Tags_model;
				$pagination = new $this->Tags_model;
				
				$pagination->setSelect('COUNT(*) as total_items');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'tags',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Tags_model;
				$item->setTagId( $this->input->post('tag_id'), true );

				echo json_encode( array(
							'id' => $this->input->post('tag_id'),
							'table' => 'tags',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_tags ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tag_id'),
							'table' => 'tags',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Tags_model;
				$item->setTagId( $this->input->post('tag_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByTagId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_tags ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_tags ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_tags ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tag_id'),
							'table' => 'tags',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Tags_model->setTagId( $this->input->post('tag_id') );
				$data = $this->Tags_model->getByTagId();
		
				
				if( $this->Tags_model->deleteByTagId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_tags ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Tags_model;

					if( $container->insert() ) {
						$results['id'] = $container->getTagId();
						$results['results'] = $container->getByTagId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'tags',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('tag_name', 'lang:tags_tag_name', 'trim|required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('tag_id', 'lang:tags_tag_id', 'required');
			$this->form_validation->set_rules('tag_name', 'lang:tags_tag_name', 'trim|required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Tags_model;
			if( $this->input->post('tag_id') !== FALSE ) {
				$container->setTagId( $this->input->post('tag_id'), FALSE, TRUE );
			}

			if( $this->input->post('tag_name') !== FALSE ) {
				$container->setTagName( $this->input->post('tag_name'), FALSE, TRUE );
			}

			if( $this->input->post('tag_slug') !== FALSE ) {
				$container->setTagSlug( url_title( $this->input->post('tag_slug'), '-', TRUE ), FALSE, TRUE );
				if($container->getTagSlug() == '') {
					$container->setTagSlug( url_title( $this->input->post('tag_name') , '-', TRUE), FALSE, TRUE );
				}
			}

			if( $this->input->post('tag_count') !== FALSE ) {
				$container->setTagCount( $this->input->post('tag_count'), FALSE, TRUE );
			}
			else {
				$container->setTagCount( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			
				if($container->getTagSlug() == '') {
					$container->setTagSlug( url_title( $this->input->post('tag_name') , '-', TRUE), FALSE, TRUE );
				}

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByTagId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}

			$results['id'] = $container->getTagId();
			$results['results'] = $container->getByTagId();
		}

	    return $results;
	}
	
}
/* End of file tags.php */
/* Location: ./application/controllers/tags.php */
