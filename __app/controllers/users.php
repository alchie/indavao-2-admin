<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('users');
        $this->load->model( array('Users_model') );
        
        $this->template_data->set('main_page', 'accounts' ); 
        $this->template_data->set('sub_page', 'users' ); 
        $this->template_data->set('page_title', 'Users' ); 

    }
    
    public function index()
	{
        $this->load->view('users', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'first_name';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Users_model;
				$pagination = new $this->Users_model;
				
				$list->setJoin('users users_referrer','users.referrer = users_referrer.user_id');
				$list->setSelect('users.*');
				$list->setSelect('users_referrer.name as ref_name');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('users users_referrer','users.referrer = users_referrer.user_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'users',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Users_model;
				$item->setUserId( $this->input->post('user_id'), true );

				$item->setJoin('users users_referrer','users.referrer = users_referrer.user_id');
				$item->setSelect('users.*');
				$item->setSelect('users_referrer.name as ref_name');

				echo json_encode( array(
							'id' => $this->input->post('user_id'),
							'table' => 'users',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('user_id'),
							'table' => 'users',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Users_model;
				$item->setUserId( $this->input->post('user_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByUserId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('user_id'),
							'table' => 'users',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Users_model->setUserId( $this->input->post('user_id') );
				$data = $this->Users_model->getByUserId();
		
				
				if( $this->Users_model->deleteByUserId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Users_model;

					if( $container->insert() ) {
						$results['id'] = $container->getUserId();
						$results['results'] = $container->getByUserId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'users',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('email', 'lang:users_email', 'valid_email');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('user_id', 'lang:users_user_id', 'required');
			$this->form_validation->set_rules('email', 'lang:users_email', 'valid_email');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Users_model;
			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			if( $this->input->post('api') !== FALSE ) {
				$container->setApi( $this->input->post('api'), FALSE, TRUE );
			}

			if( $this->input->post('email') !== FALSE ) {
				$container->setEmail( $this->input->post('email'), FALSE, TRUE );
			}

			if( $this->input->post('first_name') !== FALSE ) {
				$container->setFirstName( $this->input->post('first_name'), FALSE, TRUE );
			}

			if( $this->input->post('last_name') !== FALSE ) {
				$container->setLastName( $this->input->post('last_name'), FALSE, TRUE );
			}

			if( $this->input->post('gender') !== FALSE ) {
				$container->setGender( $this->input->post('gender'), FALSE, TRUE );
			}

			if( $this->input->post('name') !== FALSE ) {
				$container->setName( $this->input->post('name'), FALSE, TRUE );
			}

			if( $this->input->post('verified') !== FALSE ) {
				$container->setVerified( $this->input->post('verified'), FALSE, TRUE );
			}
			else {
				$container->setVerified( '0', FALSE, TRUE );
			}

			if( $this->input->post('link') !== FALSE ) {
				$container->setLink( $this->input->post('link'), FALSE, TRUE );
			}

			if( $this->input->post('referrer') !== FALSE ) {
				$container->setReferrer( $this->input->post('referrer'), FALSE, TRUE );
			}

			if( $this->input->post('add_points') !== FALSE ) {
				$container->setAddPoints( $this->input->post('add_points'), FALSE, TRUE );
			}
			else {
				$container->setAddPoints( '0', FALSE, TRUE );
			}

			if( $this->input->post('date_joined') !== FALSE ) {
				$container->setDateJoined( $this->input->post('date_joined'), FALSE, TRUE );
			}

			if( $this->input->post('last_login') !== FALSE ) {
				$container->setLastLogin( $this->input->post('last_login'), FALSE, TRUE );
			}

			if( $this->input->post('manager') !== FALSE ) {
				$container->setManager( $this->input->post('manager'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByUserId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('users users_referrer','users.referrer = users_referrer.user_id');
				$container->setSelect('users.*');
				$container->setSelect('users_referrer.name as ref_name');


			$results['id'] = $container->getUserId();
			$results['results'] = $container->getByUserId();
		}

	    return $results;
	}
	
}
/* End of file users.php */
/* Location: ./application/controllers/users.php */
