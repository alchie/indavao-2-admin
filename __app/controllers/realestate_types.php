<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate_types extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('realestate_types');
        $this->load->model( array('Realestate_types_model') );
        
        $this->template_data->set('main_page', 'realestate_types' ); 
        $this->template_data->set('sub_page', 'realestate_types' ); 
        $this->template_data->set('page_title', 'Property Types' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 're_type_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Realestate_types_model;
				$pagination = new $this->Realestate_types_model;
				
				$list->setJoin('realestate_data realestate_data','realestate_types.re_id = realestate_data.re_id');
				$list->setSelect('realestate_types.*');
				$list->setSelect('realestate_data.re_title as re_title');

				$list->setJoin('attributes attributes','realestate_types.type_name = attributes.attr_name');
				$list->setSelect('realestate_types.*');
				$list->setSelect('attributes.attr_label as attr_label');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('realestate_data realestate_data','realestate_types.re_id = realestate_data.re_id');
				$pagination->setJoin('attributes attributes','realestate_types.type_name = attributes.attr_name');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'realestate_types',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Realestate_types_model;
				$item->setReTypeId( $this->input->post('re_type_id'), true );

				$item->setJoin('realestate_data realestate_data','realestate_types.re_id = realestate_data.re_id');
				$item->setSelect('realestate_types.*');
				$item->setSelect('realestate_data.re_title as re_title');

				$item->setJoin('attributes attributes','realestate_types.type_name = attributes.attr_name');
				$item->setSelect('realestate_types.*');
				$item->setSelect('attributes.attr_label as attr_label');

				echo json_encode( array(
							'id' => $this->input->post('re_type_id'),
							'table' => 'realestate_types',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_types ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_type_id'),
							'table' => 'realestate_types',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Realestate_types_model;
				$item->setReTypeId( $this->input->post('re_type_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByReTypeId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_types ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_types ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_realestate_types ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('re_type_id'),
							'table' => 'realestate_types',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Realestate_types_model->setReTypeId( $this->input->post('re_type_id') );
				$data = $this->Realestate_types_model->getByReTypeId();
		
				
				if( $this->Realestate_types_model->deleteByReTypeId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_realestate_types ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Realestate_types_model;

					if( $container->insert() ) {
						$results['id'] = $container->getReTypeId();
						$results['results'] = $container->getByReTypeId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'realestate_types',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('re_id', 'lang:realestate_types_re_id', 'required');
			$this->form_validation->set_rules('type_name', 'lang:realestate_types_type_name', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('re_type_id', 'lang:realestate_types_re_type_id', 'required');
			$this->form_validation->set_rules('re_id', 'lang:realestate_types_re_id', 'required');
			$this->form_validation->set_rules('type_name', 'lang:realestate_types_type_name', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Realestate_types_model;
			if( $this->input->post('re_type_id') !== FALSE ) {
				$container->setReTypeId( $this->input->post('re_type_id'), FALSE, TRUE );
			}

			if( $this->input->post('re_id') !== FALSE ) {
				$container->setReId( $this->input->post('re_id'), FALSE, TRUE );
			}

			if( $this->input->post('type_name') !== FALSE ) {
				$container->setTypeName( $this->input->post('type_name'), FALSE, TRUE );
			}

			if( $this->input->post('re_type_active') !== FALSE ) {
				$container->setReTypeActive( $this->input->post('re_type_active'), FALSE, TRUE );
			}
			else {
				$container->setReTypeActive( '0', FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByReTypeId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('realestate_data realestate_data','realestate_types.re_id = realestate_data.re_id');
				$container->setSelect('realestate_types.*');
				$container->setSelect('realestate_data.re_title as re_title');

				$container->setJoin('attributes attributes','realestate_types.type_name = attributes.attr_name');
				$container->setSelect('realestate_types.*');
				$container->setSelect('attributes.attr_label as attr_label');


			$results['id'] = $container->getReTypeId();
			$results['results'] = $container->getByReTypeId();
		}

	    return $results;
	}
	
}
/* End of file realestate_types.php */
/* Location: ./application/controllers/realestate_types.php */
