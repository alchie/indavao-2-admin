<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tasks_verified extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('tasks_verified');
        $this->load->model( array('Tasks_verified_model') );
        
        $this->template_data->set('main_page', 'tasks_verified' ); 
        $this->template_data->set('sub_page', 'tasks_verified' ); 
        $this->template_data->set('page_title', 'Tasks Verified' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'date_verified';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Tasks_verified_model;
				$pagination = new $this->Tasks_verified_model;
				
				$pagination->setSelect('COUNT(*) as total_items');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'tasks_verified',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Tasks_verified_model;
				$item->setTasksVerId( $this->input->post('tasks_ver_id'), true );

				echo json_encode( array(
							'id' => $this->input->post('tasks_ver_id'),
							'table' => 'tasks_verified',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_tasks_verified ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tasks_ver_id'),
							'table' => 'tasks_verified',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Tasks_verified_model;
				$item->setTasksVerId( $this->input->post('tasks_ver_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByTasksVerId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_tasks_verified ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_tasks_verified ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_tasks_verified ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('tasks_ver_id'),
							'table' => 'tasks_verified',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Tasks_verified_model->setTasksVerId( $this->input->post('tasks_ver_id') );
				$data = $this->Tasks_verified_model->getByTasksVerId();
		
				
				if( $this->Tasks_verified_model->deleteByTasksVerId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_tasks_verified ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Tasks_verified_model;

					if( $container->insert() ) {
						$results['id'] = $container->getTasksVerId();
						$results['results'] = $container->getByTasksVerId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'tasks_verified',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('post_url', 'lang:tasks_verified_post_url', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('post_url', 'lang:tasks_verified_post_url', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Tasks_verified_model;
			if( $this->input->post('tasks_ver_id') !== FALSE ) {
				$container->setTasksVerId( $this->input->post('tasks_ver_id'), FALSE, TRUE );
			}

			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			if( $this->input->post('tasks_url_id') !== FALSE ) {
				$container->setTasksUrlId( $this->input->post('tasks_url_id'), FALSE, TRUE );
			}

			if( $this->input->post('object_id') !== FALSE ) {
				$container->setObjectId( $this->input->post('object_id'), FALSE, TRUE );
			}

			if( $this->input->post('object_type') !== FALSE ) {
				$container->setObjectType( $this->input->post('object_type'), FALSE, TRUE );
			}

			if( $this->input->post('post_url') !== FALSE ) {
				$container->setPostUrl( $this->input->post('post_url'), FALSE, TRUE );
			}

			if( $this->input->post('date_verified') !== FALSE ) {
				$container->setDateVerified( $this->input->post('date_verified'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByTasksVerId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}

			$results['id'] = $container->getTasksVerId();
			$results['results'] = $container->getByTasksVerId();
		}

	    return $results;
	}
	
}
/* End of file tasks_verified.php */
/* Location: ./application/controllers/tasks_verified.php */
