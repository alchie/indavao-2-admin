<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_messages extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('contact_messages');
        $this->load->model( array('Contact_messages_model') );
        
        $this->template_data->set('main_page', 'content' ); 
        $this->template_data->set('sub_page', 'contact_messages' ); 
        $this->template_data->set('page_title', 'Messages' ); 

    }
    
    public function index()
	{
        $this->load->view('contact_messages', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'date_sent';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Contact_messages_model;
				$pagination = new $this->Contact_messages_model;
				
				$list->setJoin('users users','contact_messages.user_id = users.user_id');
				$list->setSelect('contact_messages.*');
				$list->setSelect('users.name as name2');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('users users','contact_messages.user_id = users.user_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'contact_messages',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Contact_messages_model;
				$item->setMsgId( $this->input->post('msg_id'), true );

				$item->setJoin('users users','contact_messages.user_id = users.user_id');
				$item->setSelect('contact_messages.*');
				$item->setSelect('users.name as name2');

				echo json_encode( array(
							'id' => $this->input->post('msg_id'),
							'table' => 'contact_messages',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_contact_messages ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('msg_id'),
							'table' => 'contact_messages',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Contact_messages_model;
				$item->setMsgId( $this->input->post('msg_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByMsgId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_contact_messages ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_contact_messages ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_contact_messages ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('msg_id'),
							'table' => 'contact_messages',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Contact_messages_model->setMsgId( $this->input->post('msg_id') );
				$data = $this->Contact_messages_model->getByMsgId();
		
				
				if( $this->Contact_messages_model->deleteByMsgId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_contact_messages ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Contact_messages_model;

					if( $container->insert() ) {
						$results['id'] = $container->getMsgId();
						$results['results'] = $container->getByMsgId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'contact_messages',
	    );

		if( $action == 'add' ) {
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('msg_id', 'lang:contact_messages_msg_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Contact_messages_model;
			if( $this->input->post('msg_id') !== FALSE ) {
				$container->setMsgId( $this->input->post('msg_id'), FALSE, TRUE );
			}

			if( $this->input->post('object_id') !== FALSE ) {
				$container->setObjectId( $this->input->post('object_id'), FALSE, TRUE );
			}

			if( $this->input->post('object_type') !== FALSE ) {
				$container->setObjectType( $this->input->post('object_type'), FALSE, TRUE );
			}

			if( $this->input->post('name') !== FALSE ) {
				$container->setName( $this->input->post('name'), FALSE, TRUE );
			}

			if( $this->input->post('phone') !== FALSE ) {
				$container->setPhone( $this->input->post('phone'), FALSE, TRUE );
			}

			if( $this->input->post('email') !== FALSE ) {
				$container->setEmail( $this->input->post('email'), FALSE, TRUE );
			}

			if( $this->input->post('message') !== FALSE ) {
				$container->setMessage( $this->input->post('message'), FALSE, TRUE );
			}

			if( $this->input->post('date_sent') !== FALSE ) {
				$container->setDateSent( $this->input->post('date_sent'), FALSE, TRUE );
			}

			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			if( $this->input->post('msg_type') !== FALSE ) {
				$container->setMsgType( $this->input->post('msg_type'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByMsgId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('users users','contact_messages.user_id = users.user_id');
				$container->setSelect('contact_messages.*');
				$container->setSelect('users.name as name2');


			$results['id'] = $container->getMsgId();
			$results['results'] = $container->getByMsgId();
		}

	    return $results;
	}
	
}
/* End of file contact_messages.php */
/* Location: ./application/controllers/contact_messages.php */
