<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-users" class="list-view">
<div class="panel panel-default panel-users">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users->can_add) && ($admin_access->controller_users->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users">Add User</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Email<span  data-key="email" data-table="users" id="list_search_button_email" class="btn btn-primary btn-xs pull-right btn-search list-search-users" title="Search Email">
		<i class="fa fa-search"></i></span></th><th width="">First Name<span  data-key="first_name" data-table="users" id="list_search_button_first_name" class="btn btn-primary btn-xs pull-right btn-search list-search-users" title="Search First Name">
		<i class="fa fa-search"></i></span></th><th width="">Last Name<span  data-key="last_name" data-table="users" id="list_search_button_last_name" class="btn btn-primary btn-xs pull-right btn-search list-search-users" title="Search Last Name">
		<i class="fa fa-search"></i></span></th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="gender" data-table="users">Gender <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Referrer<span data-linked='users' data-key="ref_name" data-table="users" id="list_search_button_ref_name" class="btn btn-primary btn-xs pull-right btn-search list-search-users" title="Search Referrer">
		<i class="fa fa-search"></i></span></th><th width="5%">Points</th><th width="5%">Manager</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users -->
</div>
		<?php if( isset($admin_access->controller_users->can_add) && ($admin_access->controller_users->can_add == 1) ) { ?>
		<div id="add-view-users" style="display:none">
<div class="panel panel-default add-panel-users">
                        <div class="panel-heading"><h3 class="panel-title">Add User</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_user_id" class="add_users_user_id users-input  table-users add-table-users hidden text" placeholder="User ID" value="" />
<div class="form-group">
<label for="add_users_email">Email</label> 
<input data-type="text" type="text" name="email" id="add_users_email" class="form-control add_users_email users-input  table-users add-table-users text text" placeholder="Email" value=""/>
</div>
<div class="form-group">
<label for="add_users_first_name">First Name</label> 
<input data-type="text" type="text" name="first_name" id="add_users_first_name" class="form-control add_users_first_name users-input  table-users add-table-users text text" placeholder="First Name" value=""/>
</div>
<div class="form-group">
<label for="add_users_last_name">Last Name</label> 
<input data-type="text" type="text" name="last_name" id="add_users_last_name" class="form-control add_users_last_name users-input  table-users add-table-users text text" placeholder="Last Name" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_gender">Gender</label> 
			<select name="gender" id="add_users_gender" class="selectpicker form-control add_users_gender users-input  table-users add-table-users dropdown text" placeholder="Gender" data-live-search="true" >
			<option value="">- - Select Gender - -</option>
<option value="female">Female</option>
<option value="male">Male</option>
</select></div>
<div class="form-group">
<label for="add_users_name">Full Name</label> 
<input data-type="text" type="text" name="name" id="add_users_name" class="form-control add_users_name users-input  table-users add-table-users text text" placeholder="Full Name" value=""/>
</div>
<div class="form-group"><strong>Verified</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="verified" id="add_users_verified" class="add_users_verified users-input  table-users add-table-users checkbox text" placeholder="Verified" value="1" />Verified</label></div></div>
<div class="form-group">
<label for="add_users_link">Link</label> 
<input data-type="text" type="text" name="link" id="add_users_link" class="form-control add_users_link users-input  table-users add-table-users text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="add_users_referrer">Referrer</label> 
<input data-type="text" type="hidden" name="referrer" id="add_users_referrer" class="form-control add_users_referrer users-input  table-users add-table-users text text text-searchable-key-referrer  add text-searchable-key" />
<a href="javascript:void(0)" data-field="referrer"  data-table="users" data-key="user_id" data-value="name" data-display="ref_name" data-action="add"  class="text-searchable-list referrer" data-toggle="modal" data-target="#add-text-searchable-box-referrer"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="referrer" class="form-control add text-searchable referrer" placeholder="Search Referrer" data-field="referrer"  data-table="users" data-key="user_id" data-value="name" data-display="ref_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-referrer" tabindex="-1" role="dialog" aria-labelledby="Referrer" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Referrer List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Points</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="add_points" id="add_users_add_points" class="add_users_add_points users-input  table-users add-table-users checkbox text" placeholder="Points" value="1" />Add Points</label></div></div>
<div class="form-group">
<label for="add_users_date_joined">Date Joined</label> 
<input data-type="text" type="text" name="date_joined" id="add_users_date_joined" class="form-control add_users_date_joined users-input  table-users add-table-users text text datetimepicker" placeholder="Date Joined" value=""/>
</div>
<div class="form-group">
<label for="add_users_last_login">Last Login</label> 
<input data-type="text" type="text" name="last_login" id="add_users_last_login" class="form-control add_users_last_login users-input  table-users add-table-users text text datetimepicker" placeholder="Last Login" value=""/>
</div>
<div class="form-group"><strong>Manager</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="manager" id="add_users_manager" class="add_users_manager users-input  table-users add-table-users checkbox text" placeholder="Manager" value="1" />Can Manage Site</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users->can_edit) && ($admin_access->controller_users->can_edit == 1) ) { ?>
		<div id="edit-view-users" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-users" id="update-back-users">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="users" data-child="0">Edit User</a>
		</li>
			<?php if( isset($admin_access->controller_users_bookmarks) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_bookmarks" data-child="1">Bookmarks</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_shares) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_shares" data-child="1">Shares</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_meta" data-child="1">Meta</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_points) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_points" data-child="1">Points</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_permissions) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_permissions" data-child="1">Permissions</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_points_withdrawals) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_points_withdrawals" data-child="1">Redemptions</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-users parent active"><div class="panel panel-default edit-panel-users">
<div class="panel-heading">
	 <h3 class="panel-title">Edit User</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="edit_users_user_id" class="edit_users_user_id users-input  table-users edit-table-users hidden text" placeholder="User ID" value="" />
<div class="form-group">
<label for="edit_users_email">Email</label> 
<input data-type="text" type="text" name="email" id="edit_users_email" class="form-control edit_users_email users-input  table-users edit-table-users text text" placeholder="Email" value=""/>
</div>
<div class="form-group">
<label for="edit_users_first_name">First Name</label> 
<input data-type="text" type="text" name="first_name" id="edit_users_first_name" class="form-control edit_users_first_name users-input  table-users edit-table-users text text" placeholder="First Name" value=""/>
</div>
<div class="form-group">
<label for="edit_users_last_name">Last Name</label> 
<input data-type="text" type="text" name="last_name" id="edit_users_last_name" class="form-control edit_users_last_name users-input  table-users edit-table-users text text" placeholder="Last Name" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_gender">Gender</label> 
			<select name="gender" id="edit_users_gender" class="selectpicker form-control edit_users_gender users-input  table-users edit-table-users dropdown text" placeholder="Gender" data-live-search="true" >
			<option value="">- - Select Gender - -</option>
<option value="female">Female</option>
<option value="male">Male</option>
</select></div>
<div class="form-group">
<label for="edit_users_name">Full Name</label> 
<input data-type="text" type="text" name="name" id="edit_users_name" class="form-control edit_users_name users-input  table-users edit-table-users text text" placeholder="Full Name" value=""/>
</div>
<div class="form-group"><strong>Verified</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="verified" id="edit_users_verified" class="edit_users_verified users-input  table-users edit-table-users checkbox text" placeholder="Verified" value="1" />Verified</label></div></div>
<div class="form-group">
<label for="edit_users_link">Link</label> 
<input data-type="text" type="text" name="link" id="edit_users_link" class="form-control edit_users_link users-input  table-users edit-table-users text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="edit_users_referrer">Referrer</label> 
<input data-type="text" type="hidden" name="referrer" id="edit_users_referrer" class="form-control edit_users_referrer users-input  table-users edit-table-users text  text-searchable-key-referrer  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="referrer"  data-table="users" data-key="user_id" data-value="name" data-display="ref_name" data-action="edit"  class="text-searchable-list referrer" data-toggle="modal" data-target="#edit-text-searchable-box-referrer"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="referrer" class="form-control edit text-searchable referrer" placeholder="Search Referrer" data-field="referrer"  data-table="users" data-key="user_id" data-value="name" data-display="ref_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-referrer" tabindex="-1" role="dialog" aria-labelledby="Referrer" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Referrer List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group change-deliberately button users add_points">
<label>Points</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users add_points" data-table="users" data-field="add_points">Update Points</a>
			<p class="text-value users add_points edit_users_add_points users-input  table-users edit-table-users checkbox " data-type="checkbox"></p>
			</div>
			<div class="panel panel-info change-deliberately form users add_points" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users add_points" data-table="users" data-field="add_points">Cancel</a>
			Update Points</div>
			<div class="panel-body">
			
<div class="form-group"><strong>Points</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="add_points" id="edit_users_add_points" class="edit_users_add_points users-input  table-users edit-table-users checkbox " placeholder="Points" value="1" />Add Points</label></div></div></div>
</div>

<div class="form-group change-deliberately button users date_joined">
<label>Date Joined</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users date_joined" data-table="users" data-field="date_joined">Update Date Joined</a>
			<p class="text-value users date_joined edit_users_date_joined users-input  table-users edit-table-users text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form users date_joined" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users date_joined" data-table="users" data-field="date_joined">Cancel</a>
			Update Date Joined</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_users_date_joined">Date Joined</label> 
<input data-type="text" type="text" name="date_joined" id="edit_users_date_joined" class="form-control edit_users_date_joined users-input  table-users edit-table-users text  datetimepicker" placeholder="Date Joined" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button users last_login">
<label>Last Login</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users last_login" data-table="users" data-field="last_login">Update Last Login</a>
			<p class="text-value users last_login edit_users_last_login users-input  table-users edit-table-users text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form users last_login" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users last_login" data-table="users" data-field="last_login">Cancel</a>
			Update Last Login</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_users_last_login">Last Login</label> 
<input data-type="text" type="text" name="last_login" id="edit_users_last_login" class="form-control edit_users_last_login users-input  table-users edit-table-users text  datetimepicker" placeholder="Last Login" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button users manager">
<label>Manager</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users manager" data-table="users" data-field="manager">Update Manager</a>
			<p class="text-value users manager edit_users_manager users-input  table-users edit-table-users checkbox " data-type="checkbox"></p>
			</div>
			<div class="panel panel-info change-deliberately form users manager" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users manager" data-table="users" data-field="manager">Cancel</a>
			Update Manager</div>
			<div class="panel-body">
			
<div class="form-group"><strong>Manager</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="manager" id="edit_users_manager" class="edit_users_manager users-input  table-users edit-table-users checkbox " placeholder="Manager" value="1" />Can Manage Site</label></div></div></div>
</div>

</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users" id="update-back-users">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users -->
</div><!-- .tab-content .tab-content-users -->
			<?php if( isset($admin_access->controller_users_bookmarks) ) { ?>
			<div class="tab-content tab-content-users_bookmarks" style="display:none"><div id="list-view-users_bookmarks" class="list-view">
<div class="panel panel-default panel-users_bookmarks">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_bookmarks->can_add) && ($admin_access->controller_users_bookmarks->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_bookmarks">Add Bookmark</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="None">Object ID</th><th width="None">Object Type</th><th width="None">Date Added</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_bookmarks -->
</div>
		<?php if( isset($admin_access->controller_users_bookmarks->can_add) && ($admin_access->controller_users_bookmarks->can_add == 1) ) { ?>
		<div id="add-view-users_bookmarks" style="display:none">
<div class="panel panel-default add-panel-users_bookmarks">
                        <div class="panel-heading"><h3 class="panel-title">Add Bookmark</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_bookmarks_user_id" class="add_users_bookmarks_user_id users_bookmarks-input  table-users_bookmarks add-table-users_bookmarks hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_users_bookmarks_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="add_users_bookmarks_object_id" class="form-control add_users_bookmarks_object_id users_bookmarks-input  table-users_bookmarks add-table-users_bookmarks text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="add_users_bookmarks_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="add_users_bookmarks_object_type" class="form-control add_users_bookmarks_object_type users_bookmarks-input  table-users_bookmarks add-table-users_bookmarks text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group">
<label for="add_users_bookmarks_date_added">Date Added</label> 
<input data-type="text" type="text" name="date_added" id="add_users_bookmarks_date_added" class="form-control add_users_bookmarks_date_added users_bookmarks-input  table-users_bookmarks add-table-users_bookmarks text text datetimepicker" placeholder="Date Added" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_bookmarks">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_bookmarks">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_bookmarks -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_bookmarks->can_edit) && ($admin_access->controller_users_bookmarks->can_edit == 1) ) { ?>
		<div id="edit-view-users_bookmarks" style="display:none">
		
		<div class="panel panel-default edit-panel-users_bookmarks">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Bookmark</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="bookmark_id" id="edit_users_bookmarks_bookmark_id" class="edit_users_bookmarks_bookmark_id users_bookmarks-input  table-users_bookmarks edit-table-users_bookmarks hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_bookmarks_user_id" class="edit_users_bookmarks_user_id users_bookmarks-input  table-users_bookmarks edit-table-users_bookmarks hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="edit_users_bookmarks_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="edit_users_bookmarks_object_id" class="form-control edit_users_bookmarks_object_id users_bookmarks-input  table-users_bookmarks edit-table-users_bookmarks text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="edit_users_bookmarks_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="edit_users_bookmarks_object_type" class="form-control edit_users_bookmarks_object_type users_bookmarks-input  table-users_bookmarks edit-table-users_bookmarks text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group">
<label for="edit_users_bookmarks_date_added">Date Added</label> 
<input data-type="text" type="text" name="date_added" id="edit_users_bookmarks_date_added" class="form-control edit_users_bookmarks_date_added users_bookmarks-input  table-users_bookmarks edit-table-users_bookmarks text text datetimepicker" placeholder="Date Added" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_bookmarks">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_bookmarks" id="update-back-users_bookmarks">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_bookmarks -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_bookmarks -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_shares) ) { ?>
			<div class="tab-content tab-content-users_shares" style="display:none"><div id="list-view-users_shares" class="list-view">
<div class="panel panel-default panel-users_shares">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_shares->can_add) && ($admin_access->controller_users_shares->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_shares">Add Share</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Object ID</th><th width="None">Object</th><th width="None">FB Post ID</th><th width="None">Liked</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_shares -->
</div>
		<?php if( isset($admin_access->controller_users_shares->can_add) && ($admin_access->controller_users_shares->can_add == 1) ) { ?>
		<div id="add-view-users_shares" style="display:none">
<div class="panel panel-default add-panel-users_shares">
                        <div class="panel-heading"><h3 class="panel-title">Add Share</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="share_id" id="add_users_shares_share_id" class="add_users_shares_share_id users_shares-input  table-users_shares add-table-users_shares hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="add_users_shares_user_id" class="add_users_shares_user_id users_shares-input  table-users_shares add-table-users_shares hidden text" placeholder="User ID" value="" />
<div class="form-group">
<label for="add_users_shares_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="add_users_shares_object_id" class="form-control add_users_shares_object_id users_shares-input  table-users_shares add-table-users_shares text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="add_users_shares_object_type">Object</label> 
<input data-type="text" type="text" name="object_type" id="add_users_shares_object_type" class="form-control add_users_shares_object_type users_shares-input  table-users_shares add-table-users_shares text text" placeholder="Object" value=""/>
</div>
<div class="form-group">
<label for="add_users_shares_fb_post_id">FB Post ID</label> 
<input data-type="text" type="text" name="fb_post_id" id="add_users_shares_fb_post_id" class="form-control add_users_shares_fb_post_id users_shares-input  table-users_shares add-table-users_shares text text" placeholder="FB Post ID" value=""/>
</div>
<div class="form-group"><strong>Liked</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="likes" id="add_users_shares_likes" class="add_users_shares_likes users_shares-input  table-users_shares add-table-users_shares checkbox text" placeholder="Liked" value="1" />Liked</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_shares">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_shares">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_shares -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_shares->can_edit) && ($admin_access->controller_users_shares->can_edit == 1) ) { ?>
		<div id="edit-view-users_shares" style="display:none">
		
		<div class="panel panel-default edit-panel-users_shares">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Share</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="share_id" id="edit_users_shares_share_id" class="edit_users_shares_share_id users_shares-input  table-users_shares edit-table-users_shares hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_shares_user_id" class="edit_users_shares_user_id users_shares-input  table-users_shares edit-table-users_shares hidden text" placeholder="User ID" value="" />
<div class="form-group">
<label for="edit_users_shares_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="edit_users_shares_object_id" class="form-control edit_users_shares_object_id users_shares-input  table-users_shares edit-table-users_shares text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="edit_users_shares_object_type">Object</label> 
<input data-type="text" type="text" name="object_type" id="edit_users_shares_object_type" class="form-control edit_users_shares_object_type users_shares-input  table-users_shares edit-table-users_shares text text" placeholder="Object" value=""/>
</div>
<div class="form-group">
<label for="edit_users_shares_fb_post_id">FB Post ID</label> 
<input data-type="text" type="text" name="fb_post_id" id="edit_users_shares_fb_post_id" class="form-control edit_users_shares_fb_post_id users_shares-input  table-users_shares edit-table-users_shares text text" placeholder="FB Post ID" value=""/>
</div>
<div class="form-group"><strong>Liked</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="likes" id="edit_users_shares_likes" class="edit_users_shares_likes users_shares-input  table-users_shares edit-table-users_shares checkbox text" placeholder="Liked" value="1" />Liked</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_shares">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_shares" id="update-back-users_shares">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_shares -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_shares -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_meta) ) { ?>
			<div class="tab-content tab-content-users_meta" style="display:none"><div id="list-view-users_meta" class="list-view">
<div class="panel panel-default panel-users_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_meta->can_add) && ($admin_access->controller_users_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Key</th><th width="None">Value</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_meta -->
</div>
		<?php if( isset($admin_access->controller_users_meta->can_add) && ($admin_access->controller_users_meta->can_add == 1) ) { ?>
		<div id="add-view-users_meta" style="display:none">
<div class="panel panel-default add-panel-users_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_meta_user_id" class="add_users_meta_user_id users_meta-input  table-users_meta add-table-users_meta hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_users_meta_meta_key">Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_users_meta_meta_key" class="form-control add_users_meta_meta_key users_meta-input  table-users_meta add-table-users_meta text text" placeholder="Key" value=""/>
</div>
<div class="form-group">
<label for="add_users_meta_meta_value">Value</label>
<textarea rows="10" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_users_meta_meta_value" class="form-control add_users_meta_meta_value users_meta-input  table-users_meta add-table-users_meta textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="user_meta_active" id="add_users_meta_user_meta_active" class="add_users_meta_user_meta_active users_meta-input  table-users_meta add-table-users_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_meta->can_edit) && ($admin_access->controller_users_meta->can_edit == 1) ) { ?>
		<div id="edit-view-users_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-users_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_meta_id" id="edit_users_meta_user_meta_id" class="edit_users_meta_user_meta_id users_meta-input  table-users_meta edit-table-users_meta hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_meta_user_id" class="edit_users_meta_user_id users_meta-input  table-users_meta edit-table-users_meta hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="edit_users_meta_meta_key">Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_users_meta_meta_key" class="form-control edit_users_meta_meta_key users_meta-input  table-users_meta edit-table-users_meta text text" placeholder="Key" value=""/>
</div>
<div class="form-group">
<label for="edit_users_meta_meta_value">Value</label>
<textarea rows="10" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_users_meta_meta_value" class="form-control edit_users_meta_meta_value users_meta-input  table-users_meta edit-table-users_meta textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="user_meta_active" id="edit_users_meta_user_meta_active" class="edit_users_meta_user_meta_active users_meta-input  table-users_meta edit-table-users_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_meta" id="update-back-users_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_meta -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_points) ) { ?>
			<div class="tab-content tab-content-users_points" style="display:none"><div id="list-view-users_points" class="list-view">
<div class="panel panel-default panel-users_points">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_points->can_add) && ($admin_access->controller_users_points->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_points">Add Points</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Object ID</th><th width="None">Object Type</th><th width="None">Points Code</th><th width="None">Points Credited</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_points -->
</div>
		<?php if( isset($admin_access->controller_users_points->can_add) && ($admin_access->controller_users_points->can_add == 1) ) { ?>
		<div id="add-view-users_points" style="display:none">
<div class="panel panel-default add-panel-users_points">
                        <div class="panel-heading"><h3 class="panel-title">Add Points</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_points_user_id" class="add_users_points_user_id users_points-input  table-users_points add-table-users_points hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_users_points_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="add_users_points_object_id" class="form-control add_users_points_object_id users_points-input  table-users_points add-table-users_points text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="add_users_points_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="add_users_points_object_type" class="form-control add_users_points_object_type users_points-input  table-users_points add-table-users_points text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_points_type">Points Code</label> 
			<select name="points_type" id="add_users_points_points_type" class="selectpicker form-control add_users_points_points_type users_points-input  table-users_points add-table-users_points dropdown text dropdown-table" placeholder="Points Code" data-live-search="true"  data-type="dropdown" data-label="Points Code" data-field="points_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="users_points_code" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Points Code - -</option>
</select></div>
<div class="form-group">
<label for="add_users_points_points_credited">Points Credited</label> 
<input data-type="text" type="text" name="points_credited" id="add_users_points_points_credited" class="form-control add_users_points_points_credited users_points-input  table-users_points add-table-users_points text text" placeholder="Points Credited" value=""/>
</div>
<div class="form-group">
<label for="add_users_points_date_added">Date Credited</label> 
<input data-type="text" type="text" name="date_added" id="add_users_points_date_added" class="form-control add_users_points_date_added users_points-input  table-users_points add-table-users_points text text datetimepicker" placeholder="Date Credited" value=""/>
</div>
<div class="form-group"><strong>Claimed</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="points_claimed" id="add_users_points_points_claimed" class="add_users_points_points_claimed users_points-input  table-users_points add-table-users_points checkbox text" placeholder="Claimed" value="1" />Claim</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_points">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_points">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_points -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_points->can_edit) && ($admin_access->controller_users_points->can_edit == 1) ) { ?>
		<div id="edit-view-users_points" style="display:none">
		
		<div class="panel panel-default edit-panel-users_points">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Points</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="points_id" id="edit_users_points_points_id" class="edit_users_points_points_id users_points-input  table-users_points edit-table-users_points hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_points_user_id" class="edit_users_points_user_id users_points-input  table-users_points edit-table-users_points hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="edit_users_points_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="edit_users_points_object_id" class="form-control edit_users_points_object_id users_points-input  table-users_points edit-table-users_points text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="edit_users_points_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="edit_users_points_object_type" class="form-control edit_users_points_object_type users_points-input  table-users_points edit-table-users_points text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_points_type">Points Code</label> 
			<select name="points_type" id="edit_users_points_points_type" class="selectpicker form-control edit_users_points_points_type users_points-input  table-users_points edit-table-users_points dropdown text dropdown-table" placeholder="Points Code" data-live-search="true"  data-type="dropdown" data-label="Points Code" data-field="points_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="users_points_code" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Points Code - -</option>
</select></div>
<div class="form-group">
<label for="edit_users_points_points_credited">Points Credited</label> 
<input data-type="text" type="text" name="points_credited" id="edit_users_points_points_credited" class="form-control edit_users_points_points_credited users_points-input  table-users_points edit-table-users_points text text" placeholder="Points Credited" value=""/>
</div>
<div class="form-group">
<label for="edit_users_points_date_added">Date Credited</label> 
<input data-type="text" type="text" name="date_added" id="edit_users_points_date_added" class="form-control edit_users_points_date_added users_points-input  table-users_points edit-table-users_points text text datetimepicker" placeholder="Date Credited" value=""/>
</div>
<div class="form-group"><strong>Claimed</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="points_claimed" id="edit_users_points_points_claimed" class="edit_users_points_points_claimed users_points-input  table-users_points edit-table-users_points checkbox text" placeholder="Claimed" value="1" />Claim</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_points">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_points" id="update-back-users_points">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_points -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_points -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_permissions) ) { ?>
			<div class="tab-content tab-content-users_permissions" style="display:none"><div id="list-view-users_permissions" class="list-view">
<div class="panel panel-default panel-users_permissions">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_permissions->can_add) && ($admin_access->controller_users_permissions->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_permissions">Add Permission</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Permission</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_permissions -->
</div>
		<?php if( isset($admin_access->controller_users_permissions->can_add) && ($admin_access->controller_users_permissions->can_add == 1) ) { ?>
		<div id="add-view-users_permissions" style="display:none">
<div class="panel panel-default add-panel-users_permissions">
                        <div class="panel-heading"><h3 class="panel-title">Add Permission</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_permissions_user_id" class="add_users_permissions_user_id users_permissions-input  table-users_permissions add-table-users_permissions hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_lessons_permission">Permission</label> 
			<select name="permission" id="add_users_permissions_permission" class="selectpicker form-control add_users_permissions_permission users_permissions-input  table-users_permissions add-table-users_permissions dropdown text" placeholder="Permission" data-live-search="true" >
			<option value="">- - Select Permission - -</option>
<option value="add_business">Add Business</option>
<option value="add_jobs">Add Job Posts</option>
<option value="add_realestate">Add Real Estate</option>
<option value="clear_cache">Clear Cache</option>
<option value="delete_business">Delete Business</option>
<option value="delete_jobs">Delete Job Posts</option>
<option value="delete_realestate">Delete Real Estate</option>
<option value="edit_business">Edit Business</option>
<option value="edit_jobs">Edit Job Posts</option>
<option value="edit_realestate">Edit Real Estate</option>
<option value="manage_business">Manage Business</option>
<option value="manage_jobs">Manage Job Posts</option>
<option value="manage_realestate">Manage Real Estate</option>
<option value="manage_taxonomies">Manage Taxonomies</option>
<option value="manage_users">Manage Users</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_users_permissions_active" class="add_users_permissions_active users_permissions-input  table-users_permissions add-table-users_permissions checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_permissions">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_permissions">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_permissions -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_permissions->can_edit) && ($admin_access->controller_users_permissions->can_edit == 1) ) { ?>
		<div id="edit-view-users_permissions" style="display:none">
		
		<div class="panel panel-default edit-panel-users_permissions">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Permission</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="permission_id" id="edit_users_permissions_permission_id" class="edit_users_permissions_permission_id users_permissions-input  table-users_permissions edit-table-users_permissions hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_permissions_user_id" class="edit_users_permissions_user_id users_permissions-input  table-users_permissions edit-table-users_permissions hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_lessons_permission">Permission</label> 
			<select name="permission" id="edit_users_permissions_permission" class="selectpicker form-control edit_users_permissions_permission users_permissions-input  table-users_permissions edit-table-users_permissions dropdown text" placeholder="Permission" data-live-search="true" >
			<option value="">- - Select Permission - -</option>
<option value="add_business">Add Business</option>
<option value="add_jobs">Add Job Posts</option>
<option value="add_realestate">Add Real Estate</option>
<option value="clear_cache">Clear Cache</option>
<option value="delete_business">Delete Business</option>
<option value="delete_jobs">Delete Job Posts</option>
<option value="delete_realestate">Delete Real Estate</option>
<option value="edit_business">Edit Business</option>
<option value="edit_jobs">Edit Job Posts</option>
<option value="edit_realestate">Edit Real Estate</option>
<option value="manage_business">Manage Business</option>
<option value="manage_jobs">Manage Job Posts</option>
<option value="manage_realestate">Manage Real Estate</option>
<option value="manage_taxonomies">Manage Taxonomies</option>
<option value="manage_users">Manage Users</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_users_permissions_active" class="edit_users_permissions_active users_permissions-input  table-users_permissions edit-table-users_permissions checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_permissions">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_permissions" id="update-back-users_permissions">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_permissions -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_permissions -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_points_withdrawals) ) { ?>
			<div class="tab-content tab-content-users_points_withdrawals" style="display:none"><div id="list-view-users_points_withdrawals" class="list-view">
<div class="panel panel-default panel-users_points_withdrawals">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_points_withdrawals->can_add) && ($admin_access->controller_users_points_withdrawals->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_points_withdrawals">Add Redemption</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_points_withdrawals -->
</div>
		<?php if( isset($admin_access->controller_users_points_withdrawals->can_add) && ($admin_access->controller_users_points_withdrawals->can_add == 1) ) { ?>
		<div id="add-view-users_points_withdrawals" style="display:none">
<div class="panel panel-default add-panel-users_points_withdrawals">
                        <div class="panel-heading"><h3 class="panel-title">Add Redemption</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_points_withdrawals_user_id" class="add_users_points_withdrawals_user_id users_points_withdrawals-input  table-users_points_withdrawals add-table-users_points_withdrawals hidden text" placeholder="User" value="" />
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_points_withdrawals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_points_withdrawals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_points_withdrawals -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_points_withdrawals->can_edit) && ($admin_access->controller_users_points_withdrawals->can_edit == 1) ) { ?>
		<div id="edit-view-users_points_withdrawals" style="display:none">
		
		<div class="panel panel-default edit-panel-users_points_withdrawals">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Redemption</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="withdrawal_id" id="edit_users_points_withdrawals_withdrawal_id" class="edit_users_points_withdrawals_withdrawal_id users_points_withdrawals-input  table-users_points_withdrawals edit-table-users_points_withdrawals hidden " placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_points_withdrawals_user_id" class="edit_users_points_withdrawals_user_id users_points_withdrawals-input  table-users_points_withdrawals edit-table-users_points_withdrawals hidden text" placeholder="User" value="" />
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_points_withdrawals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_points_withdrawals" id="update-back-users_points_withdrawals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_points_withdrawals -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_points_withdrawals -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'users',
		tables : { 
		<?php if( isset($admin_access->controller_users) ) { ?>
		
'users' : { label : 'User',
fields : ["api","user_id","email","first_name","last_name","gender","name","verified","link","referrer","add_points","date_joined","last_login","manager"],
add_fields : ["user_id","email","first_name","last_name","gender","name","verified","link","referrer","add_points","date_joined","last_login","manager"],
edit_fields : ["user_id","email","first_name","last_name","gender","name","verified","link","referrer"],
list_limit : 20,
list_fields : ["email","first_name","last_name","gender","ref_name","add_points","manager"],
order_by : 'first_name',
order_sort : 'ASC',
filters : {"gender":{"type":"manual","anchor":0}},
primary_key : 'user_id',
primary_title : 'name',
active_key : 'verified',
actual_values : {"referrer" : "ref_name"},
check_cross : ["add_points","manager"],
actions_edit : <?php echo ($admin_access->controller_users->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_bookmarks) ) { ?>
		
'users_bookmarks' : { label : 'Bookmark',
fields : ["bookmark_id","user_id","object_id","object_type","date_added"],
add_fields : ["user_id","object_id","object_type","date_added"],
edit_fields : ["bookmark_id","user_id","object_id","object_type","date_added"],
list_limit : 20,
list_fields : ["object_id","object_type","date_added"],
order_by : 'date_added',
order_sort : 'DESC',
primary_key : 'bookmark_id',
actual_values : {"user_id" : "name"},
required_key : 'user_id',
required_value : 'user_id',
required_table : 'users',
actions_edit : <?php echo ($admin_access->controller_users_bookmarks->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_bookmarks->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_shares) ) { ?>
		
'users_shares' : { label : 'FB Shares',
fields : ["share_id","user_id","object_id","object_type","fb_post_id","likes"],
add_fields : ["share_id","user_id","object_id","object_type","fb_post_id","likes"],
edit_fields : ["share_id","user_id","object_id","object_type","fb_post_id","likes"],
list_limit : 20,
list_fields : ["object_id","object_type","fb_post_id","likes"],
order_by : 'share_id',
order_sort : 'DESC',
primary_key : 'share_id',
active_key : 'likes',
required_key : 'user_id',
required_value : 'user_id',
required_table : 'users',
actions_edit : <?php echo ($admin_access->controller_users_shares->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_shares->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_meta) ) { ?>
		
'users_meta' : { label : 'User Meta',
fields : ["user_meta_id","user_id","meta_key","meta_value","user_meta_active"],
add_fields : ["user_id","meta_key","meta_value","user_meta_active"],
edit_fields : ["user_meta_id","user_id","meta_key","meta_value","user_meta_active"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 'meta_key',
order_sort : 'ASC',
primary_key : 'user_meta_id',
primary_title : 'meta_key',
active_key : 'user_meta_active',
required_key : 'user_id',
required_value : 'user_id',
required_table : 'users',
actions_edit : <?php echo ($admin_access->controller_users_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_points) ) { ?>
		
'users_points' : { label : 'Points',
fields : ["points_id","user_id","object_id","object_type","points_type","points_credited","date_added","points_claimed"],
add_fields : ["user_id","object_id","object_type","points_type","points_credited","date_added","points_claimed"],
edit_fields : ["points_id","user_id","object_id","object_type","points_type","points_credited","date_added","points_claimed"],
list_limit : 20,
list_fields : ["object_id","object_type","points_type","points_credited"],
order_by : 'date_added',
order_sort : 'DESC',
primary_key : 'id',
primary_title : 'user_id',
active_key : 'points_claimed',
required_key : 'user_id',
required_value : 'user_id',
required_table : 'users',
actions_edit : <?php echo ($admin_access->controller_users_points->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_points->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_permissions) ) { ?>
		
'users_permissions' : { label : 'Permission',
fields : ["permission_id","user_id","permission","active"],
add_fields : ["user_id","permission","active"],
edit_fields : ["permission_id","user_id","permission","active"],
list_limit : 20,
list_fields : ["permission"],
order_by : 'permission',
order_sort : 'DESC',
filters : {"permission":{"type":"manual","anchor":0}},
primary_key : 'permission_id',
primary_title : 'permission',
active_key : 'active',
required_key : 'user_id',
required_value : 'user_id',
required_table : 'users',
actions_edit : <?php echo ($admin_access->controller_users_permissions->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_permissions->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_points_withdrawals) ) { ?>
		
'users_points_withdrawals' : { label : 'Withdrawal',
fields : ["date_withdrawn","withdrawal_id","user_id","points_withdrawn","withdrawal_reason"],
add_fields : ["user_id"],
edit_fields : ["withdrawal_id","user_id"],
list_limit : 20,
order_by : 'date_withdrawn',
order_sort : 'DESC',
primary_key : 'withdrawal_id',
primary_title : 'withdrawal_reason',
actual_values : {"user_id" : "name"},
required_key : 'user_id',
required_value : 'user_id',
required_table : 'users',
actions_edit : <?php echo ($admin_access->controller_users_points_withdrawals->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_points_withdrawals->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"verified": {"1": "Verified"}, "permission": {"manage_users": "Manage Users", "delete_business": "Delete Business", "clear_cache": "Clear Cache", "manage_business": "Manage Business", "edit_realestate": "Edit Real Estate", "manage_realestate": "Manage Real Estate", "add_business": "Add Business", "manage_taxonomies": "Manage Taxonomies", "edit_jobs": "Edit Job Posts", "manage_jobs": "Manage Job Posts", "delete_jobs": "Delete Job Posts", "edit_business": "Edit Business", "delete_realestate": "Delete Real Estate", "add_jobs": "Add Job Posts", "add_realestate": "Add Real Estate"}, "gender": {"male": "Male", "female": "Female"}, "points_claimed": {"1": "Claim"}, "manager": {"1": "Can Manage Site"}, "add_points": {"1": "Add Points"}, "likes": {"1": "Liked"}, "active": {"1": "Active"}, "user_meta_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>