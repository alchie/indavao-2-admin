<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-directory_data" class="list-view">
<div class="panel panel-default panel-directory_data">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_data->can_add) && ($admin_access->controller_directory_data->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_data">Add Directory</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Name<span  data-key="dir_name" data-table="directory_data" id="list_search_button_dir_name" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_data" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="5%">Branch</th><th width="5%">Verified</th><th width="6%"><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="dir_status" data-table="directory_data">Status <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="10%">User<span data-linked='users' data-key="name" data-table="directory_data" id="list_search_button_name" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_data" title="Search User">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_data -->
</div>
		<?php if( isset($admin_access->controller_directory_data->can_add) && ($admin_access->controller_directory_data->can_add == 1) ) { ?>
		<div id="add-view-directory_data" style="display:none">
<div class="panel panel-default add-panel-directory_data">
                        <div class="panel-heading"><h3 class="panel-title">Add Directory</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_directory_data_dir_name">Name</label> 
<input data-type="text" type="text" name="dir_name" id="add_directory_data_dir_name" class="form-control add_directory_data_dir_name directory_data-input  table-directory_data add-table-directory_data text text" placeholder="Name" value=""/>
</div>
<div class="form-group"><strong>Branch</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_branch" id="add_directory_data_dir_branch" class="add_directory_data_dir_branch directory_data-input  table-directory_data add-table-directory_data checkbox text" placeholder="Branch" value="1" />Branch</label></div></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_active" id="add_directory_data_dir_active" class="add_directory_data_dir_active directory_data-input  table-directory_data add-table-directory_data checkbox text" placeholder="Active" value="1" />Active</label></div></div>
<div class="form-group"><strong>Verified</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_verified" id="add_directory_data_dir_verified" class="add_directory_data_dir_verified directory_data-input  table-directory_data add-table-directory_data checkbox text" placeholder="Verified" value="1" />Verified</label></div></div>
<div class="form-group">
<label for="add_directory_data_user_id">User</label> 
<input data-type="text" type="hidden" name="user_id" id="add_directory_data_user_id" class="form-control add_directory_data_user_id directory_data-input  table-directory_data add-table-directory_data text text text-searchable-key-user_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name" data-action="add"  class="text-searchable-list user_id" data-toggle="modal" data-target="#add-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control add text-searchable user_id" placeholder="Search User" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_data">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_data">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_data -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_data->can_edit) && ($admin_access->controller_directory_data->can_edit == 1) ) { ?>
		<div id="edit-view-directory_data" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-directory_data" id="update-back-directory_data">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="directory_data" data-child="0">Edit Directory</a>
		</li>
			<?php if( isset($admin_access->controller_directory_details) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="directory_details" data-child="1">Details</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_category) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="directory_category" data-child="1">Category</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_location) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="directory_location" data-child="1">Location</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="directory_meta" data-child="1">Meta</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_ancestors) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="directory_ancestors" data-child="1">Ancestors</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_media) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="directory_media" data-child="1">Media</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_tags) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="directory_tags" data-child="1">Tags</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-directory_data parent active"><div class="panel panel-default edit-panel-directory_data">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Directory</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_data_dir_id" class="edit_directory_data_dir_id directory_data-input  table-directory_data edit-table-directory_data hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_directory_data_dir_name">Name</label> 
<input data-type="text" type="text" name="dir_name" id="edit_directory_data_dir_name" class="form-control edit_directory_data_dir_name directory_data-input  table-directory_data edit-table-directory_data text text" placeholder="Name" value=""/>
</div>
<div class="form-group change-deliberately button directory_data dir_slug">
<label>Slug</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update directory_data dir_slug" data-table="directory_data" data-field="dir_slug">Update Slug</a>
			<p class="text-value directory_data dir_slug edit_directory_data_dir_slug directory_data-input  table-directory_data edit-table-directory_data text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form directory_data dir_slug" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel directory_data dir_slug" data-table="directory_data" data-field="dir_slug">Cancel</a>
			Update Slug</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_directory_data_dir_slug">Slug</label> 
<input data-type="text" type="text" name="dir_slug" id="edit_directory_data_dir_slug" class="form-control edit_directory_data_dir_slug directory_data-input  table-directory_data edit-table-directory_data text " placeholder="Slug" value=""/>
</div></div>
</div>

<div class="form-group"><strong>Branch</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_branch" id="edit_directory_data_dir_branch" class="edit_directory_data_dir_branch directory_data-input  table-directory_data edit-table-directory_data checkbox text" placeholder="Branch" value="1" />Branch</label></div></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_active" id="edit_directory_data_dir_active" class="edit_directory_data_dir_active directory_data-input  table-directory_data edit-table-directory_data checkbox text" placeholder="Active" value="1" />Active</label></div></div>
<div class="form-group"><strong>Verified</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_verified" id="edit_directory_data_dir_verified" class="edit_directory_data_dir_verified directory_data-input  table-directory_data edit-table-directory_data checkbox text" placeholder="Verified" value="1" />Verified</label></div></div>
<div class="form-group change-deliberately button directory_data dir_added">
<label>Date Added</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update directory_data dir_added" data-table="directory_data" data-field="dir_added">Update Date Added</a>
			<p class="text-value directory_data dir_added edit_directory_data_dir_added directory_data-input  table-directory_data edit-table-directory_data text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form directory_data dir_added" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel directory_data dir_added" data-table="directory_data" data-field="dir_added">Cancel</a>
			Update Date Added</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_directory_data_dir_added">Date Added</label> 
<input data-type="text" type="text" name="dir_added" id="edit_directory_data_dir_added" class="form-control edit_directory_data_dir_added directory_data-input  table-directory_data edit-table-directory_data text  datetimepicker" placeholder="Date Added" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button directory_data dir_status">
<label>Status</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update directory_data dir_status" data-table="directory_data" data-field="dir_status">Update Status</a>
			<p class="text-value directory_data dir_status edit_directory_data_dir_status directory_data-input  table-directory_data edit-table-directory_data dropdown " data-type="dropdown"></p>
			</div>
			<div class="panel panel-info change-deliberately form directory_data dir_status" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel directory_data dir_status" data-table="directory_data" data-field="dir_status">Cancel</a>
			Update Status</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="add_lessons_dir_status">Status</label> 
			<select name="dir_status" id="edit_directory_data_dir_status" class="selectpicker form-control edit_directory_data_dir_status directory_data-input  table-directory_data edit-table-directory_data dropdown " placeholder="Status" data-live-search="true" >
			<option value="">- - Select Status - -</option>
<option value="pending">Approval Request</option>
<option value="draft">Draft</option>
<option value="private">Private</option>
<option value="publish">Published</option>
<option value="user">Users Only</option>
</select></div></div>
</div>

<div class="form-group change-deliberately button directory_data user_id">
<label>User</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update directory_data user_id" data-table="directory_data" data-field="user_id">Update User</a>
			<p class="text-value directory_data user_id edit_directory_data_user_id directory_data-input  table-directory_data edit-table-directory_data text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form directory_data user_id" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel directory_data user_id" data-table="directory_data" data-field="user_id">Cancel</a>
			Update User</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_directory_data_user_id">User</label> 
<input data-type="text" type="hidden" name="user_id" id="edit_directory_data_user_id" class="form-control edit_directory_data_user_id directory_data-input  table-directory_data edit-table-directory_data text  text-searchable-key-user_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name" data-action="edit"  class="text-searchable-list user_id" data-toggle="modal" data-target="#edit-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control edit text-searchable user_id" placeholder="Search User" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div></div>
</div>

</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_data">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_data" id="update-back-directory_data">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_data -->
</div><!-- .tab-content .tab-content-directory_data -->
			<?php if( isset($admin_access->controller_directory_details) ) { ?>
			<div class="tab-content tab-content-directory_details" style="display:none"><div id="list-view-directory_details" class="list-view">
<div class="panel panel-default panel-directory_details">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_details->can_add) && ($admin_access->controller_directory_details->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_details">Add Detail</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="dir_d_key" data-table="directory_details">Detail Key <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Value<span  data-key="dir_d_value" data-table="directory_details" id="list_search_button_dir_d_value" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_details" title="Search Value">
		<i class="fa fa-search"></i></span></th><th width="None">Label</th><th width="10%">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_details -->
</div>
		<?php if( isset($admin_access->controller_directory_details->can_add) && ($admin_access->controller_directory_details->can_add == 1) ) { ?>
		<div id="add-view-directory_details" style="display:none">
<div class="panel panel-default add-panel-directory_details">
                        <div class="panel-heading"><h3 class="panel-title">Add Detail</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="add_directory_details_dir_id" class="add_directory_details_dir_id directory_details-input  table-directory_details add-table-directory_details hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_lessons_dir_d_key">Detail Key</label> 
			<select name="dir_d_key" id="add_directory_details_dir_d_key" class="selectpicker form-control add_directory_details_dir_d_key directory_details-input  table-directory_details add-table-directory_details dropdown text dropdown-table" placeholder="Detail Key" data-live-search="true"  data-type="dropdown" data-label="Detail Key" data-field="dir_d_key" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="directory_details" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Detail Key - -</option>
</select></div>
<div class="form-group">
<label for="add_directory_details_dir_d_value">Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="dir_d_value" id="add_directory_details_dir_d_value" class="form-control add_directory_details_dir_d_value directory_details-input  table-directory_details add-table-directory_details textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group">
<label for="add_directory_details_dir_d_label">Label</label> 
<input data-type="text" type="text" name="dir_d_label" id="add_directory_details_dir_d_label" class="form-control add_directory_details_dir_d_label directory_details-input  table-directory_details add-table-directory_details text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="add_directory_details_dir_d_order">Order</label> 
<input data-type="text" type="text" name="dir_d_order" id="add_directory_details_dir_d_order" class="form-control add_directory_details_dir_d_order directory_details-input  table-directory_details add-table-directory_details text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_d_active" id="add_directory_details_dir_d_active" class="add_directory_details_dir_d_active directory_details-input  table-directory_details add-table-directory_details checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_details">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_details">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_details -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_details->can_edit) && ($admin_access->controller_directory_details->can_edit == 1) ) { ?>
		<div id="edit-view-directory_details" style="display:none">
		
		<div class="panel panel-default edit-panel-directory_details">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Detail</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_d_id" id="edit_directory_details_dir_d_id" class="edit_directory_details_dir_d_id directory_details-input  table-directory_details edit-table-directory_details hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_details_dir_id" class="edit_directory_details_dir_id directory_details-input  table-directory_details edit-table-directory_details hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_lessons_dir_d_key">Detail Key</label> 
			<select name="dir_d_key" id="edit_directory_details_dir_d_key" class="selectpicker form-control edit_directory_details_dir_d_key directory_details-input  table-directory_details edit-table-directory_details dropdown text dropdown-table" placeholder="Detail Key" data-live-search="true"  data-type="dropdown" data-label="Detail Key" data-field="dir_d_key" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="directory_details" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Detail Key - -</option>
</select></div>
<div class="form-group">
<label for="edit_directory_details_dir_d_value">Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="dir_d_value" id="edit_directory_details_dir_d_value" class="form-control edit_directory_details_dir_d_value directory_details-input  table-directory_details edit-table-directory_details textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group">
<label for="edit_directory_details_dir_d_label">Label</label> 
<input data-type="text" type="text" name="dir_d_label" id="edit_directory_details_dir_d_label" class="form-control edit_directory_details_dir_d_label directory_details-input  table-directory_details edit-table-directory_details text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="edit_directory_details_dir_d_order">Order</label> 
<input data-type="text" type="text" name="dir_d_order" id="edit_directory_details_dir_d_order" class="form-control edit_directory_details_dir_d_order directory_details-input  table-directory_details edit-table-directory_details text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_d_active" id="edit_directory_details_dir_d_active" class="edit_directory_details_dir_d_active directory_details-input  table-directory_details edit-table-directory_details checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_details">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_details" id="update-back-directory_details">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_details -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-directory_details -->
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_category) ) { ?>
			<div class="tab-content tab-content-directory_category" style="display:none"><div id="list-view-directory_category" class="list-view">
<div class="panel panel-default panel-directory_category">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_category->can_add) && ($admin_access->controller_directory_category->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_category">Add Category</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Taxonomy<span data-linked='taxonomies' data-key="tax_label" data-table="directory_category" id="list_search_button_tax_label" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_category" title="Search Taxonomy">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_category -->
</div>
		<?php if( isset($admin_access->controller_directory_category->can_add) && ($admin_access->controller_directory_category->can_add == 1) ) { ?>
		<div id="add-view-directory_category" style="display:none">
<div class="panel panel-default add-panel-directory_category">
                        <div class="panel-heading"><h3 class="panel-title">Add Category</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="add_directory_category_dir_id" class="add_directory_category_dir_id directory_category-input  table-directory_category add-table-directory_category hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_directory_category_tax_id">Taxonomy</label> 
<input data-type="text" type="hidden" name="tax_id" id="add_directory_category_tax_id" class="form-control add_directory_category_tax_id directory_category-input  table-directory_category add-table-directory_category text text text-searchable-key-tax_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="tax_id"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="add"  class="text-searchable-list tax_id" data-toggle="modal" data-target="#add-text-searchable-box-tax_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tax_id" class="form-control add text-searchable tax_id" placeholder="Search Taxonomy" data-field="tax_id"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-tax_id" tabindex="-1" role="dialog" aria-labelledby="Taxonomy" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Taxonomy List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_cat_active" id="add_directory_category_dir_cat_active" class="add_directory_category_dir_cat_active directory_category-input  table-directory_category add-table-directory_category checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_category">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_category">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_category -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_category->can_edit) && ($admin_access->controller_directory_category->can_edit == 1) ) { ?>
		<div id="edit-view-directory_category" style="display:none">
		
		<div class="panel panel-default edit-panel-directory_category">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Category</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_cat_id" id="edit_directory_category_dir_cat_id" class="edit_directory_category_dir_cat_id directory_category-input  table-directory_category edit-table-directory_category hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_category_dir_id" class="edit_directory_category_dir_id directory_category-input  table-directory_category edit-table-directory_category hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="edit_directory_category_tax_id">Taxonomy</label> 
<input data-type="text" type="hidden" name="tax_id" id="edit_directory_category_tax_id" class="form-control edit_directory_category_tax_id directory_category-input  table-directory_category edit-table-directory_category text text text-searchable-key-tax_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="tax_id"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="edit"  class="text-searchable-list tax_id" data-toggle="modal" data-target="#edit-text-searchable-box-tax_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tax_id" class="form-control edit text-searchable tax_id" placeholder="Search Taxonomy" data-field="tax_id"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-tax_id" tabindex="-1" role="dialog" aria-labelledby="Taxonomy" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Taxonomy List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_cat_active" id="edit_directory_category_dir_cat_active" class="edit_directory_category_dir_cat_active directory_category-input  table-directory_category edit-table-directory_category checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_category">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_category" id="update-back-directory_category">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_category -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-directory_category -->
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_location) ) { ?>
			<div class="tab-content tab-content-directory_location" style="display:none"><div id="list-view-directory_location" class="list-view">
<div class="panel panel-default panel-directory_location">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_location->can_add) && ($admin_access->controller_directory_location->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_location">Add Location</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Location<span data-linked='locations' data-key="loc_name" data-table="directory_location" id="list_search_button_loc_name" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_location" title="Search Location">
		<i class="fa fa-search"></i></span></th><th width="10%">Primary</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_location -->
</div>
		<?php if( isset($admin_access->controller_directory_location->can_add) && ($admin_access->controller_directory_location->can_add == 1) ) { ?>
		<div id="add-view-directory_location" style="display:none">
<div class="panel panel-default add-panel-directory_location">
                        <div class="panel-heading"><h3 class="panel-title">Add Location</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="add_directory_location_dir_id" class="add_directory_location_dir_id directory_location-input  table-directory_location add-table-directory_location hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_directory_location_loc_id">Location</label> 
<input data-type="text" type="hidden" name="loc_id" id="add_directory_location_loc_id" class="form-control add_directory_location_loc_id directory_location-input  table-directory_location add-table-directory_location text text text-searchable-key-loc_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name" data-action="add"  class="text-searchable-list loc_id" data-toggle="modal" data-target="#add-text-searchable-box-loc_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="loc_id" class="form-control add text-searchable loc_id" placeholder="Search Location" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-loc_id" tabindex="-1" role="dialog" aria-labelledby="Location" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Location List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Primary</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_loc_primary" id="add_directory_location_dir_loc_primary" class="add_directory_location_dir_loc_primary directory_location-input  table-directory_location add-table-directory_location checkbox text" placeholder="Primary" value="1" />Primary</label></div></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_loc_active" id="add_directory_location_dir_loc_active" class="add_directory_location_dir_loc_active directory_location-input  table-directory_location add-table-directory_location checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_location">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_location">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_location -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_location->can_edit) && ($admin_access->controller_directory_location->can_edit == 1) ) { ?>
		<div id="edit-view-directory_location" style="display:none">
		
		<div class="panel panel-default edit-panel-directory_location">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Location</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_loc_id" id="edit_directory_location_dir_loc_id" class="edit_directory_location_dir_loc_id directory_location-input  table-directory_location edit-table-directory_location hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_location_dir_id" class="edit_directory_location_dir_id directory_location-input  table-directory_location edit-table-directory_location hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="edit_directory_location_loc_id">Location</label> 
<input data-type="text" type="hidden" name="loc_id" id="edit_directory_location_loc_id" class="form-control edit_directory_location_loc_id directory_location-input  table-directory_location edit-table-directory_location text text text-searchable-key-loc_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name" data-action="edit"  class="text-searchable-list loc_id" data-toggle="modal" data-target="#edit-text-searchable-box-loc_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="loc_id" class="form-control edit text-searchable loc_id" placeholder="Search Location" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-loc_id" tabindex="-1" role="dialog" aria-labelledby="Location" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Location List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Primary</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_loc_primary" id="edit_directory_location_dir_loc_primary" class="edit_directory_location_dir_loc_primary directory_location-input  table-directory_location edit-table-directory_location checkbox text" placeholder="Primary" value="1" />Primary</label></div></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_loc_active" id="edit_directory_location_dir_loc_active" class="edit_directory_location_dir_loc_active directory_location-input  table-directory_location edit-table-directory_location checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_location">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_location" id="update-back-directory_location">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_location -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-directory_location -->
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_meta) ) { ?>
			<div class="tab-content tab-content-directory_meta" style="display:none"><div id="list-view-directory_meta" class="list-view">
<div class="panel panel-default panel-directory_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_meta->can_add) && ($admin_access->controller_directory_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Meta Key</th><th width="None">Meta Value</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_meta -->
</div>
		<?php if( isset($admin_access->controller_directory_meta->can_add) && ($admin_access->controller_directory_meta->can_add == 1) ) { ?>
		<div id="add-view-directory_meta" style="display:none">
<div class="panel panel-default add-panel-directory_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="add_directory_meta_dir_id" class="add_directory_meta_dir_id directory_meta-input  table-directory_meta add-table-directory_meta hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_directory_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_directory_meta_meta_key" class="form-control add_directory_meta_meta_key directory_meta-input  table-directory_meta add-table-directory_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="add_directory_meta_meta_value">Meta Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_directory_meta_meta_value" class="form-control add_directory_meta_meta_value directory_meta-input  table-directory_meta add-table-directory_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_directory_meta_active" class="add_directory_meta_active directory_meta-input  table-directory_meta add-table-directory_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_meta->can_edit) && ($admin_access->controller_directory_meta->can_edit == 1) ) { ?>
		<div id="edit-view-directory_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-directory_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_m_id" id="edit_directory_meta_dir_m_id" class="edit_directory_meta_dir_m_id directory_meta-input  table-directory_meta edit-table-directory_meta hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_meta_dir_id" class="edit_directory_meta_dir_id directory_meta-input  table-directory_meta edit-table-directory_meta hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="edit_directory_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_directory_meta_meta_key" class="form-control edit_directory_meta_meta_key directory_meta-input  table-directory_meta edit-table-directory_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="edit_directory_meta_meta_value">Meta Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_directory_meta_meta_value" class="form-control edit_directory_meta_meta_value directory_meta-input  table-directory_meta edit-table-directory_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_directory_meta_active" class="edit_directory_meta_active directory_meta-input  table-directory_meta edit-table-directory_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_meta" id="update-back-directory_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-directory_meta -->
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_ancestors) ) { ?>
			<div class="tab-content tab-content-directory_ancestors" style="display:none"><div id="list-view-directory_ancestors" class="list-view">
<div class="panel panel-default panel-directory_ancestors">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_ancestors->can_add) && ($admin_access->controller_directory_ancestors->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_ancestors">Add Ancestor</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Parent Directory</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_ancestors -->
</div>
		<?php if( isset($admin_access->controller_directory_ancestors->can_add) && ($admin_access->controller_directory_ancestors->can_add == 1) ) { ?>
		<div id="add-view-directory_ancestors" style="display:none">
<div class="panel panel-default add-panel-directory_ancestors">
                        <div class="panel-heading"><h3 class="panel-title">Add Ancestor</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="add_directory_ancestors_dir_id" class="add_directory_ancestors_dir_id directory_ancestors-input  table-directory_ancestors add-table-directory_ancestors hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_directory_ancestors_ancestor_id">Parent Directory</label> 
<input data-type="text" type="text" name="ancestor_id" id="add_directory_ancestors_ancestor_id" class="form-control add_directory_ancestors_ancestor_id directory_ancestors-input  table-directory_ancestors add-table-directory_ancestors text text" placeholder="Parent Directory" value=""/>
</div>
<div class="form-group"><strong>Actve</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_anc_active" id="add_directory_ancestors_dir_anc_active" class="add_directory_ancestors_dir_anc_active directory_ancestors-input  table-directory_ancestors add-table-directory_ancestors checkbox text" placeholder="Actve" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_ancestors -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_ancestors->can_edit) && ($admin_access->controller_directory_ancestors->can_edit == 1) ) { ?>
		<div id="edit-view-directory_ancestors" style="display:none">
		
		<div class="panel panel-default edit-panel-directory_ancestors">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Ancestor</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_anc_id" id="edit_directory_ancestors_dir_anc_id" class="edit_directory_ancestors_dir_anc_id directory_ancestors-input  table-directory_ancestors edit-table-directory_ancestors hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_ancestors_dir_id" class="edit_directory_ancestors_dir_id directory_ancestors-input  table-directory_ancestors edit-table-directory_ancestors hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="edit_directory_ancestors_ancestor_id">Parent Directory</label> 
<input data-type="text" type="text" name="ancestor_id" id="edit_directory_ancestors_ancestor_id" class="form-control edit_directory_ancestors_ancestor_id directory_ancestors-input  table-directory_ancestors edit-table-directory_ancestors text text" placeholder="Parent Directory" value=""/>
</div>
<div class="form-group"><strong>Actve</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_anc_active" id="edit_directory_ancestors_dir_anc_active" class="edit_directory_ancestors_dir_anc_active directory_ancestors-input  table-directory_ancestors edit-table-directory_ancestors checkbox text" placeholder="Actve" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_ancestors" id="update-back-directory_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_ancestors -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-directory_ancestors -->
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_media) ) { ?>
			<div class="tab-content tab-content-directory_media" style="display:none"><div id="list-view-directory_media" class="list-view">
<div class="panel panel-default panel-directory_media">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_media->can_add) && ($admin_access->controller_directory_media->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_media">Add Media</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="dir_med_group" data-table="directory_media">Group <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Media<span data-linked='media_uploads' data-key="file_name" data-table="directory_media" id="list_search_button_file_name" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_media" title="Search Media">
		<i class="fa fa-search"></i></span></th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="media_type" data-table="directory_media">Type <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="None">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_media -->
</div>
		<?php if( isset($admin_access->controller_directory_media->can_add) && ($admin_access->controller_directory_media->can_add == 1) ) { ?>
		<div id="add-view-directory_media" style="display:none">
<div class="panel panel-default add-panel-directory_media">
                        <div class="panel-heading"><h3 class="panel-title">Add Media</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="add_directory_media_dir_id" class="add_directory_media_dir_id directory_media-input  table-directory_media add-table-directory_media hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_lessons_dir_med_group">Group</label> 
			<select name="dir_med_group" id="add_directory_media_dir_med_group" class="selectpicker form-control add_directory_media_dir_med_group directory_media-input  table-directory_media add-table-directory_media dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="gallery">Gallery</option>
<option value="general">General Media</option>
<option value="slider">Slider</option>
</select></div>
<div class="form-group">
<label for="add_directory_media_media_id">Media</label> 
<input data-type="text" type="hidden" name="media_id" id="add_directory_media_media_id" class="form-control add_directory_media_media_id directory_media-input  table-directory_media add-table-directory_media text text text-searchable-key-media_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="add"  class="text-searchable-list media_id" data-toggle="modal" data-target="#add-text-searchable-box-media_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_id" class="form-control add text-searchable media_id" placeholder="Search Media" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-media_id" tabindex="-1" role="dialog" aria-labelledby="Media" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Media List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_directory_media_media_thumb">Thumbnail</label> 
<input data-type="text" type="hidden" name="media_thumb" id="add_directory_media_media_thumb" class="form-control add_directory_media_media_thumb directory_media-input  table-directory_media add-table-directory_media text text text-searchable-key-media_thumb  add text-searchable-key" />
<a href="javascript:void(0)" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="add"  class="text-searchable-list media_thumb" data-toggle="modal" data-target="#add-text-searchable-box-media_thumb"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_thumb" class="form-control add text-searchable media_thumb" placeholder="Search Thumbnail" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-media_thumb" tabindex="-1" role="dialog" aria-labelledby="Thumbnail" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Thumbnail List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_media_type">Type</label> 
			<select name="media_type" id="add_directory_media_media_type" class="selectpicker form-control add_directory_media_media_type directory_media-input  table-directory_media add-table-directory_media dropdown text" placeholder="Type" data-live-search="true" >
			<option value="">- - Select Type - -</option>
<option value="image">Image</option>
<option value="video">Video</option>
</select></div>
<div class="form-group">
<label for="add_directory_media_media_caption">Caption</label> 
<input data-type="text" type="text" name="media_caption" id="add_directory_media_media_caption" class="form-control add_directory_media_media_caption directory_media-input  table-directory_media add-table-directory_media text text" placeholder="Caption" value=""/>
</div>
<div class="form-group">
<label for="add_directory_media_media_link">Link</label> 
<input data-type="text" type="text" name="media_link" id="add_directory_media_media_link" class="form-control add_directory_media_media_link directory_media-input  table-directory_media add-table-directory_media text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="add_directory_media_media_name">Name</label> 
<input data-type="text" type="text" name="media_name" id="add_directory_media_media_name" class="form-control add_directory_media_media_name directory_media-input  table-directory_media add-table-directory_media text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_directory_media_dir_med_order">Order</label> 
<input data-type="text" type="text" name="dir_med_order" id="add_directory_media_dir_med_order" class="form-control add_directory_media_dir_med_order directory_media-input  table-directory_media add-table-directory_media text text" placeholder="Order" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_med_active" id="add_directory_media_dir_med_active" class="add_directory_media_dir_med_active directory_media-input  table-directory_media add-table-directory_media checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_media">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_media">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_media -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_media->can_edit) && ($admin_access->controller_directory_media->can_edit == 1) ) { ?>
		<div id="edit-view-directory_media" style="display:none">
		
		<div class="panel panel-default edit-panel-directory_media">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Media</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_med_id" id="edit_directory_media_dir_med_id" class="edit_directory_media_dir_med_id directory_media-input  table-directory_media edit-table-directory_media hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_media_dir_id" class="edit_directory_media_dir_id directory_media-input  table-directory_media edit-table-directory_media hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_lessons_dir_med_group">Group</label> 
			<select name="dir_med_group" id="edit_directory_media_dir_med_group" class="selectpicker form-control edit_directory_media_dir_med_group directory_media-input  table-directory_media edit-table-directory_media dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="gallery">Gallery</option>
<option value="general">General Media</option>
<option value="slider">Slider</option>
</select></div>
<div class="form-group">
<label for="edit_directory_media_media_id">Media</label> 
<input data-type="text" type="hidden" name="media_id" id="edit_directory_media_media_id" class="form-control edit_directory_media_media_id directory_media-input  table-directory_media edit-table-directory_media text text text-searchable-key-media_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="edit"  class="text-searchable-list media_id" data-toggle="modal" data-target="#edit-text-searchable-box-media_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_id" class="form-control edit text-searchable media_id" placeholder="Search Media" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-media_id" tabindex="-1" role="dialog" aria-labelledby="Media" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Media List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="edit_directory_media_media_thumb">Thumbnail</label> 
<input data-type="text" type="hidden" name="media_thumb" id="edit_directory_media_media_thumb" class="form-control edit_directory_media_media_thumb directory_media-input  table-directory_media edit-table-directory_media text text text-searchable-key-media_thumb  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="edit"  class="text-searchable-list media_thumb" data-toggle="modal" data-target="#edit-text-searchable-box-media_thumb"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_thumb" class="form-control edit text-searchable media_thumb" placeholder="Search Thumbnail" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-media_thumb" tabindex="-1" role="dialog" aria-labelledby="Thumbnail" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Thumbnail List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_media_type">Type</label> 
			<select name="media_type" id="edit_directory_media_media_type" class="selectpicker form-control edit_directory_media_media_type directory_media-input  table-directory_media edit-table-directory_media dropdown text" placeholder="Type" data-live-search="true" >
			<option value="">- - Select Type - -</option>
<option value="image">Image</option>
<option value="video">Video</option>
</select></div>
<div class="form-group">
<label for="edit_directory_media_media_caption">Caption</label> 
<input data-type="text" type="text" name="media_caption" id="edit_directory_media_media_caption" class="form-control edit_directory_media_media_caption directory_media-input  table-directory_media edit-table-directory_media text text" placeholder="Caption" value=""/>
</div>
<div class="form-group">
<label for="edit_directory_media_media_link">Link</label> 
<input data-type="text" type="text" name="media_link" id="edit_directory_media_media_link" class="form-control edit_directory_media_media_link directory_media-input  table-directory_media edit-table-directory_media text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="edit_directory_media_media_name">Name</label> 
<input data-type="text" type="text" name="media_name" id="edit_directory_media_media_name" class="form-control edit_directory_media_media_name directory_media-input  table-directory_media edit-table-directory_media text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="edit_directory_media_dir_med_order">Order</label> 
<input data-type="text" type="text" name="dir_med_order" id="edit_directory_media_dir_med_order" class="form-control edit_directory_media_dir_med_order directory_media-input  table-directory_media edit-table-directory_media text text" placeholder="Order" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_med_active" id="edit_directory_media_dir_med_active" class="edit_directory_media_dir_med_active directory_media-input  table-directory_media edit-table-directory_media checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_media">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_media" id="update-back-directory_media">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_media -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-directory_media -->
			<?php } ?>
			<?php if( isset($admin_access->controller_directory_tags) ) { ?>
			<div class="tab-content tab-content-directory_tags" style="display:none"><div id="list-view-directory_tags" class="list-view">
<div class="panel panel-default panel-directory_tags">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_tags->can_add) && ($admin_access->controller_directory_tags->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_tags">Add Tag</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Tag<span data-linked='tags' data-key="tag_name" data-table="directory_tags" id="list_search_button_tag_name" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_tags" title="Search Tag">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_tags -->
</div>
		<?php if( isset($admin_access->controller_directory_tags->can_add) && ($admin_access->controller_directory_tags->can_add == 1) ) { ?>
		<div id="add-view-directory_tags" style="display:none">
<div class="panel panel-default add-panel-directory_tags">
                        <div class="panel-heading"><h3 class="panel-title">Add Tag</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_id" id="add_directory_tags_dir_id" class="add_directory_tags_dir_id directory_tags-input  table-directory_tags add-table-directory_tags hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="add_directory_tags_tag_id">Tag</label> 
<input data-type="text" type="hidden" name="tag_id" id="add_directory_tags_tag_id" class="form-control add_directory_tags_tag_id directory_tags-input  table-directory_tags add-table-directory_tags text text text-searchable-key-tag_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="add"  class="text-searchable-list tag_id" data-toggle="modal" data-target="#add-text-searchable-box-tag_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tag_id" class="form-control add text-searchable tag_id" placeholder="Search Tag" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-tag_id" tabindex="-1" role="dialog" aria-labelledby="Tag" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Tag List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_tag_active" id="add_directory_tags_dir_tag_active" class="add_directory_tags_dir_tag_active directory_tags-input  table-directory_tags add-table-directory_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_tags -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_tags->can_edit) && ($admin_access->controller_directory_tags->can_edit == 1) ) { ?>
		<div id="edit-view-directory_tags" style="display:none">
		
		<div class="panel panel-default edit-panel-directory_tags">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Tag</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_tag_id" id="edit_directory_tags_dir_tag_id" class="edit_directory_tags_dir_tag_id directory_tags-input  table-directory_tags edit-table-directory_tags hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="dir_id" id="edit_directory_tags_dir_id" class="edit_directory_tags_dir_id directory_tags-input  table-directory_tags edit-table-directory_tags hidden text" placeholder="Directory" value="" />
<div class="form-group">
<label for="edit_directory_tags_tag_id">Tag</label> 
<input data-type="text" type="hidden" name="tag_id" id="edit_directory_tags_tag_id" class="form-control edit_directory_tags_tag_id directory_tags-input  table-directory_tags edit-table-directory_tags text text text-searchable-key-tag_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="edit"  class="text-searchable-list tag_id" data-toggle="modal" data-target="#edit-text-searchable-box-tag_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tag_id" class="form-control edit text-searchable tag_id" placeholder="Search Tag" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-tag_id" tabindex="-1" role="dialog" aria-labelledby="Tag" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Tag List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_tag_active" id="edit_directory_tags_dir_tag_active" class="edit_directory_tags_dir_tag_active directory_tags-input  table-directory_tags edit-table-directory_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_tags" id="update-back-directory_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_tags -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-directory_tags -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'directory_data',
		tables : { 
		<?php if( isset($admin_access->controller_directory_data) ) { ?>
		
'directory_data' : { label : 'Directory',
fields : ["dir_id","dir_name","dir_slug","dir_branch","dir_active","dir_verified","dir_added","dir_status","user_id"],
add_fields : ["dir_name","dir_branch","dir_active","dir_verified","user_id"],
edit_fields : ["dir_id","dir_name","dir_branch","dir_active","dir_verified"],
list_limit : 20,
list_fields : ["dir_name","dir_branch","dir_verified","dir_status","name"],
order_by : 'dir_name',
order_sort : 'ASC',
filters : {"dir_status":{"type":"manual","anchor":0}},
primary_key : 'dir_id',
primary_title : 'dir_name',
active_key : 'dir_active',
actual_values : {"user_id" : "name"},
check_cross : ["dir_branch","dir_verified"],
actions_edit : <?php echo ($admin_access->controller_directory_data->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_data->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_directory_details) ) { ?>
		
'directory_details' : { label : 'Detail',
fields : ["dir_d_id","dir_id","dir_d_key","dir_d_value","dir_d_label","dir_d_order","dir_d_active"],
add_fields : ["dir_id","dir_d_key","dir_d_value","dir_d_label","dir_d_order","dir_d_active"],
edit_fields : ["dir_d_id","dir_id","dir_d_key","dir_d_value","dir_d_label","dir_d_order","dir_d_active"],
list_limit : 20,
list_fields : ["dir_d_key","dir_d_value","dir_d_label","dir_d_order"],
order_by : 'dir_d_id',
order_sort : 'DESC',
filters : {"dir_d_key":{"type":"table","anchor":0,"table":"attributes","key":"attr_name","value":"attr_label", "filter" : 1, "filter_key" : "attr_group", "filter_value" : "directory_details", "order" : 1, "order_by" : "attr_name", "order_sort" : "ASC" }},
primary_key : 'dir_d_id',
primary_title : 'dir_d_key',
active_key : 'dir_d_active',
actual_values : {"dir_id" : "dir_name","dir_d_key" : "attr_label"},
required_key : 'dir_id',
required_value : 'dir_id',
required_table : 'directory_data',
actions_edit : <?php echo ($admin_access->controller_directory_details->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_details->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_directory_category) ) { ?>
		
'directory_category' : { label : 'Category',
fields : ["dir_cat_id","dir_id","tax_id","dir_cat_active"],
add_fields : ["dir_id","tax_id","dir_cat_active"],
edit_fields : ["dir_cat_id","dir_id","tax_id","dir_cat_active"],
list_limit : 20,
list_fields : ["tax_label"],
order_by : 'dir_cat_id',
order_sort : 'DESC',
primary_key : 'dir_cat_id',
primary_title : 'tax_id',
active_key : 'dir_cat_active',
actual_values : {"dir_id" : "dir_name","tax_id" : "tax_label"},
required_key : 'dir_id',
required_value : 'dir_id',
required_table : 'directory_data',
actions_edit : <?php echo ($admin_access->controller_directory_category->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_category->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_directory_location) ) { ?>
		
'directory_location' : { label : 'Location',
fields : ["dir_loc_id","dir_id","loc_id","dir_loc_primary","dir_loc_active"],
add_fields : ["dir_id","loc_id","dir_loc_primary","dir_loc_active"],
edit_fields : ["dir_loc_id","dir_id","loc_id","dir_loc_primary","dir_loc_active"],
list_limit : 20,
list_fields : ["loc_name","dir_loc_primary"],
order_by : 'dir_loc_id',
order_sort : 'DESC',
primary_key : 'dir_loc_id',
primary_title : 'loc_id',
active_key : 'dir_loc_active',
actual_values : {"dir_id" : "dir_name","loc_id" : "loc_name"},
check_cross : ["dir_loc_primary"],
required_key : 'dir_id',
required_value : 'dir_id',
required_table : 'directory_data',
actions_edit : <?php echo ($admin_access->controller_directory_location->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_location->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_directory_meta) ) { ?>
		
'directory_meta' : { label : 'Meta',
fields : ["dir_m_id","dir_id","meta_key","meta_value","active"],
add_fields : ["dir_id","meta_key","meta_value","active"],
edit_fields : ["dir_m_id","dir_id","meta_key","meta_value","active"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 'dir_m_id',
order_sort : 'DESC',
primary_key : 'dir_m_id',
primary_title : 'meta_key',
active_key : 'active',
required_key : 'dir_id',
required_value : 'dir_id',
required_table : 'directory_data',
actions_edit : <?php echo ($admin_access->controller_directory_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_directory_ancestors) ) { ?>
		
'directory_ancestors' : { label : 'Ancestor',
fields : ["dir_anc_id","dir_id","ancestor_id","dir_anc_active"],
add_fields : ["dir_id","ancestor_id","dir_anc_active"],
edit_fields : ["dir_anc_id","dir_id","ancestor_id","dir_anc_active"],
list_limit : 20,
list_fields : ["ancestor_id"],
order_by : 'dir_anc_id',
order_sort : 'DESC',
primary_key : 'dir_anc_id',
primary_title : 'dir_id',
active_key : 'dir_anc_active',
required_key : 'dir_id',
required_value : 'dir_id',
required_table : 'directory_data',
actions_edit : <?php echo ($admin_access->controller_directory_ancestors->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_ancestors->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_directory_media) ) { ?>
		
'directory_media' : { label : 'Media',
fields : ["dir_med_id","dir_id","dir_med_group","media_id","media_thumb","media_type","media_caption","media_link","media_name","dir_med_order","dir_med_active"],
add_fields : ["dir_id","dir_med_group","media_id","media_thumb","media_type","media_caption","media_link","media_name","dir_med_order","dir_med_active"],
edit_fields : ["dir_med_id","dir_id","dir_med_group","media_id","media_thumb","media_type","media_caption","media_link","media_name","dir_med_order","dir_med_active"],
list_limit : 20,
list_fields : ["dir_med_group","file_name","media_type","dir_med_order"],
order_by : 'dir_med_id',
order_sort : 'DESC',
filters : {"dir_med_group":{"type":"manual","anchor":0},"media_type":{"type":"manual","anchor":0}},
primary_key : 'dir_med_id',
primary_title : 'dir_id',
active_key : 'dir_med_active',
actual_values : {"dir_id" : "dir_name","media_id" : "file_name"},
required_key : 'dir_id',
required_value : 'dir_id',
required_table : 'directory_data',
actions_edit : <?php echo ($admin_access->controller_directory_media->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_media->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_directory_tags) ) { ?>
		
'directory_tags' : { label : 'Tag',
fields : ["dir_tag_id","dir_id","tag_id","dir_tag_active"],
add_fields : ["dir_id","tag_id","dir_tag_active"],
edit_fields : ["dir_tag_id","dir_id","tag_id","dir_tag_active"],
list_limit : 20,
list_fields : ["tag_name"],
order_by : 'dir_tag_id',
order_sort : 'DESC',
primary_key : 'dir_tag_id',
active_key : 'dir_tag_active',
actual_values : {"dir_id" : "dir_name","tag_id" : "tag_name"},
required_key : 'dir_id',
required_value : 'dir_id',
required_table : 'directory_data',
actions_edit : <?php echo ($admin_access->controller_directory_tags->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_tags->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"dir_loc_active": {"1": "Active"}, "dir_med_group": {"slider": "Slider", "gallery": "Gallery", "general": "General Media"}, "dir_status": {"user": "Users Only", "draft": "Draft", "private": "Private", "pending": "Approval Request", "publish": "Published"}, "dir_med_active": {"1": "Active"}, "dir_tag_active": {"1": "Active"}, "dir_d_active": {"1": "Active"}, "active": {"1": "Active"}, "dir_branch": {"1": "Branch"}, "dir_cat_active": {"1": "Active"}, "dir_verified": {"1": "Verified"}, "media_type": {"image": "Image", "video": "Video"}, "dir_active": {"1": "Active"}, "dir_loc_primary": {"1": "Primary"}, "dir_anc_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>