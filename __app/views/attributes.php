<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-attributes" class="list-view">
<div class="panel panel-default panel-attributes">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_attributes->can_add) && ($admin_access->controller_attributes->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-attributes">Add Attribute</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Name<span  data-key="attr_name" data-table="attributes" id="list_search_button_attr_name" class="btn btn-primary btn-xs pull-right btn-search list-search-attributes" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="">Label<span  data-key="attr_label" data-table="attributes" id="list_search_button_attr_label" class="btn btn-primary btn-xs pull-right btn-search list-search-attributes" title="Search Label">
		<i class="fa fa-search"></i></span></th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="attr_group" data-table="attributes">Group <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-attributes -->
</div>
		<?php if( isset($admin_access->controller_attributes->can_add) && ($admin_access->controller_attributes->can_add == 1) ) { ?>
		<div id="add-view-attributes" style="display:none">
<div class="panel panel-default add-panel-attributes">
                        <div class="panel-heading"><h3 class="panel-title">Add Attribute</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_attributes_attr_name">Name</label> 
<input data-type="text" type="text" name="attr_name" id="add_attributes_attr_name" class="form-control add_attributes_attr_name attributes-input  table-attributes add-table-attributes text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_attributes_attr_label">Label</label> 
<input data-type="text" type="text" name="attr_label" id="add_attributes_attr_label" class="form-control add_attributes_attr_label attributes-input  table-attributes add-table-attributes text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="add_attributes_attr_value">Default Value</label> 
<input data-type="text" type="text" name="attr_value" id="add_attributes_attr_value" class="form-control add_attributes_attr_value attributes-input  table-attributes add-table-attributes text text" placeholder="Default Value" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_attr_group">Group</label> 
			<select name="attr_group" id="add_attributes_attr_group" class="selectpicker form-control add_attributes_attr_group attributes-input  table-attributes add-table-attributes dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="directory_details">Directory Details</option>
<option value="locations">Locations</option>
<option value="points_redeemable_items">Points Redeemable Items</option>
<option value="realestate_property_type">Property Types</option>
<option value="realestate_details">Real Estate Details</option>
<option value="realestate_features">Real Estate Features</option>
<option value="settings">Settings</option>
<option value="taxonomy_type">Taxonomy Type</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="attr_active" id="add_attributes_attr_active" class="add_attributes_attr_active attributes-input  table-attributes add-table-attributes checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-attributes">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-attributes">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes -->
</div>
<?php } ?><?php if( isset($admin_access->controller_attributes->can_edit) && ($admin_access->controller_attributes->can_edit == 1) ) { ?>
		<div id="edit-view-attributes" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-attributes" id="update-back-attributes">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="attributes" data-child="0">Edit Attribute</a>
		</li>
			<?php if( isset($admin_access->controller_attributes_ancestors) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="attributes_ancestors" data-child="1">Ancestors</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_attributes_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="attributes_meta" data-child="1">Meta</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-attributes parent active"><div class="panel panel-default edit-panel-attributes">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Attribute</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="attr_id" id="edit_attributes_attr_id" class="edit_attributes_attr_id attributes-input  table-attributes edit-table-attributes hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_attributes_attr_name">Name</label> 
<input data-type="text" type="text" name="attr_name" id="edit_attributes_attr_name" class="form-control edit_attributes_attr_name attributes-input  table-attributes edit-table-attributes text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="edit_attributes_attr_label">Label</label> 
<input data-type="text" type="text" name="attr_label" id="edit_attributes_attr_label" class="form-control edit_attributes_attr_label attributes-input  table-attributes edit-table-attributes text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="edit_attributes_attr_value">Default Value</label> 
<input data-type="text" type="text" name="attr_value" id="edit_attributes_attr_value" class="form-control edit_attributes_attr_value attributes-input  table-attributes edit-table-attributes text text" placeholder="Default Value" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_attr_group">Group</label> 
			<select name="attr_group" id="edit_attributes_attr_group" class="selectpicker form-control edit_attributes_attr_group attributes-input  table-attributes edit-table-attributes dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="directory_details">Directory Details</option>
<option value="locations">Locations</option>
<option value="points_redeemable_items">Points Redeemable Items</option>
<option value="realestate_property_type">Property Types</option>
<option value="realestate_details">Real Estate Details</option>
<option value="realestate_features">Real Estate Features</option>
<option value="settings">Settings</option>
<option value="taxonomy_type">Taxonomy Type</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="attr_active" id="edit_attributes_attr_active" class="edit_attributes_attr_active attributes-input  table-attributes edit-table-attributes checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-attributes">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-attributes" id="update-back-attributes">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes -->
</div><!-- .tab-content .tab-content-attributes -->
			<?php if( isset($admin_access->controller_attributes_ancestors) ) { ?>
			<div class="tab-content tab-content-attributes_ancestors" style="display:none"><div id="list-view-attributes_ancestors" class="list-view">
<div class="panel panel-default panel-attributes_ancestors">
<div class="panel-heading">
&nbsp;
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>

</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-attributes_ancestors -->
</div>
		<?php if( isset($admin_access->controller_attributes_ancestors->can_add) && ($admin_access->controller_attributes_ancestors->can_add == 1) ) { ?>
		<div id="add-view-attributes_ancestors" style="display:none">
<div class="panel panel-default add-panel-attributes_ancestors">
                        <div class="panel-heading"><h3 class="panel-title">Add Ancestor</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-attributes_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-attributes_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes_ancestors -->
</div>
<?php } ?><?php if( isset($admin_access->controller_attributes_ancestors->can_edit) && ($admin_access->controller_attributes_ancestors->can_edit == 1) ) { ?>
		<div id="edit-view-attributes_ancestors" style="display:none">
		
		<div class="panel panel-default edit-panel-attributes_ancestors">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Ancestor</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-attributes_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-attributes_ancestors" id="update-back-attributes_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes_ancestors -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-attributes_ancestors -->
			<?php } ?>
			<?php if( isset($admin_access->controller_attributes_meta) ) { ?>
			<div class="tab-content tab-content-attributes_meta" style="display:none"><div id="list-view-attributes_meta" class="list-view">
<div class="panel panel-default panel-attributes_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_attributes_meta->can_add) && ($admin_access->controller_attributes_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-attributes_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Key</th><th width="None">Value</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-attributes_meta -->
</div>
		<?php if( isset($admin_access->controller_attributes_meta->can_add) && ($admin_access->controller_attributes_meta->can_add == 1) ) { ?>
		<div id="add-view-attributes_meta" style="display:none">
<div class="panel panel-default add-panel-attributes_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="attr_id" id="add_attributes_meta_attr_id" class="add_attributes_meta_attr_id attributes_meta-input  table-attributes_meta add-table-attributes_meta hidden text" placeholder="Attribute" value="" />
<div class="form-group">
<label for="add_attributes_meta_meta_key">Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_attributes_meta_meta_key" class="form-control add_attributes_meta_meta_key attributes_meta-input  table-attributes_meta add-table-attributes_meta text text" placeholder="Key" value=""/>
</div>
<div class="form-group">
<label for="add_attributes_meta_meta_value">Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_attributes_meta_meta_value" class="form-control add_attributes_meta_meta_value attributes_meta-input  table-attributes_meta add-table-attributes_meta textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_attributes_meta_active" class="add_attributes_meta_active attributes_meta-input  table-attributes_meta add-table-attributes_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-attributes_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-attributes_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_attributes_meta->can_edit) && ($admin_access->controller_attributes_meta->can_edit == 1) ) { ?>
		<div id="edit-view-attributes_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-attributes_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="attr_m_id" id="edit_attributes_meta_attr_m_id" class="edit_attributes_meta_attr_m_id attributes_meta-input  table-attributes_meta edit-table-attributes_meta hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="attr_id" id="edit_attributes_meta_attr_id" class="edit_attributes_meta_attr_id attributes_meta-input  table-attributes_meta edit-table-attributes_meta hidden text" placeholder="Attribute" value="" />
<div class="form-group">
<label for="edit_attributes_meta_meta_key">Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_attributes_meta_meta_key" class="form-control edit_attributes_meta_meta_key attributes_meta-input  table-attributes_meta edit-table-attributes_meta text text" placeholder="Key" value=""/>
</div>
<div class="form-group">
<label for="edit_attributes_meta_meta_value">Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_attributes_meta_meta_value" class="form-control edit_attributes_meta_meta_value attributes_meta-input  table-attributes_meta edit-table-attributes_meta textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_attributes_meta_active" class="edit_attributes_meta_active attributes_meta-input  table-attributes_meta edit-table-attributes_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-attributes_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-attributes_meta" id="update-back-attributes_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-attributes_meta -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'attributes',
		tables : { 
		<?php if( isset($admin_access->controller_attributes) ) { ?>
		
'attributes' : { label : 'Attribute',
fields : ["attr_id","attr_name","attr_label","attr_value","attr_group","attr_active"],
add_fields : ["attr_name","attr_label","attr_value","attr_group","attr_active"],
edit_fields : ["attr_id","attr_name","attr_label","attr_value","attr_group","attr_active"],
list_limit : 20,
list_fields : ["attr_name","attr_label","attr_group"],
order_by : 'attr_label',
order_sort : 'ASC',
filters : {"attr_group":{"type":"manual","anchor":0}},
primary_key : 'attr_id',
primary_title : 'attr_label',
active_key : 'attr_active',
actions_edit : <?php echo ($admin_access->controller_attributes->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_attributes->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_attributes_ancestors) ) { ?>
		
'attributes_ancestors' : { fields : ["attr_anc_id","attr_id","ancestor_id","attr_anc_active"],
required_key : 'attr_id',
required_value : 'attr_id',
required_table : 'attributes' },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_attributes_meta) ) { ?>
		
'attributes_meta' : { label : 'Attributes Meta',
fields : ["attr_m_id","attr_id","meta_key","meta_value","active"],
add_fields : ["attr_id","meta_key","meta_value","active"],
edit_fields : ["attr_m_id","attr_id","meta_key","meta_value","active"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 'meta_key',
order_sort : 'ASC',
primary_key : 'attr_m_id',
primary_title : 'meta_key',
active_key : 'active',
actual_values : {"attr_id" : "attr_label"},
required_key : 'attr_id',
required_value : 'attr_id',
required_table : 'attributes',
actions_edit : <?php echo ($admin_access->controller_attributes_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_attributes_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"active": {"1": "Active"}, "attr_active": {"1": "Active"}, "attr_group": {"settings": "Settings", "locations": "Locations", "points_redeemable_items": "Points Redeemable Items", "realestate_features": "Real Estate Features", "directory_details": "Directory Details", "realestate_details": "Real Estate Details", "taxonomy_type": "Taxonomy Type", "realestate_property_type": "Property Types"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>