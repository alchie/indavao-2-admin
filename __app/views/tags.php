<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-tags" class="list-view">
<div class="panel panel-default panel-tags">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_tags->can_add) && ($admin_access->controller_tags->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-tags">Add Tag</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="None">Name</th><th width="10%">Count</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-tags -->
</div>
		<?php if( isset($admin_access->controller_tags->can_add) && ($admin_access->controller_tags->can_add == 1) ) { ?>
		<div id="add-view-tags" style="display:none">
<div class="panel panel-default add-panel-tags">
                        <div class="panel-heading"><h3 class="panel-title">Add Tag</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_tags_tag_name">Name</label> 
<input data-type="text" type="text" name="tag_name" id="add_tags_tag_name" class="form-control add_tags_tag_name tags-input  table-tags add-table-tags text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_tags_tag_count">Count</label> 
<input data-type="text" type="text" name="tag_count" id="add_tags_tag_count" class="form-control add_tags_tag_count tags-input  table-tags add-table-tags text text" placeholder="Count" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tags -->
</div>
<?php } ?><?php if( isset($admin_access->controller_tags->can_edit) && ($admin_access->controller_tags->can_edit == 1) ) { ?>
		<div id="edit-view-tags" style="display:none">
		
		<div class="tab-content tab-content-tags parent active"><div class="panel panel-default edit-panel-tags">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Tag</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tag_id" id="edit_tags_tag_id" class="edit_tags_tag_id tags-input  table-tags edit-table-tags hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_tags_tag_name">Name</label> 
<input data-type="text" type="text" name="tag_name" id="edit_tags_tag_name" class="form-control edit_tags_tag_name tags-input  table-tags edit-table-tags text text" placeholder="Name" value=""/>
</div>
<div class="form-group change-deliberately button tags tag_slug">
<label>Slug</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update tags tag_slug" data-table="tags" data-field="tag_slug">Update Slug</a>
			<p class="text-value tags tag_slug edit_tags_tag_slug tags-input  table-tags edit-table-tags text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form tags tag_slug" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel tags tag_slug" data-table="tags" data-field="tag_slug">Cancel</a>
			Update Slug</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_tags_tag_slug">Slug</label> 
<input data-type="text" type="text" name="tag_slug" id="edit_tags_tag_slug" class="form-control edit_tags_tag_slug tags-input  table-tags edit-table-tags text " placeholder="Slug" value=""/>
</div></div>
</div>

<div class="form-group">
<label for="edit_tags_tag_count">Count</label> 
<input data-type="text" type="text" name="tag_count" id="edit_tags_tag_count" class="form-control edit_tags_tag_count tags-input  table-tags edit-table-tags text text" placeholder="Count" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-tags" id="update-back-tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tags -->
</div><!-- .tab-content .tab-content-tags --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'tags',
		tables : { 
		<?php if( isset($admin_access->controller_tags) ) { ?>
		
'tags' : { label : 'Tag',
fields : ["tag_id","tag_name","tag_slug","tag_count"],
add_fields : ["tag_name","tag_count"],
edit_fields : ["tag_id","tag_name","tag_count"],
list_limit : 20,
list_fields : ["tag_name","tag_count"],
order_by : 'tag_name',
order_sort : 'ASC',
primary_key : 'tag_id',
primary_title : 'tag_name',
actions_edit : <?php echo ($admin_access->controller_tags->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_tags->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>