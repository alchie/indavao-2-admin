<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-locations" class="list-view">
<div class="panel panel-default panel-locations">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_locations->can_add) && ($admin_access->controller_locations->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-locations">Add Location</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="5%">ID</th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="loc_type" data-table="locations">Type <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Name<span  data-key="loc_name" data-table="locations" id="list_search_button_loc_name" class="btn btn-primary btn-xs pull-right btn-search list-search-locations" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="5%">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-locations -->
</div>
		<?php if( isset($admin_access->controller_locations->can_add) && ($admin_access->controller_locations->can_add == 1) ) { ?>
		<div id="add-view-locations" style="display:none">
<div class="panel panel-default add-panel-locations">
                        <div class="panel-heading"><h3 class="panel-title">Add Location</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="loc_id" id="add_locations_loc_id" class="add_locations_loc_id locations-input  table-locations add-table-locations hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_lessons_loc_type">Type</label> 
			<select name="loc_type" id="add_locations_loc_type" class="selectpicker form-control add_locations_loc_type locations-input  table-locations add-table-locations dropdown text dropdown-table" placeholder="Type" data-live-search="true"  data-type="dropdown" data-label="Type" data-field="loc_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="locations" data-order="1" data-order-by="attr_label" data-order-sort="ASC">
			<option value="">- - Select Type - -</option>
</select></div>
<div class="form-group">
<label for="add_locations_loc_name">Name</label> 
<input data-type="text" type="text" name="loc_name" id="add_locations_loc_name" class="form-control add_locations_loc_name locations-input  table-locations add-table-locations text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_loc_order">Order</label> 
			<select name="loc_order" id="add_locations_loc_order" class="selectpicker form-control add_locations_loc_order locations-input  table-locations add-table-locations dropdown text" placeholder="Order" data-live-search="true" >
			<option value="">- - Select Order - -</option>
<option value="0">0</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="loc_active" id="add_locations_loc_active" class="add_locations_loc_active locations-input  table-locations add-table-locations checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-locations">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-locations">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-locations -->
</div>
<?php } ?><?php if( isset($admin_access->controller_locations->can_edit) && ($admin_access->controller_locations->can_edit == 1) ) { ?>
		<div id="edit-view-locations" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-locations" id="update-back-locations">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="locations" data-child="0">Edit Location</a>
		</li>
			<?php if( isset($admin_access->controller_locations_ancestors) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="locations_ancestors" data-child="1">Ancestors</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_locations_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="locations_meta" data-child="1">Meta Tags</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-locations parent active"><div class="panel panel-default edit-panel-locations">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Location</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="loc_id" id="edit_locations_loc_id" class="edit_locations_loc_id locations-input  table-locations edit-table-locations hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_lessons_loc_type">Type</label> 
			<select name="loc_type" id="edit_locations_loc_type" class="selectpicker form-control edit_locations_loc_type locations-input  table-locations edit-table-locations dropdown text dropdown-table" placeholder="Type" data-live-search="true"  data-type="dropdown" data-label="Type" data-field="loc_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="locations" data-order="1" data-order-by="attr_label" data-order-sort="ASC">
			<option value="">- - Select Type - -</option>
</select></div>
<div class="form-group">
<label for="edit_locations_loc_name">Name</label> 
<input data-type="text" type="text" name="loc_name" id="edit_locations_loc_name" class="form-control edit_locations_loc_name locations-input  table-locations edit-table-locations text text" placeholder="Name" value=""/>
</div>
<div class="form-group change-deliberately button locations loc_slug">
<label>Slug</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update locations loc_slug" data-table="locations" data-field="loc_slug">Update Slug</a>
			<p class="text-value locations loc_slug edit_locations_loc_slug locations-input  table-locations edit-table-locations text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form locations loc_slug" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel locations loc_slug" data-table="locations" data-field="loc_slug">Cancel</a>
			Update Slug</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_locations_loc_slug">Slug</label> 
<input data-type="text" type="text" name="loc_slug" id="edit_locations_loc_slug" class="form-control edit_locations_loc_slug locations-input  table-locations edit-table-locations text " placeholder="Slug" value=""/>
</div></div>
</div>

<div class="form-group">
<label for="add_lessons_loc_order">Order</label> 
			<select name="loc_order" id="edit_locations_loc_order" class="selectpicker form-control edit_locations_loc_order locations-input  table-locations edit-table-locations dropdown text" placeholder="Order" data-live-search="true" >
			<option value="">- - Select Order - -</option>
<option value="0">0</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="loc_active" id="edit_locations_loc_active" class="edit_locations_loc_active locations-input  table-locations edit-table-locations checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-locations">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-locations" id="update-back-locations">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-locations -->
</div><!-- .tab-content .tab-content-locations -->
			<?php if( isset($admin_access->controller_locations_ancestors) ) { ?>
			<div class="tab-content tab-content-locations_ancestors" style="display:none"><div id="list-view-locations_ancestors" class="list-view">
<div class="panel panel-default panel-locations_ancestors">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_locations_ancestors->can_add) && ($admin_access->controller_locations_ancestors->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-locations_ancestors">Add Ancestor</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Parent Location<span data-linked='locations' data-key="loc_name_2" data-table="locations_ancestors" id="list_search_button_loc_name_2" class="btn btn-primary btn-xs pull-right btn-search list-search-locations_ancestors" title="Search Parent Location">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-locations_ancestors -->
</div>
		<?php if( isset($admin_access->controller_locations_ancestors->can_add) && ($admin_access->controller_locations_ancestors->can_add == 1) ) { ?>
		<div id="add-view-locations_ancestors" style="display:none">
<div class="panel panel-default add-panel-locations_ancestors">
                        <div class="panel-heading"><h3 class="panel-title">Add Ancestor</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="loc_id" id="add_locations_ancestors_loc_id" class="add_locations_ancestors_loc_id locations_ancestors-input  table-locations_ancestors add-table-locations_ancestors hidden text" placeholder="Location" value="" />
<div class="form-group">
<label for="add_locations_ancestors_loc_parent">Parent Location</label> 
<input data-type="text" type="hidden" name="loc_parent" id="add_locations_ancestors_loc_parent" class="form-control add_locations_ancestors_loc_parent locations_ancestors-input  table-locations_ancestors add-table-locations_ancestors text text text-searchable-key-loc_parent  add text-searchable-key" />
<a href="javascript:void(0)" data-field="loc_parent"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_2" data-action="add"  class="text-searchable-list loc_parent" data-toggle="modal" data-target="#add-text-searchable-box-loc_parent"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="loc_parent" class="form-control add text-searchable loc_parent" placeholder="Search Parent Location" data-field="loc_parent"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_2" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-loc_parent" tabindex="-1" role="dialog" aria-labelledby="Parent Location" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Parent Location List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="loc_anc_active" id="add_locations_ancestors_loc_anc_active" class="add_locations_ancestors_loc_anc_active locations_ancestors-input  table-locations_ancestors add-table-locations_ancestors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-locations_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-locations_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-locations_ancestors -->
</div>
<?php } ?><?php if( isset($admin_access->controller_locations_ancestors->can_edit) && ($admin_access->controller_locations_ancestors->can_edit == 1) ) { ?>
		<div id="edit-view-locations_ancestors" style="display:none">
		
		<div class="panel panel-default edit-panel-locations_ancestors">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Ancestor</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="loc_anc_id" id="edit_locations_ancestors_loc_anc_id" class="edit_locations_ancestors_loc_anc_id locations_ancestors-input  table-locations_ancestors edit-table-locations_ancestors hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="loc_id" id="edit_locations_ancestors_loc_id" class="edit_locations_ancestors_loc_id locations_ancestors-input  table-locations_ancestors edit-table-locations_ancestors hidden " placeholder="Location" value="" />
<div class="form-group">
<label for="edit_locations_ancestors_loc_parent">Parent Location</label> 
<input data-type="text" type="hidden" name="loc_parent" id="edit_locations_ancestors_loc_parent" class="form-control edit_locations_ancestors_loc_parent locations_ancestors-input  table-locations_ancestors edit-table-locations_ancestors text text text-searchable-key-loc_parent  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="loc_parent"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_2" data-action="edit"  class="text-searchable-list loc_parent" data-toggle="modal" data-target="#edit-text-searchable-box-loc_parent"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="loc_parent" class="form-control edit text-searchable loc_parent" placeholder="Search Parent Location" data-field="loc_parent"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_2" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-loc_parent" tabindex="-1" role="dialog" aria-labelledby="Parent Location" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Parent Location List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="loc_anc_active" id="edit_locations_ancestors_loc_anc_active" class="edit_locations_ancestors_loc_anc_active locations_ancestors-input  table-locations_ancestors edit-table-locations_ancestors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-locations_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-locations_ancestors" id="update-back-locations_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-locations_ancestors -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-locations_ancestors -->
			<?php } ?>
			<?php if( isset($admin_access->controller_locations_meta) ) { ?>
			<div class="tab-content tab-content-locations_meta" style="display:none"><div id="list-view-locations_meta" class="list-view">
<div class="panel panel-default panel-locations_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_locations_meta->can_add) && ($admin_access->controller_locations_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-locations_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="None">Meta Key</th><th width="None">Meta Value</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-locations_meta -->
</div>
		<?php if( isset($admin_access->controller_locations_meta->can_add) && ($admin_access->controller_locations_meta->can_add == 1) ) { ?>
		<div id="add-view-locations_meta" style="display:none">
<div class="panel panel-default add-panel-locations_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="loc_id" id="add_locations_meta_loc_id" class="add_locations_meta_loc_id locations_meta-input  table-locations_meta add-table-locations_meta hidden text" placeholder="Location" value="" />
<div class="form-group">
<label for="add_locations_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_locations_meta_meta_key" class="form-control add_locations_meta_meta_key locations_meta-input  table-locations_meta add-table-locations_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="add_locations_meta_meta_value">Meta Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_locations_meta_meta_value" class="form-control add_locations_meta_meta_value locations_meta-input  table-locations_meta add-table-locations_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="loc_m_active" id="add_locations_meta_loc_m_active" class="add_locations_meta_loc_m_active locations_meta-input  table-locations_meta add-table-locations_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-locations_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-locations_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-locations_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_locations_meta->can_edit) && ($admin_access->controller_locations_meta->can_edit == 1) ) { ?>
		<div id="edit-view-locations_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-locations_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="loc_m_id" id="edit_locations_meta_loc_m_id" class="edit_locations_meta_loc_m_id locations_meta-input  table-locations_meta edit-table-locations_meta hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="loc_id" id="edit_locations_meta_loc_id" class="edit_locations_meta_loc_id locations_meta-input  table-locations_meta edit-table-locations_meta hidden " placeholder="Location" value="" />
<div class="form-group">
<label for="edit_locations_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_locations_meta_meta_key" class="form-control edit_locations_meta_meta_key locations_meta-input  table-locations_meta edit-table-locations_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="edit_locations_meta_meta_value">Meta Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_locations_meta_meta_value" class="form-control edit_locations_meta_meta_value locations_meta-input  table-locations_meta edit-table-locations_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="loc_m_active" id="edit_locations_meta_loc_m_active" class="edit_locations_meta_loc_m_active locations_meta-input  table-locations_meta edit-table-locations_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-locations_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-locations_meta" id="update-back-locations_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-locations_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-locations_meta -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'locations',
		tables : { 
		<?php if( isset($admin_access->controller_locations) ) { ?>
		
'locations' : { label : 'Location',
fields : ["loc_id","loc_type","loc_name","loc_slug","loc_order","loc_active"],
add_fields : ["loc_id","loc_type","loc_name","loc_order","loc_active"],
edit_fields : ["loc_id","loc_type","loc_name","loc_order","loc_active"],
list_limit : 20,
list_fields : ["loc_id","loc_type","loc_name","loc_order"],
order_by : 'loc_name',
order_sort : 'ASC',
filters : {"loc_type":{"type":"table","anchor":0,"table":"attributes","key":"attr_name","value":"attr_label", "filter" : 1, "filter_key" : "attr_group", "filter_value" : "locations", "order" : 1, "order_by" : "attr_label", "order_sort" : "ASC" }},
primary_key : 'loc_id',
primary_title : 'loc_name',
active_key : 'loc_active',
actual_values : {"loc_type" : "attr_label"},
actions_edit : <?php echo ($admin_access->controller_locations->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_locations->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_locations_ancestors) ) { ?>
		
'locations_ancestors' : { label : 'Ancestor',
fields : ["loc_anc_id","loc_id","loc_parent","loc_anc_active"],
add_fields : ["loc_id","loc_parent","loc_anc_active"],
edit_fields : ["loc_anc_id","loc_id","loc_parent","loc_anc_active"],
list_limit : 20,
list_fields : ["loc_name_2"],
order_by : 'loc_anc_id',
order_sort : 'ASC',
primary_key : 'loc_anc_id',
active_key : 'loc_anc_active',
actual_values : {"loc_id" : "loc_name","loc_parent" : "loc_name_2"},
required_key : 'loc_id',
required_value : 'loc_id',
required_table : 'locations',
actions_edit : <?php echo ($admin_access->controller_locations_ancestors->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_locations_ancestors->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_locations_meta) ) { ?>
		
'locations_meta' : { label : 'Meta',
fields : ["loc_m_id","loc_id","meta_key","meta_value","loc_m_active"],
add_fields : ["loc_id","meta_key","meta_value","loc_m_active"],
edit_fields : ["loc_m_id","loc_id","meta_key","meta_value","loc_m_active"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 'meta_key',
order_sort : 'ASC',
primary_key : 'loc_m_id',
primary_title : 'meta_key',
actual_values : {"loc_id" : "loc_name"},
required_key : 'loc_id',
required_value : 'loc_id',
required_table : 'locations',
actions_edit : <?php echo ($admin_access->controller_locations_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_locations_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"loc_anc_active": {"1": "Active"}, "loc_active": {"1": "Active"}, "loc_order": {"1": "1", "0": "0", "3": "3", "2": "2", "5": "5", "4": "4", "7": "7", "6": "6", "9": "9", "8": "8"}, "loc_m_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>