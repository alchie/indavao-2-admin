<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-directory_tags" class="list-view">
<div class="panel panel-default panel-directory_tags">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_directory_tags->can_add) && ($admin_access->controller_directory_tags->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-directory_tags">Add Tag</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Directory<span data-linked='directory_data' data-key="dir_name" data-table="directory_tags" id="list_search_button_dir_name" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_tags" title="Search Directory">
		<i class="fa fa-search"></i></span></th><th width="">Tag<span data-linked='tags' data-key="tag_name" data-table="directory_tags" id="list_search_button_tag_name" class="btn btn-primary btn-xs pull-right btn-search list-search-directory_tags" title="Search Tag">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-directory_tags -->
</div>
		<?php if( isset($admin_access->controller_directory_tags->can_add) && ($admin_access->controller_directory_tags->can_add == 1) ) { ?>
		<div id="add-view-directory_tags" style="display:none">
<div class="panel panel-default add-panel-directory_tags">
                        <div class="panel-heading"><h3 class="panel-title">Add Tag</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_directory_tags_dir_id">Directory</label> 
<input data-type="text" type="hidden" name="dir_id" id="add_directory_tags_dir_id" class="form-control add_directory_tags_dir_id directory_tags-input  table-directory_tags add-table-directory_tags text text text-searchable-key-dir_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="dir_id"  data-table="directory_data" data-key="dir_id" data-value="dir_name" data-display="dir_name" data-action="add"  class="text-searchable-list dir_id" data-toggle="modal" data-target="#add-text-searchable-box-dir_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="dir_id" class="form-control add text-searchable dir_id" placeholder="Search Directory" data-field="dir_id"  data-table="directory_data" data-key="dir_id" data-value="dir_name" data-display="dir_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-dir_id" tabindex="-1" role="dialog" aria-labelledby="Directory" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Directory List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_directory_tags_tag_id">Tag</label> 
<input data-type="text" type="hidden" name="tag_id" id="add_directory_tags_tag_id" class="form-control add_directory_tags_tag_id directory_tags-input  table-directory_tags add-table-directory_tags text text text-searchable-key-tag_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="add"  class="text-searchable-list tag_id" data-toggle="modal" data-target="#add-text-searchable-box-tag_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tag_id" class="form-control add text-searchable tag_id" placeholder="Search Tag" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-tag_id" tabindex="-1" role="dialog" aria-labelledby="Tag" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Tag List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_tag_active" id="add_directory_tags_dir_tag_active" class="add_directory_tags_dir_tag_active directory_tags-input  table-directory_tags add-table-directory_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-directory_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-directory_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_tags -->
</div>
<?php } ?><?php if( isset($admin_access->controller_directory_tags->can_edit) && ($admin_access->controller_directory_tags->can_edit == 1) ) { ?>
		<div id="edit-view-directory_tags" style="display:none">
		
		<div class="tab-content tab-content-directory_tags parent active"><div class="panel panel-default edit-panel-directory_tags">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Tag</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="dir_tag_id" id="edit_directory_tags_dir_tag_id" class="edit_directory_tags_dir_tag_id directory_tags-input  table-directory_tags edit-table-directory_tags hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_directory_tags_dir_id">Directory</label> 
<input data-type="text" type="hidden" name="dir_id" id="edit_directory_tags_dir_id" class="form-control edit_directory_tags_dir_id directory_tags-input  table-directory_tags edit-table-directory_tags text text text-searchable-key-dir_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="dir_id"  data-table="directory_data" data-key="dir_id" data-value="dir_name" data-display="dir_name" data-action="edit"  class="text-searchable-list dir_id" data-toggle="modal" data-target="#edit-text-searchable-box-dir_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="dir_id" class="form-control edit text-searchable dir_id" placeholder="Search Directory" data-field="dir_id"  data-table="directory_data" data-key="dir_id" data-value="dir_name" data-display="dir_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-dir_id" tabindex="-1" role="dialog" aria-labelledby="Directory" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Directory List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="edit_directory_tags_tag_id">Tag</label> 
<input data-type="text" type="hidden" name="tag_id" id="edit_directory_tags_tag_id" class="form-control edit_directory_tags_tag_id directory_tags-input  table-directory_tags edit-table-directory_tags text text text-searchable-key-tag_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="edit"  class="text-searchable-list tag_id" data-toggle="modal" data-target="#edit-text-searchable-box-tag_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tag_id" class="form-control edit text-searchable tag_id" placeholder="Search Tag" data-field="tag_id"  data-table="tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-tag_id" tabindex="-1" role="dialog" aria-labelledby="Tag" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Tag List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="dir_tag_active" id="edit_directory_tags_dir_tag_active" class="edit_directory_tags_dir_tag_active directory_tags-input  table-directory_tags edit-table-directory_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-directory_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-directory_tags" id="update-back-directory_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-directory_tags -->
</div><!-- .tab-content .tab-content-directory_tags --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'directory_tags',
		tables : { 
		<?php if( isset($admin_access->controller_directory_tags) ) { ?>
		
'directory_tags' : { label : 'Tag',
fields : ["dir_tag_id","dir_id","tag_id","dir_tag_active"],
add_fields : ["dir_id","tag_id","dir_tag_active"],
edit_fields : ["dir_tag_id","dir_id","tag_id","dir_tag_active"],
list_limit : 20,
list_fields : ["dir_name","tag_name"],
order_by : 'dir_tag_id',
order_sort : 'DESC',
primary_key : 'dir_tag_id',
active_key : 'dir_tag_active',
actual_values : {"dir_id" : "dir_name","tag_id" : "tag_name"},
actions_edit : <?php echo ($admin_access->controller_directory_tags->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_directory_tags->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"dir_tag_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>