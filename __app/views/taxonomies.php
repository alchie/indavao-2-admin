<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-taxonomies" class="list-view">
<div class="panel panel-default panel-taxonomies">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_taxonomies->can_add) && ($admin_access->controller_taxonomies->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-taxonomies">Add Taxonomy</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">ID</th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="tax_type" data-table="taxonomies">Type <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Label<span  data-key="tax_label" data-table="taxonomies" id="list_search_button_tax_label" class="btn btn-primary btn-xs pull-right btn-search list-search-taxonomies" title="Search Label">
		<i class="fa fa-search"></i></span></th><th width="">Name<span  data-key="tax_name" data-table="taxonomies" id="list_search_button_tax_name" class="btn btn-primary btn-xs pull-right btn-search list-search-taxonomies" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-taxonomies -->
</div>
		<?php if( isset($admin_access->controller_taxonomies->can_add) && ($admin_access->controller_taxonomies->can_add == 1) ) { ?>
		<div id="add-view-taxonomies" style="display:none">
<div class="panel panel-default add-panel-taxonomies">
                        <div class="panel-heading"><h3 class="panel-title">Add Taxonomy</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_lessons_tax_type">Type</label> 
			<select name="tax_type" id="add_taxonomies_tax_type" class="selectpicker form-control add_taxonomies_tax_type taxonomies-input  table-taxonomies add-table-taxonomies dropdown text dropdown-table" placeholder="Type" data-live-search="true"  data-type="dropdown" data-label="Type" data-field="tax_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="taxonomy_type" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Type - -</option>
</select></div>
<div class="form-group">
<label for="add_taxonomies_tax_label">Label</label> 
<input data-type="text" type="text" name="tax_label" id="add_taxonomies_tax_label" class="form-control add_taxonomies_tax_label taxonomies-input  table-taxonomies add-table-taxonomies text text" placeholder="Label" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tax_active" id="add_taxonomies_tax_active" class="add_taxonomies_tax_active taxonomies-input  table-taxonomies add-table-taxonomies checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-taxonomies">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-taxonomies">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-taxonomies -->
</div>
<?php } ?><?php if( isset($admin_access->controller_taxonomies->can_edit) && ($admin_access->controller_taxonomies->can_edit == 1) ) { ?>
		<div id="edit-view-taxonomies" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-taxonomies" id="update-back-taxonomies">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="taxonomies" data-child="0">Edit Taxonomy</a>
		</li>
			<?php if( isset($admin_access->controller_taxonomies_ancestors) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="taxonomies_ancestors" data-child="1">Ancestors</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_taxonomies_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="taxonomies_meta" data-child="1">Meta</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-taxonomies parent active"><div class="panel panel-default edit-panel-taxonomies">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Taxonomy</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tax_id" id="edit_taxonomies_tax_id" class="edit_taxonomies_tax_id taxonomies-input  table-taxonomies edit-table-taxonomies hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_lessons_tax_type">Type</label> 
			<select name="tax_type" id="edit_taxonomies_tax_type" class="selectpicker form-control edit_taxonomies_tax_type taxonomies-input  table-taxonomies edit-table-taxonomies dropdown text dropdown-table" placeholder="Type" data-live-search="true"  data-type="dropdown" data-label="Type" data-field="tax_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="taxonomy_type" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Type - -</option>
</select></div>
<div class="form-group">
<label for="edit_taxonomies_tax_label">Label</label> 
<input data-type="text" type="text" name="tax_label" id="edit_taxonomies_tax_label" class="form-control edit_taxonomies_tax_label taxonomies-input  table-taxonomies edit-table-taxonomies text text" placeholder="Label" value=""/>
</div>
<div class="form-group change-deliberately button taxonomies tax_name">
<label>Name</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update taxonomies tax_name" data-table="taxonomies" data-field="tax_name">Update Name</a>
			<p class="text-value taxonomies tax_name edit_taxonomies_tax_name taxonomies-input  table-taxonomies edit-table-taxonomies text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form taxonomies tax_name" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel taxonomies tax_name" data-table="taxonomies" data-field="tax_name">Cancel</a>
			Update Name</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_taxonomies_tax_name">Name</label> 
<input data-type="text" type="text" name="tax_name" id="edit_taxonomies_tax_name" class="form-control edit_taxonomies_tax_name taxonomies-input  table-taxonomies edit-table-taxonomies text " placeholder="Name" value=""/>
</div></div>
</div>

<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tax_active" id="edit_taxonomies_tax_active" class="edit_taxonomies_tax_active taxonomies-input  table-taxonomies edit-table-taxonomies checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-taxonomies">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-taxonomies" id="update-back-taxonomies">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-taxonomies -->
</div><!-- .tab-content .tab-content-taxonomies -->
			<?php if( isset($admin_access->controller_taxonomies_ancestors) ) { ?>
			<div class="tab-content tab-content-taxonomies_ancestors" style="display:none"><div id="list-view-taxonomies_ancestors" class="list-view">
<div class="panel panel-default panel-taxonomies_ancestors">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_taxonomies_ancestors->can_add) && ($admin_access->controller_taxonomies_ancestors->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-taxonomies_ancestors">Add Ancestor</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Parent Taxonomy<span data-linked='taxonomies' data-key="tax_label" data-table="taxonomies_ancestors" id="list_search_button_tax_label" class="btn btn-primary btn-xs pull-right btn-search list-search-taxonomies_ancestors" title="Search Parent Taxonomy">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-taxonomies_ancestors -->
</div>
		<?php if( isset($admin_access->controller_taxonomies_ancestors->can_add) && ($admin_access->controller_taxonomies_ancestors->can_add == 1) ) { ?>
		<div id="add-view-taxonomies_ancestors" style="display:none">
<div class="panel panel-default add-panel-taxonomies_ancestors">
                        <div class="panel-heading"><h3 class="panel-title">Add Ancestor</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tax_id" id="add_taxonomies_ancestors_tax_id" class="add_taxonomies_ancestors_tax_id taxonomies_ancestors-input  table-taxonomies_ancestors add-table-taxonomies_ancestors hidden text" placeholder="Taxonomy" value="" />
<div class="form-group">
<label for="add_taxonomies_ancestors_tax_parent">Parent Taxonomy</label> 
<input data-type="text" type="hidden" name="tax_parent" id="add_taxonomies_ancestors_tax_parent" class="form-control add_taxonomies_ancestors_tax_parent taxonomies_ancestors-input  table-taxonomies_ancestors add-table-taxonomies_ancestors text text text-searchable-key-tax_parent  add text-searchable-key" />
<a href="javascript:void(0)" data-field="tax_parent"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="add"  class="text-searchable-list tax_parent" data-toggle="modal" data-target="#add-text-searchable-box-tax_parent"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tax_parent" class="form-control add text-searchable tax_parent" placeholder="Search Parent Taxonomy" data-field="tax_parent"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-tax_parent" tabindex="-1" role="dialog" aria-labelledby="Parent Taxonomy" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Parent Taxonomy List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tax_anc_active" id="add_taxonomies_ancestors_tax_anc_active" class="add_taxonomies_ancestors_tax_anc_active taxonomies_ancestors-input  table-taxonomies_ancestors add-table-taxonomies_ancestors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-taxonomies_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-taxonomies_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-taxonomies_ancestors -->
</div>
<?php } ?><?php if( isset($admin_access->controller_taxonomies_ancestors->can_edit) && ($admin_access->controller_taxonomies_ancestors->can_edit == 1) ) { ?>
		<div id="edit-view-taxonomies_ancestors" style="display:none">
		
		<div class="panel panel-default edit-panel-taxonomies_ancestors">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Ancestor</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tax_anc_id" id="edit_taxonomies_ancestors_tax_anc_id" class="edit_taxonomies_ancestors_tax_anc_id taxonomies_ancestors-input  table-taxonomies_ancestors edit-table-taxonomies_ancestors hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="tax_id" id="edit_taxonomies_ancestors_tax_id" class="edit_taxonomies_ancestors_tax_id taxonomies_ancestors-input  table-taxonomies_ancestors edit-table-taxonomies_ancestors hidden text" placeholder="Taxonomy" value="" />
<div class="form-group">
<label for="edit_taxonomies_ancestors_tax_parent">Parent Taxonomy</label> 
<input data-type="text" type="hidden" name="tax_parent" id="edit_taxonomies_ancestors_tax_parent" class="form-control edit_taxonomies_ancestors_tax_parent taxonomies_ancestors-input  table-taxonomies_ancestors edit-table-taxonomies_ancestors text text text-searchable-key-tax_parent  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="tax_parent"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="edit"  class="text-searchable-list tax_parent" data-toggle="modal" data-target="#edit-text-searchable-box-tax_parent"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tax_parent" class="form-control edit text-searchable tax_parent" placeholder="Search Parent Taxonomy" data-field="tax_parent"  data-table="taxonomies" data-key="tax_id" data-value="tax_label" data-display="tax_label" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-tax_parent" tabindex="-1" role="dialog" aria-labelledby="Parent Taxonomy" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Parent Taxonomy List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tax_anc_active" id="edit_taxonomies_ancestors_tax_anc_active" class="edit_taxonomies_ancestors_tax_anc_active taxonomies_ancestors-input  table-taxonomies_ancestors edit-table-taxonomies_ancestors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-taxonomies_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-taxonomies_ancestors" id="update-back-taxonomies_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-taxonomies_ancestors -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-taxonomies_ancestors -->
			<?php } ?>
			<?php if( isset($admin_access->controller_taxonomies_meta) ) { ?>
			<div class="tab-content tab-content-taxonomies_meta" style="display:none"><div id="list-view-taxonomies_meta" class="list-view">
<div class="panel panel-default panel-taxonomies_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_taxonomies_meta->can_add) && ($admin_access->controller_taxonomies_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-taxonomies_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Meta Key</th><th width="None">Meta Value</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-taxonomies_meta -->
</div>
		<?php if( isset($admin_access->controller_taxonomies_meta->can_add) && ($admin_access->controller_taxonomies_meta->can_add == 1) ) { ?>
		<div id="add-view-taxonomies_meta" style="display:none">
<div class="panel panel-default add-panel-taxonomies_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tax_id" id="add_taxonomies_meta_tax_id" class="add_taxonomies_meta_tax_id taxonomies_meta-input  table-taxonomies_meta add-table-taxonomies_meta hidden text" placeholder="Taxonomy" value="" />
<div class="form-group">
<label for="add_taxonomies_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_taxonomies_meta_meta_key" class="form-control add_taxonomies_meta_meta_key taxonomies_meta-input  table-taxonomies_meta add-table-taxonomies_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="add_taxonomies_meta_meta_value">Meta Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_taxonomies_meta_meta_value" class="form-control add_taxonomies_meta_meta_value taxonomies_meta-input  table-taxonomies_meta add-table-taxonomies_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tax_m_active" id="add_taxonomies_meta_tax_m_active" class="add_taxonomies_meta_tax_m_active taxonomies_meta-input  table-taxonomies_meta add-table-taxonomies_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-taxonomies_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-taxonomies_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-taxonomies_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_taxonomies_meta->can_edit) && ($admin_access->controller_taxonomies_meta->can_edit == 1) ) { ?>
		<div id="edit-view-taxonomies_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-taxonomies_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tax_m_id" id="edit_taxonomies_meta_tax_m_id" class="edit_taxonomies_meta_tax_m_id taxonomies_meta-input  table-taxonomies_meta edit-table-taxonomies_meta hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="tax_id" id="edit_taxonomies_meta_tax_id" class="edit_taxonomies_meta_tax_id taxonomies_meta-input  table-taxonomies_meta edit-table-taxonomies_meta hidden text" placeholder="Taxonomy" value="" />
<div class="form-group">
<label for="edit_taxonomies_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_taxonomies_meta_meta_key" class="form-control edit_taxonomies_meta_meta_key taxonomies_meta-input  table-taxonomies_meta edit-table-taxonomies_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="edit_taxonomies_meta_meta_value">Meta Value</label>
<textarea rows="15" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_taxonomies_meta_meta_value" class="form-control edit_taxonomies_meta_meta_value taxonomies_meta-input  table-taxonomies_meta edit-table-taxonomies_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tax_m_active" id="edit_taxonomies_meta_tax_m_active" class="edit_taxonomies_meta_tax_m_active taxonomies_meta-input  table-taxonomies_meta edit-table-taxonomies_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-taxonomies_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-taxonomies_meta" id="update-back-taxonomies_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-taxonomies_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-taxonomies_meta -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'taxonomies',
		tables : { 
		<?php if( isset($admin_access->controller_taxonomies) ) { ?>
		
'taxonomies' : { label : 'Taxonomy',
fields : ["tax_id","tax_type","tax_label","tax_name","tax_active"],
add_fields : ["tax_type","tax_label","tax_active"],
edit_fields : ["tax_id","tax_type","tax_label","tax_active"],
list_limit : 20,
list_fields : ["tax_id","tax_type","tax_label","tax_name"],
order_by : 'tax_name',
order_sort : 'ASC',
filters : {"tax_type":{"type":"table","anchor":0,"table":"attributes","key":"attr_name","value":"attr_label", "filter" : 1, "filter_key" : "attr_group", "filter_value" : "taxonomy_type", "order" : 1, "order_by" : "attr_name", "order_sort" : "ASC" }},
primary_key : 'tax_id',
primary_title : 'tax_label',
active_key : 'tax_active',
actual_values : {"tax_type" : "attr_label"},
actions_edit : <?php echo ($admin_access->controller_taxonomies->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_taxonomies->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_taxonomies_ancestors) ) { ?>
		
'taxonomies_ancestors' : { label : 'Ancestor',
fields : ["tax_anc_id","tax_id","tax_parent","tax_anc_active"],
add_fields : ["tax_id","tax_parent","tax_anc_active"],
edit_fields : ["tax_anc_id","tax_id","tax_parent","tax_anc_active"],
list_limit : 20,
list_fields : ["tax_label"],
order_by : 'tax_anc_id',
order_sort : 'DESC',
primary_key : 'tax_anc_id',
active_key : 'tax_anc_active',
actual_values : {"tax_id" : "tax_label","tax_parent" : "tax_label"},
required_key : 'tax_id',
required_value : 'tax_id',
required_table : 'taxonomies',
actions_edit : <?php echo ($admin_access->controller_taxonomies_ancestors->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_taxonomies_ancestors->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_taxonomies_meta) ) { ?>
		
'taxonomies_meta' : { label : 'Meta',
fields : ["tax_m_id","tax_id","meta_key","meta_value","tax_m_active"],
add_fields : ["tax_id","meta_key","meta_value","tax_m_active"],
edit_fields : ["tax_m_id","tax_id","meta_key","meta_value","tax_m_active"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 'meta_key',
order_sort : 'ASC',
primary_key : 'tax_m_id',
primary_title : 'meta_key',
active_key : 'tax_m_active',
actual_values : {"tax_id" : "tax_name"},
required_key : 'tax_id',
required_value : 'tax_id',
required_table : 'taxonomies',
actions_edit : <?php echo ($admin_access->controller_taxonomies_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_taxonomies_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"tax_m_active": {"1": "Active"}, "tax_anc_active": {"1": "Active"}, "tax_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>