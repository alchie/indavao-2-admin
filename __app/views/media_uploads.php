<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-media_uploads" class="list-view">
<div class="panel panel-default panel-media_uploads">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_media_uploads->can_add) && ($admin_access->controller_media_uploads->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-media_uploads">Upload Media File</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="100px">Media ID</th><th width="">Filename<span  data-key="file_name" data-table="media_uploads" id="list_search_button_file_name" class="btn btn-primary btn-xs pull-right btn-search list-search-media_uploads" title="Search Filename">
		<i class="fa fa-search"></i></span></th><th width="">File Type<span  data-key="file_type" data-table="media_uploads" id="list_search_button_file_type" class="btn btn-primary btn-xs pull-right btn-search list-search-media_uploads" title="Search File Type">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-media_uploads -->
</div>
		<?php if( isset($admin_access->controller_media_uploads->can_add) && ($admin_access->controller_media_uploads->can_add == 1) ) { ?>
		<div id="add-view-media_uploads" style="display:none">
<div class="panel panel-default add-panel-media_uploads">
                        <div class="panel-heading">
 <div class="btn btn-success btn-xs btn-file pull-right" id="input-file-media_uploads">
    Select Media Files<input type="file" multiple>
  </div>
<h3 class="panel-title">Upload Media Files</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

			<div class="row" id="fileupload-media_uploads-preview" style="margin-top:20px;min-height:200px;"></div>
			
</div> <!-- .panel-body -->

<div class="panel-footer">
<button class="btn btn-success btn-sm action-button" id="upload-action-media_uploads" data-table="media_uploads">Upload All</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm upload-back-button" id="upload-back-media_uploads">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-media_uploads -->
</div>
<?php } ?><?php if( isset($admin_access->controller_media_uploads->can_edit) && ($admin_access->controller_media_uploads->can_edit == 1) ) { ?>
		<div id="edit-view-media_uploads" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-media_uploads" id="update-back-media_uploads">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="media_uploads" data-child="0">Edit Media File</a>
		</li>
			<?php if( isset($admin_access->controller_realestate_media) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_media" data-child="1">Real Estate Media</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-media_uploads parent active"><div class="panel panel-default edit-panel-media_uploads">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Media File</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="media_id" id="edit_media_uploads_media_id" class="edit_media_uploads_media_id media_uploads-input  table-media_uploads edit-table-media_uploads hidden text" placeholder="Media ID" value="" />
<div class="form-group">
<label for="edit_media_uploads_file_name">Filename</label> 
<input data-type="text" type="text" name="file_name" id="edit_media_uploads_file_name" class="form-control edit_media_uploads_file_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Filename" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_type">File Type</label> 
<input data-type="text" type="text" name="file_type" id="edit_media_uploads_file_type" class="form-control edit_media_uploads_file_type media_uploads-input  table-media_uploads edit-table-media_uploads text " placeholder="File Type" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_path">File Path</label> 
<input data-type="text" type="text" name="file_path" id="edit_media_uploads_file_path" class="form-control edit_media_uploads_file_path media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="File Path" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_full_path">Full Path</label> 
<input data-type="text" type="text" name="full_path" id="edit_media_uploads_full_path" class="form-control edit_media_uploads_full_path media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Full Path" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_raw_name">Raw Name</label> 
<input data-type="text" type="text" name="raw_name" id="edit_media_uploads_raw_name" class="form-control edit_media_uploads_raw_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Raw Name" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_orig_name">Original Name</label> 
<input data-type="text" type="text" name="orig_name" id="edit_media_uploads_orig_name" class="form-control edit_media_uploads_orig_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Original Name" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_client_name">Client Name</label> 
<input data-type="text" type="text" name="client_name" id="edit_media_uploads_client_name" class="form-control edit_media_uploads_client_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Client Name" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_ext">File Extension</label> 
<input data-type="text" type="text" name="file_ext" id="edit_media_uploads_file_ext" class="form-control edit_media_uploads_file_ext media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="File Extension" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_size">File Size</label> 
<input data-type="text" type="text" name="file_size" id="edit_media_uploads_file_size" class="form-control edit_media_uploads_file_size media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="File Size" value=""/>
</div>
<div class="form-group"><strong>Is Image</strong></div>
<div class="form-group">
<label for="edit_media_uploads_image_width">Image Width</label> 
<input data-type="text" type="text" name="image_width" id="edit_media_uploads_image_width" class="form-control edit_media_uploads_image_width media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Width" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_image_height">Image Height</label> 
<input data-type="text" type="text" name="image_height" id="edit_media_uploads_image_height" class="form-control edit_media_uploads_image_height media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Height" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_image_type">Image Type</label> 
<input data-type="text" type="text" name="image_type" id="edit_media_uploads_image_type" class="form-control edit_media_uploads_image_type media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Type" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_image_size_str">Image Size String</label> 
<input data-type="text" type="text" name="image_size_str" id="edit_media_uploads_image_size_str" class="form-control edit_media_uploads_image_size_str media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Size String" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-media_uploads">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-media_uploads" id="update-back-media_uploads">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-media_uploads -->
</div><!-- .tab-content .tab-content-media_uploads -->
			<?php if( isset($admin_access->controller_realestate_media) ) { ?>
			<div class="tab-content tab-content-realestate_media" style="display:none"><div id="list-view-realestate_media" class="list-view">
<div class="panel panel-default panel-realestate_media">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_media->can_add) && ($admin_access->controller_realestate_media->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_media">Add To Real Estate</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Real Estate<span data-linked='realestate_data' data-key="re_title_m" data-table="realestate_media" id="list_search_button_re_title_m" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_media" title="Search Real Estate">
		<i class="fa fa-search"></i></span></th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="re_med_group" data-table="realestate_media">Group <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="None">Media Type</th><th width="None">Caption</th><th width="None">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_media -->
</div>
		<?php if( isset($admin_access->controller_realestate_media->can_add) && ($admin_access->controller_realestate_media->can_add == 1) ) { ?>
		<div id="add-view-realestate_media" style="display:none">
<div class="panel panel-default add-panel-realestate_media">
                        <div class="panel-heading"><h3 class="panel-title">Add To Real Estate</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_realestate_media_re_id">Real Estate</label> 
<input data-type="text" type="hidden" name="re_id" id="add_realestate_media_re_id" class="form-control add_realestate_media_re_id realestate_media-input  table-realestate_media add-table-realestate_media text text text-searchable-key-re_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="re_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_m" data-action="add"  class="text-searchable-list re_id" data-toggle="modal" data-target="#add-text-searchable-box-re_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="re_id" class="form-control add text-searchable re_id" placeholder="Search Real Estate" data-field="re_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_m" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-re_id" tabindex="-1" role="dialog" aria-labelledby="Real Estate" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Real Estate List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_re_med_group">Group</label> 
			<select name="re_med_group" id="add_realestate_media_re_med_group" class="selectpicker form-control add_realestate_media_re_med_group realestate_media-input  table-realestate_media add-table-realestate_media dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="gallery">Gallery</option>
<option value="general">General Media</option>
<option value="slider">Slider</option>
</select></div>
<input data-type="hidden" type="hidden" name="media_id" id="add_realestate_media_media_id" class="add_realestate_media_media_id realestate_media-input  table-realestate_media add-table-realestate_media hidden " placeholder="Media File" value="" />
<div class="form-group">
<label for="add_realestate_media_media_thumb">Thumbnail</label> 
<input data-type="text" type="hidden" name="media_thumb" id="add_realestate_media_media_thumb" class="form-control add_realestate_media_media_thumb realestate_media-input  table-realestate_media add-table-realestate_media text text text-searchable-key-media_thumb  add text-searchable-key" />
<a href="javascript:void(0)" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="add"  class="text-searchable-list media_thumb" data-toggle="modal" data-target="#add-text-searchable-box-media_thumb"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_thumb" class="form-control add text-searchable media_thumb" placeholder="Search Thumbnail" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-media_thumb" tabindex="-1" role="dialog" aria-labelledby="Thumbnail" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Thumbnail List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_media_type">Media Type</label> 
			<select name="media_type" id="add_realestate_media_media_type" class="selectpicker form-control add_realestate_media_media_type realestate_media-input  table-realestate_media add-table-realestate_media dropdown text" placeholder="Media Type" data-live-search="true" >
			<option value="">- - Select Media Type - -</option>
<option value="image">Image</option>
<option value="video">Video</option>
</select></div>
<div class="form-group">
<label for="add_realestate_media_media_caption">Caption</label> 
<input data-type="text" type="text" name="media_caption" id="add_realestate_media_media_caption" class="form-control add_realestate_media_media_caption realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Caption" value=""/>
</div>
<div class="form-group">
<label for="add_realestate_media_media_link">Link</label> 
<input data-type="text" type="text" name="media_link" id="add_realestate_media_media_link" class="form-control add_realestate_media_media_link realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="add_realestate_media_re_med_order">Order</label> 
<input data-type="text" type="text" name="re_med_order" id="add_realestate_media_re_med_order" class="form-control add_realestate_media_re_med_order realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Order" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_med_active" id="add_realestate_media_re_med_active" class="add_realestate_media_re_med_active realestate_media-input  table-realestate_media add-table-realestate_media checkbox text" placeholder="Active" value="1" />Set Active</label></div></div>
<div class="form-group">
<label for="add_realestate_media_media_name">Name</label> 
<input data-type="text" type="text" name="media_name" id="add_realestate_media_media_name" class="form-control add_realestate_media_media_name realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Name" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_media">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_media">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_media -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_media->can_edit) && ($admin_access->controller_realestate_media->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_media" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_media">
<div class="panel-heading">
	 <h3 class="panel-title">Edit To Real Estate</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_med_id" id="edit_realestate_media_re_med_id" class="edit_realestate_media_re_med_id realestate_media-input  table-realestate_media edit-table-realestate_media hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_realestate_media_re_id">Real Estate</label> 
<input data-type="text" type="hidden" name="re_id" id="edit_realestate_media_re_id" class="form-control edit_realestate_media_re_id realestate_media-input  table-realestate_media edit-table-realestate_media text text text-searchable-key-re_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="re_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_m" data-action="edit"  class="text-searchable-list re_id" data-toggle="modal" data-target="#edit-text-searchable-box-re_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="re_id" class="form-control edit text-searchable re_id" placeholder="Search Real Estate" data-field="re_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_m" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-re_id" tabindex="-1" role="dialog" aria-labelledby="Real Estate" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Real Estate List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_re_med_group">Group</label> 
			<select name="re_med_group" id="edit_realestate_media_re_med_group" class="selectpicker form-control edit_realestate_media_re_med_group realestate_media-input  table-realestate_media edit-table-realestate_media dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="gallery">Gallery</option>
<option value="general">General Media</option>
<option value="slider">Slider</option>
</select></div>
<input data-type="hidden" type="hidden" name="media_id" id="edit_realestate_media_media_id" class="edit_realestate_media_media_id realestate_media-input  table-realestate_media edit-table-realestate_media hidden " placeholder="Media File" value="" />
<div class="form-group">
<label for="edit_realestate_media_media_thumb">Thumbnail</label> 
<input data-type="text" type="hidden" name="media_thumb" id="edit_realestate_media_media_thumb" class="form-control edit_realestate_media_media_thumb realestate_media-input  table-realestate_media edit-table-realestate_media text text text-searchable-key-media_thumb  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="edit"  class="text-searchable-list media_thumb" data-toggle="modal" data-target="#edit-text-searchable-box-media_thumb"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_thumb" class="form-control edit text-searchable media_thumb" placeholder="Search Thumbnail" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-media_thumb" tabindex="-1" role="dialog" aria-labelledby="Thumbnail" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Thumbnail List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_media_type">Media Type</label> 
			<select name="media_type" id="edit_realestate_media_media_type" class="selectpicker form-control edit_realestate_media_media_type realestate_media-input  table-realestate_media edit-table-realestate_media dropdown text" placeholder="Media Type" data-live-search="true" >
			<option value="">- - Select Media Type - -</option>
<option value="image">Image</option>
<option value="video">Video</option>
</select></div>
<div class="form-group">
<label for="edit_realestate_media_media_caption">Caption</label> 
<input data-type="text" type="text" name="media_caption" id="edit_realestate_media_media_caption" class="form-control edit_realestate_media_media_caption realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Caption" value=""/>
</div>
<div class="form-group">
<label for="edit_realestate_media_media_link">Link</label> 
<input data-type="text" type="text" name="media_link" id="edit_realestate_media_media_link" class="form-control edit_realestate_media_media_link realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="edit_realestate_media_re_med_order">Order</label> 
<input data-type="text" type="text" name="re_med_order" id="edit_realestate_media_re_med_order" class="form-control edit_realestate_media_re_med_order realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Order" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_med_active" id="edit_realestate_media_re_med_active" class="edit_realestate_media_re_med_active realestate_media-input  table-realestate_media edit-table-realestate_media checkbox text" placeholder="Active" value="1" />Set Active</label></div></div>
<div class="form-group">
<label for="edit_realestate_media_media_name">Name</label> 
<input data-type="text" type="text" name="media_name" id="edit_realestate_media_media_name" class="form-control edit_realestate_media_media_name realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Name" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_media">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_media" id="update-back-realestate_media">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_media -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_media -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'media_uploads',
		tables : { 
		<?php if( isset($admin_access->controller_media_uploads) ) { ?>
		
'media_uploads' : { label : 'Media File',
fields : ["media_id","file_name","file_type","file_path","full_path","raw_name","orig_name","client_name","file_ext","file_size","is_image","image_width","image_height","image_type","image_size_str"],
add_fields : ["media_id"],
edit_fields : ["media_id","file_name","file_type","file_path","full_path","raw_name","orig_name","client_name","file_ext","file_size","is_image","image_width","image_height","image_type","image_size_str"],
list_limit : 20,
list_fields : ["media_id","file_name","file_type"],
order_by : 'media_id',
order_sort : 'DESC',
primary_key : 'media_id',
primary_title : 'file_name',
actions_edit : <?php echo ($admin_access->controller_media_uploads->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_media_uploads->can_delete) ? 1 : 0; ?>,
upload_settings : {
				allowed_types : 'jpg,jpeg,gif,png',
				max_size : '100000', 
				upload_url : 'https://uploads.indavao.net/',
				is_image : 'is_image',
				file_name : 'file_name',
			} },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_media) ) { ?>
		
'realestate_media' : { label : 'Real Estate Media',
fields : ["re_med_id","re_id","re_med_group","media_id","media_thumb","media_type","media_caption","media_link","re_med_order","re_med_active","media_name"],
add_fields : ["re_id","re_med_group","media_id","media_thumb","media_type","media_caption","media_link","re_med_order","re_med_active","media_name"],
edit_fields : ["re_med_id","re_id","re_med_group","media_id","media_thumb","media_type","media_caption","media_link","re_med_order","re_med_active","media_name"],
list_limit : 20,
list_fields : ["re_title_m","re_med_group","media_type","media_caption","re_med_order"],
order_by : 're_med_order',
order_sort : 'ASC',
filters : {"re_med_group":{"type":"manual","anchor":0}},
primary_key : 're_med_id',
active_key : 're_med_active',
actual_values : {"re_id" : "re_title_m","media_id" : "file_name"},
required_key : 'media_id',
required_value : 'media_id',
required_table : 'media_uploads',
actions_edit : <?php echo ($admin_access->controller_realestate_media->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_media->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"media_type": {"image": "Image", "video": "Video"}, "re_med_active": {"1": "Set Active"}, "re_med_group": {"slider": "Slider", "gallery": "Gallery", "general": "General Media"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>