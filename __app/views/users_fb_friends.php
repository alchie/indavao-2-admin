<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-users_fb_friends" class="list-view">
<div class="panel panel-default panel-users_fb_friends">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_fb_friends->can_add) && ($admin_access->controller_users_fb_friends->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_fb_friends">Add Facebook Friend</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">User</th><th width="">Friend<span data-linked='users' data-key="name2" data-table="users_fb_friends" id="list_search_button_name2" class="btn btn-primary btn-xs pull-right btn-search list-search-users_fb_friends" title="Search Friend">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_fb_friends -->
</div>
		<?php if( isset($admin_access->controller_users_fb_friends->can_add) && ($admin_access->controller_users_fb_friends->can_add == 1) ) { ?>
		<div id="add-view-users_fb_friends" style="display:none">
<div class="panel panel-default add-panel-users_fb_friends">
                        <div class="panel-heading"><h3 class="panel-title">Add Facebook Friend</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_users_fb_friends_user_id">User</label> 
<input data-type="text" type="text" name="user_id" id="add_users_fb_friends_user_id" class="form-control add_users_fb_friends_user_id users_fb_friends-input  table-users_fb_friends add-table-users_fb_friends text text" placeholder="User" value=""/>
</div>
<div class="form-group">
<label for="add_users_fb_friends_friend_id">Friend</label> 
<input data-type="text" type="text" name="friend_id" id="add_users_fb_friends_friend_id" class="form-control add_users_fb_friends_friend_id users_fb_friends-input  table-users_fb_friends add-table-users_fb_friends text text" placeholder="Friend" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_fb_friends">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_fb_friends">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_fb_friends -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_fb_friends->can_edit) && ($admin_access->controller_users_fb_friends->can_edit == 1) ) { ?>
		<div id="edit-view-users_fb_friends" style="display:none">
		
		<div class="tab-content tab-content-users_fb_friends parent active"><div class="panel panel-default edit-panel-users_fb_friends">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Facebook Friend</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="ufbf_id" id="edit_users_fb_friends_ufbf_id" class="edit_users_fb_friends_ufbf_id users_fb_friends-input  table-users_fb_friends edit-table-users_fb_friends hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_users_fb_friends_user_id">User</label> 
<input data-type="text" type="text" name="user_id" id="edit_users_fb_friends_user_id" class="form-control edit_users_fb_friends_user_id users_fb_friends-input  table-users_fb_friends edit-table-users_fb_friends text text" placeholder="User" value=""/>
</div>
<div class="form-group">
<label for="edit_users_fb_friends_friend_id">Friend</label> 
<input data-type="text" type="text" name="friend_id" id="edit_users_fb_friends_friend_id" class="form-control edit_users_fb_friends_friend_id users_fb_friends-input  table-users_fb_friends edit-table-users_fb_friends text text" placeholder="Friend" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_fb_friends">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_fb_friends" id="update-back-users_fb_friends">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_fb_friends -->
</div><!-- .tab-content .tab-content-users_fb_friends --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'users_fb_friends',
		tables : { 
		<?php if( isset($admin_access->controller_users_fb_friends) ) { ?>
		
'users_fb_friends' : { label : 'Facebook Friend',
fields : ["ufbf_id","user_id","friend_id"],
add_fields : ["user_id","friend_id"],
edit_fields : ["ufbf_id","user_id","friend_id"],
list_limit : 20,
list_fields : ["name","name2"],
order_by : 'ufbf_id',
order_sort : 'ASC',
primary_key : 'ufbf_id',
actual_values : {"user_id" : "name","friend_id" : "name2"},
actions_edit : <?php echo ($admin_access->controller_users_fb_friends->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_fb_friends->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>