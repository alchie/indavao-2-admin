        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo $base_url; ?>assets/js/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo $base_url; ?>assets/js/bootstrap.min.js"></script>
    
    <script src="<?php echo $base_url; ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <script src="<?php echo $base_url; ?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo $base_url; ?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    
    <script src="<?php echo $base_url; ?>assets/js/plugins/datetimepicker/jquery.datetimepicker.js"></script>
    
	<script src="<?php echo $base_url; ?>assets/js/plugins/fancyBox/jquery.fancybox.pack.js"></script>
	
    <script src="<?php echo $base_url; ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
    
    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo $base_url; ?>assets/js/sb-admin.js"></script>
    <script src="<?php echo $base_url; ?>assets/js/jquery.actions.js"></script>
    <?php echo $footer; ?>
</body>

</html>
