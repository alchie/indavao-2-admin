<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-users_shares" class="list-view">
<div class="panel panel-default panel-users_shares">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_shares->can_add) && ($admin_access->controller_users_shares->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_shares">Add FB Shares</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Object ID</th><th width="None">Object</th><th width="None">FB Post ID</th><th width="None">Liked</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_shares -->
</div>
		<?php if( isset($admin_access->controller_users_shares->can_add) && ($admin_access->controller_users_shares->can_add == 1) ) { ?>
		<div id="add-view-users_shares" style="display:none">
<div class="panel panel-default add-panel-users_shares">
                        <div class="panel-heading"><h3 class="panel-title">Add FB Shares</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="share_id" id="add_users_shares_share_id" class="add_users_shares_share_id users_shares-input  table-users_shares add-table-users_shares hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_users_shares_user_id">User ID</label> 
<input data-type="text" type="hidden" name="user_id" id="add_users_shares_user_id" class="form-control add_users_shares_user_id users_shares-input  table-users_shares add-table-users_shares text text text-searchable-key-user_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="fbid" data-value="name" data-display="name" data-action="add"  class="text-searchable-list user_id" data-toggle="modal" data-target="#add-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control add text-searchable user_id" placeholder="Search User ID" data-field="user_id"  data-table="users" data-key="fbid" data-value="name" data-display="name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User ID" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User ID List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_users_shares_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="add_users_shares_object_id" class="form-control add_users_shares_object_id users_shares-input  table-users_shares add-table-users_shares text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="add_users_shares_object_type">Object</label> 
<input data-type="text" type="text" name="object_type" id="add_users_shares_object_type" class="form-control add_users_shares_object_type users_shares-input  table-users_shares add-table-users_shares text text" placeholder="Object" value=""/>
</div>
<div class="form-group">
<label for="add_users_shares_fb_post_id">FB Post ID</label> 
<input data-type="text" type="text" name="fb_post_id" id="add_users_shares_fb_post_id" class="form-control add_users_shares_fb_post_id users_shares-input  table-users_shares add-table-users_shares text text" placeholder="FB Post ID" value=""/>
</div>
<div class="form-group"><strong>Liked</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="likes" id="add_users_shares_likes" class="add_users_shares_likes users_shares-input  table-users_shares add-table-users_shares checkbox text" placeholder="Liked" value="1" />Liked</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_shares">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_shares">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_shares -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_shares->can_edit) && ($admin_access->controller_users_shares->can_edit == 1) ) { ?>
		<div id="edit-view-users_shares" style="display:none">
		
		<div class="tab-content tab-content-users_shares parent active"><div class="panel panel-default edit-panel-users_shares">
<div class="panel-heading">
	 <h3 class="panel-title">Edit FB Shares</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="share_id" id="edit_users_shares_share_id" class="edit_users_shares_share_id users_shares-input  table-users_shares edit-table-users_shares hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_users_shares_user_id">User ID</label> 
<input data-type="text" type="hidden" name="user_id" id="edit_users_shares_user_id" class="form-control edit_users_shares_user_id users_shares-input  table-users_shares edit-table-users_shares text text text-searchable-key-user_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="fbid" data-value="name" data-display="name" data-action="edit"  class="text-searchable-list user_id" data-toggle="modal" data-target="#edit-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control edit text-searchable user_id" placeholder="Search User ID" data-field="user_id"  data-table="users" data-key="fbid" data-value="name" data-display="name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User ID" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User ID List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="edit_users_shares_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="edit_users_shares_object_id" class="form-control edit_users_shares_object_id users_shares-input  table-users_shares edit-table-users_shares text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="edit_users_shares_object_type">Object</label> 
<input data-type="text" type="text" name="object_type" id="edit_users_shares_object_type" class="form-control edit_users_shares_object_type users_shares-input  table-users_shares edit-table-users_shares text text" placeholder="Object" value=""/>
</div>
<div class="form-group">
<label for="edit_users_shares_fb_post_id">FB Post ID</label> 
<input data-type="text" type="text" name="fb_post_id" id="edit_users_shares_fb_post_id" class="form-control edit_users_shares_fb_post_id users_shares-input  table-users_shares edit-table-users_shares text text" placeholder="FB Post ID" value=""/>
</div>
<div class="form-group"><strong>Liked</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="likes" id="edit_users_shares_likes" class="edit_users_shares_likes users_shares-input  table-users_shares edit-table-users_shares checkbox text" placeholder="Liked" value="1" />Liked</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_shares">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_shares" id="update-back-users_shares">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_shares -->
</div><!-- .tab-content .tab-content-users_shares --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'users_shares',
		tables : { 
		<?php if( isset($admin_access->controller_users_shares) ) { ?>
		
'users_shares' : { label : 'FB Shares',
fields : ["share_id","user_id","object_id","object_type","fb_post_id","likes"],
add_fields : ["share_id","user_id","object_id","object_type","fb_post_id","likes"],
edit_fields : ["share_id","user_id","object_id","object_type","fb_post_id","likes"],
list_limit : 20,
list_fields : ["object_id","object_type","fb_post_id","likes"],
order_by : 'share_id',
order_sort : 'DESC',
primary_key : 'share_id',
active_key : 'likes',
actions_edit : <?php echo ($admin_access->controller_users_shares->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_shares->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"likes": {"1": "Liked"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>