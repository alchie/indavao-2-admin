<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-tasks_urls" class="list-view">
<div class="panel panel-default panel-tasks_urls">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_tasks_urls->can_add) && ($admin_access->controller_tasks_urls->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-tasks_urls">Add Tasks URL</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="None">Page Name</th><th width="None">Page URL</th><th width="None">Object Type</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-tasks_urls -->
</div>
		<?php if( isset($admin_access->controller_tasks_urls->can_add) && ($admin_access->controller_tasks_urls->can_add == 1) ) { ?>
		<div id="add-view-tasks_urls" style="display:none">
<div class="panel panel-default add-panel-tasks_urls">
                        <div class="panel-heading"><h3 class="panel-title">Add Tasks URL</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_tasks_urls_page_id">Page ID</label> 
<input data-type="text" type="text" name="page_id" id="add_tasks_urls_page_id" class="form-control add_tasks_urls_page_id tasks_urls-input  table-tasks_urls add-table-tasks_urls text text" placeholder="Page ID" value=""/>
</div>
<div class="form-group">
<label for="add_tasks_urls_page_name">Page Name</label> 
<input data-type="text" type="text" name="page_name" id="add_tasks_urls_page_name" class="form-control add_tasks_urls_page_name tasks_urls-input  table-tasks_urls add-table-tasks_urls text text" placeholder="Page Name" value=""/>
</div>
<div class="form-group">
<label for="add_tasks_urls_page_url">Page URL</label> 
<input data-type="text" type="text" name="page_url" id="add_tasks_urls_page_url" class="form-control add_tasks_urls_page_url tasks_urls-input  table-tasks_urls add-table-tasks_urls text text" placeholder="Page URL" value=""/>
</div>
<div class="form-group">
<label for="add_tasks_urls_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="add_tasks_urls_object_type" class="form-control add_tasks_urls_object_type tasks_urls-input  table-tasks_urls add-table-tasks_urls text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_tasks_urls_active" class="add_tasks_urls_active tasks_urls-input  table-tasks_urls add-table-tasks_urls checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-tasks_urls">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-tasks_urls">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tasks_urls -->
</div>
<?php } ?><?php if( isset($admin_access->controller_tasks_urls->can_edit) && ($admin_access->controller_tasks_urls->can_edit == 1) ) { ?>
		<div id="edit-view-tasks_urls" style="display:none">
		
		<div class="tab-content tab-content-tasks_urls parent active"><div class="panel panel-default edit-panel-tasks_urls">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Tasks URL</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tasks_url_id" id="edit_tasks_urls_tasks_url_id" class="edit_tasks_urls_tasks_url_id tasks_urls-input  table-tasks_urls edit-table-tasks_urls hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_tasks_urls_page_id">Page ID</label> 
<input data-type="text" type="text" name="page_id" id="edit_tasks_urls_page_id" class="form-control edit_tasks_urls_page_id tasks_urls-input  table-tasks_urls edit-table-tasks_urls text text" placeholder="Page ID" value=""/>
</div>
<div class="form-group">
<label for="edit_tasks_urls_page_name">Page Name</label> 
<input data-type="text" type="text" name="page_name" id="edit_tasks_urls_page_name" class="form-control edit_tasks_urls_page_name tasks_urls-input  table-tasks_urls edit-table-tasks_urls text text" placeholder="Page Name" value=""/>
</div>
<div class="form-group">
<label for="edit_tasks_urls_page_url">Page URL</label> 
<input data-type="text" type="text" name="page_url" id="edit_tasks_urls_page_url" class="form-control edit_tasks_urls_page_url tasks_urls-input  table-tasks_urls edit-table-tasks_urls text text" placeholder="Page URL" value=""/>
</div>
<div class="form-group">
<label for="edit_tasks_urls_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="edit_tasks_urls_object_type" class="form-control edit_tasks_urls_object_type tasks_urls-input  table-tasks_urls edit-table-tasks_urls text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_tasks_urls_active" class="edit_tasks_urls_active tasks_urls-input  table-tasks_urls edit-table-tasks_urls checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-tasks_urls">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-tasks_urls" id="update-back-tasks_urls">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tasks_urls -->
</div><!-- .tab-content .tab-content-tasks_urls --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'tasks_urls',
		tables : { 
		<?php if( isset($admin_access->controller_tasks_urls) ) { ?>
		
'tasks_urls' : { label : 'Tasks URL',
fields : ["tasks_url_id","page_id","page_name","page_url","object_type","active"],
add_fields : ["page_id","page_name","page_url","object_type","active"],
edit_fields : ["tasks_url_id","page_id","page_name","page_url","object_type","active"],
list_limit : 20,
list_fields : ["page_name","page_url","object_type"],
order_by : 'page_name',
order_sort : 'ASC',
primary_key : 'tasks_url_id',
primary_title : 'page_name',
active_key : 'active',
actions_edit : <?php echo ($admin_access->controller_tasks_urls->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_tasks_urls->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>