<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-contact_messages" class="list-view">
<div class="panel panel-default panel-contact_messages">
<div class="panel-heading">
&nbsp;
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Name<span  data-key="name" data-table="contact_messages" id="list_search_button_name" class="btn btn-primary btn-xs pull-right btn-search list-search-contact_messages" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="">Phone<span  data-key="phone" data-table="contact_messages" id="list_search_button_phone" class="btn btn-primary btn-xs pull-right btn-search list-search-contact_messages" title="Search Phone">
		<i class="fa fa-search"></i></span></th><th width="">Email<span  data-key="email" data-table="contact_messages" id="list_search_button_email" class="btn btn-primary btn-xs pull-right btn-search list-search-contact_messages" title="Search Email">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-contact_messages -->
</div>
		<?php if( isset($admin_access->controller_contact_messages->can_add) && ($admin_access->controller_contact_messages->can_add == 1) ) { ?>
		<div id="add-view-contact_messages" style="display:none">
<div class="panel panel-default add-panel-contact_messages">
                        <div class="panel-heading"><h3 class="panel-title">Add Message</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_contact_messages_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="add_contact_messages_object_id" class="form-control add_contact_messages_object_id contact_messages-input  table-contact_messages add-table-contact_messages text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="add_contact_messages_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="add_contact_messages_object_type" class="form-control add_contact_messages_object_type contact_messages-input  table-contact_messages add-table-contact_messages text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group">
<label for="add_contact_messages_name">Name</label> 
<input data-type="text" type="text" name="name" id="add_contact_messages_name" class="form-control add_contact_messages_name contact_messages-input  table-contact_messages add-table-contact_messages text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_contact_messages_phone">Phone</label> 
<input data-type="text" type="text" name="phone" id="add_contact_messages_phone" class="form-control add_contact_messages_phone contact_messages-input  table-contact_messages add-table-contact_messages text text" placeholder="Phone" value=""/>
</div>
<div class="form-group">
<label for="add_contact_messages_email">Email</label> 
<input data-type="text" type="text" name="email" id="add_contact_messages_email" class="form-control add_contact_messages_email contact_messages-input  table-contact_messages add-table-contact_messages text text" placeholder="Email" value=""/>
</div>
<div class="form-group">
<label for="add_contact_messages_message">Message</label>
<textarea rows="3" data-type="textarea" data-wysiwyg="0" type="text" name="message" id="add_contact_messages_message" class="form-control add_contact_messages_message contact_messages-input  table-contact_messages add-table-contact_messages textarea text" placeholder="Message" value="" /></textarea>
</div>
<div class="form-group">
<label for="add_contact_messages_date_sent">Date Sent</label> 
<input data-type="text" type="text" name="date_sent" id="add_contact_messages_date_sent" class="form-control add_contact_messages_date_sent contact_messages-input  table-contact_messages add-table-contact_messages text text datetimepicker" placeholder="Date Sent" value=""/>
</div>
<div class="form-group">
<label for="add_contact_messages_user_id">User</label> 
<input data-type="text" type="hidden" name="user_id" id="add_contact_messages_user_id" class="form-control add_contact_messages_user_id contact_messages-input  table-contact_messages add-table-contact_messages text text text-searchable-key-user_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name2" data-action="add"  class="text-searchable-list user_id" data-toggle="modal" data-target="#add-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control add text-searchable user_id" placeholder="Search User" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name2" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_contact_messages_msg_type">Type</label> 
<input data-type="text" type="text" name="msg_type" id="add_contact_messages_msg_type" class="form-control add_contact_messages_msg_type contact_messages-input  table-contact_messages add-table-contact_messages text text" placeholder="Type" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-contact_messages">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-contact_messages">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-contact_messages -->
</div>
<?php } ?><?php if( isset($admin_access->controller_contact_messages->can_edit) && ($admin_access->controller_contact_messages->can_edit == 1) ) { ?>
		<div id="edit-view-contact_messages" style="display:none">
		
		<div class="tab-content tab-content-contact_messages parent active"><div class="panel panel-default edit-panel-contact_messages">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Message</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="msg_id" id="edit_contact_messages_msg_id" class="edit_contact_messages_msg_id contact_messages-input  table-contact_messages edit-table-contact_messages hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_contact_messages_object_id">Object ID</label> 
<input data-type="text" type="text" name="object_id" id="edit_contact_messages_object_id" class="form-control edit_contact_messages_object_id contact_messages-input  table-contact_messages edit-table-contact_messages text text" placeholder="Object ID" value=""/>
</div>
<div class="form-group">
<label for="edit_contact_messages_object_type">Object Type</label> 
<input data-type="text" type="text" name="object_type" id="edit_contact_messages_object_type" class="form-control edit_contact_messages_object_type contact_messages-input  table-contact_messages edit-table-contact_messages text text" placeholder="Object Type" value=""/>
</div>
<div class="form-group">
<label for="edit_contact_messages_name">Name</label> 
<input data-type="text" type="text" name="name" id="edit_contact_messages_name" class="form-control edit_contact_messages_name contact_messages-input  table-contact_messages edit-table-contact_messages text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="edit_contact_messages_phone">Phone</label> 
<input data-type="text" type="text" name="phone" id="edit_contact_messages_phone" class="form-control edit_contact_messages_phone contact_messages-input  table-contact_messages edit-table-contact_messages text text" placeholder="Phone" value=""/>
</div>
<div class="form-group">
<label for="edit_contact_messages_email">Email</label> 
<input data-type="text" type="text" name="email" id="edit_contact_messages_email" class="form-control edit_contact_messages_email contact_messages-input  table-contact_messages edit-table-contact_messages text text" placeholder="Email" value=""/>
</div>
<div class="form-group">
<label for="edit_contact_messages_message">Message</label>
<textarea rows="3" data-type="textarea" data-wysiwyg="0" type="text" name="message" id="edit_contact_messages_message" class="form-control edit_contact_messages_message contact_messages-input  table-contact_messages edit-table-contact_messages textarea text" placeholder="Message" value="" /></textarea>
</div>
<div class="form-group">
<label for="edit_contact_messages_date_sent">Date Sent</label> 
<input data-type="text" type="text" name="date_sent" id="edit_contact_messages_date_sent" class="form-control edit_contact_messages_date_sent contact_messages-input  table-contact_messages edit-table-contact_messages text text datetimepicker" placeholder="Date Sent" value=""/>
</div>
<div class="form-group">
<label for="edit_contact_messages_user_id">User</label> 
<input data-type="text" type="hidden" name="user_id" id="edit_contact_messages_user_id" class="form-control edit_contact_messages_user_id contact_messages-input  table-contact_messages edit-table-contact_messages text text text-searchable-key-user_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name2" data-action="edit"  class="text-searchable-list user_id" data-toggle="modal" data-target="#edit-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control edit text-searchable user_id" placeholder="Search User" data-field="user_id"  data-table="users" data-key="user_id" data-value="name" data-display="name2" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="edit_contact_messages_msg_type">Type</label> 
<input data-type="text" type="text" name="msg_type" id="edit_contact_messages_msg_type" class="form-control edit_contact_messages_msg_type contact_messages-input  table-contact_messages edit-table-contact_messages text text" placeholder="Type" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-contact_messages">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-contact_messages" id="update-back-contact_messages">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-contact_messages -->
</div><!-- .tab-content .tab-content-contact_messages --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'contact_messages',
		tables : { 
		<?php if( isset($admin_access->controller_contact_messages) ) { ?>
		
'contact_messages' : { label : 'Message',
fields : ["msg_id","object_id","object_type","name","phone","email","message","date_sent","user_id","msg_type"],
add_fields : ["object_id","object_type","name","phone","email","message","date_sent","user_id","msg_type"],
edit_fields : ["msg_id","object_id","object_type","name","phone","email","message","date_sent","user_id","msg_type"],
list_limit : 20,
list_fields : ["name","phone","email"],
order_by : 'date_sent',
order_sort : 'DESC',
primary_key : 'msg_id',
primary_title : 'name',
actions_edit : <?php echo ($admin_access->controller_contact_messages->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_contact_messages->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>