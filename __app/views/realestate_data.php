<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-realestate_data" class="list-view">
<div class="panel panel-default panel-realestate_data">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_data->can_add) && ($admin_access->controller_realestate_data->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_data">Add Real Estate</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="20%"><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="re_type" data-table="realestate_data">Property Type <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Title<span  data-key="re_title" data-table="realestate_data" id="list_search_button_re_title" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_data" title="Search Title">
		<i class="fa fa-search"></i></span></th><th width="6%"><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="re_status" data-table="realestate_data">Status <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_data -->
</div>
		<?php if( isset($admin_access->controller_realestate_data->can_add) && ($admin_access->controller_realestate_data->can_add == 1) ) { ?>
		<div id="add-view-realestate_data" style="display:none">
<div class="panel panel-default add-panel-realestate_data">
                        <div class="panel-heading"><h3 class="panel-title">Add Real Estate</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_lessons_re_type">Property Type</label> 
			<select name="re_type" id="add_realestate_data_re_type" class="selectpicker form-control add_realestate_data_re_type realestate_data-input  table-realestate_data add-table-realestate_data dropdown text dropdown-table" placeholder="Property Type" data-live-search="true"  data-type="dropdown" data-label="Property Type" data-field="re_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="realestate_property_type" data-order="1" data-order-by="attr_label" data-order-sort="DESC">
			<option value="">- - Select Property Type - -</option>
</select></div>
<div class="form-group">
<label for="add_realestate_data_re_title">Title</label> 
<input data-type="text" type="text" name="re_title" id="add_realestate_data_re_title" class="form-control add_realestate_data_re_title realestate_data-input  table-realestate_data add-table-realestate_data text text" placeholder="Title" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_active" id="add_realestate_data_re_active" class="add_realestate_data_re_active realestate_data-input  table-realestate_data add-table-realestate_data checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_data">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_data">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_data -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_data->can_edit) && ($admin_access->controller_realestate_data->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_data" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-realestate_data" id="update-back-realestate_data">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="realestate_data" data-child="0">Edit Real Estate</a>
		</li>
			<?php if( isset($admin_access->controller_realestate_details) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_details" data-child="1">Details</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_features) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_features" data-child="1">Features</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_media) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_media" data-child="1">Media</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_location) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_location" data-child="1">Locations</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_meta" data-child="1">Meta</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_ancestors) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_ancestors" data-child="1">Ancestors</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_types) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="realestate_types" data-child="1">Property Types</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-realestate_data parent active"><div class="panel panel-default edit-panel-realestate_data">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Real Estate</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_data_re_id" class="edit_realestate_data_re_id realestate_data-input  table-realestate_data edit-table-realestate_data hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_lessons_re_type">Property Type</label> 
			<select name="re_type" id="edit_realestate_data_re_type" class="selectpicker form-control edit_realestate_data_re_type realestate_data-input  table-realestate_data edit-table-realestate_data dropdown  dropdown-table" placeholder="Property Type" data-live-search="true"  data-type="dropdown" data-label="Property Type" data-field="re_type" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="realestate_property_type" data-order="1" data-order-by="attr_label" data-order-sort="DESC">
			<option value="">- - Select Property Type - -</option>
</select></div>
<div class="form-group">
<label for="edit_realestate_data_re_title">Title</label> 
<input data-type="text" type="text" name="re_title" id="edit_realestate_data_re_title" class="form-control edit_realestate_data_re_title realestate_data-input  table-realestate_data edit-table-realestate_data text text" placeholder="Title" value=""/>
</div>
<div class="form-group change-deliberately button realestate_data re_slug">
<label>Slug</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update realestate_data re_slug" data-table="realestate_data" data-field="re_slug">Update Slug</a>
			<p class="text-value realestate_data re_slug edit_realestate_data_re_slug realestate_data-input  table-realestate_data edit-table-realestate_data text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form realestate_data re_slug" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel realestate_data re_slug" data-table="realestate_data" data-field="re_slug">Cancel</a>
			Update Slug</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_realestate_data_re_slug">Slug</label> 
<input data-type="text" type="text" name="re_slug" id="edit_realestate_data_re_slug" class="form-control edit_realestate_data_re_slug realestate_data-input  table-realestate_data edit-table-realestate_data text " placeholder="Slug" value=""/>
</div></div>
</div>

<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_active" id="edit_realestate_data_re_active" class="edit_realestate_data_re_active realestate_data-input  table-realestate_data edit-table-realestate_data checkbox text" placeholder="Active" value="1" />Active</label></div></div>
<div class="form-group change-deliberately button realestate_data re_added">
<label>Date Added</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update realestate_data re_added" data-table="realestate_data" data-field="re_added">Update Date Added</a>
			<p class="text-value realestate_data re_added edit_realestate_data_re_added realestate_data-input  table-realestate_data edit-table-realestate_data text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form realestate_data re_added" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel realestate_data re_added" data-table="realestate_data" data-field="re_added">Cancel</a>
			Update Date Added</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_realestate_data_re_added">Date Added</label> 
<input data-type="text" type="text" name="re_added" id="edit_realestate_data_re_added" class="form-control edit_realestate_data_re_added realestate_data-input  table-realestate_data edit-table-realestate_data text  datetimepicker" placeholder="Date Added" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button realestate_data re_parent">
<label>Parent</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update realestate_data re_parent" data-table="realestate_data" data-field="re_parent">Update Parent</a>
			<p class="text-value realestate_data re_parent edit_realestate_data_re_parent realestate_data-input  table-realestate_data edit-table-realestate_data text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form realestate_data re_parent" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel realestate_data re_parent" data-table="realestate_data" data-field="re_parent">Cancel</a>
			Update Parent</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_realestate_data_re_parent">Parent</label> 
<input data-type="text" type="hidden" name="re_parent" id="edit_realestate_data_re_parent" class="form-control edit_realestate_data_re_parent realestate_data-input  table-realestate_data edit-table-realestate_data text  text-searchable-key-re_parent  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="re_parent"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title2" data-action="edit"  class="text-searchable-list re_parent" data-toggle="modal" data-target="#edit-text-searchable-box-re_parent"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="re_parent" class="form-control edit text-searchable re_parent" placeholder="Search Parent" data-field="re_parent"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title2" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-re_parent" tabindex="-1" role="dialog" aria-labelledby="Parent" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Parent List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div></div>
</div>

<div class="form-group change-deliberately button realestate_data re_status">
<label>Status</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update realestate_data re_status" data-table="realestate_data" data-field="re_status">Update Status</a>
			<p class="text-value realestate_data re_status edit_realestate_data_re_status realestate_data-input  table-realestate_data edit-table-realestate_data dropdown " data-type="dropdown"></p>
			</div>
			<div class="panel panel-info change-deliberately form realestate_data re_status" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel realestate_data re_status" data-table="realestate_data" data-field="re_status">Cancel</a>
			Update Status</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="add_lessons_re_status">Status</label> 
			<select name="re_status" id="edit_realestate_data_re_status" class="selectpicker form-control edit_realestate_data_re_status realestate_data-input  table-realestate_data edit-table-realestate_data dropdown " placeholder="Status" data-live-search="true" >
			<option value="">- - Select Status - -</option>
<option value="draft">Draft</option>
<option value="private">Private</option>
<option value="publish">Published</option>
<option value="user">Users Only</option>
</select></div></div>
</div>

</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_data">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_data" id="update-back-realestate_data">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_data -->
</div><!-- .tab-content .tab-content-realestate_data -->
			<?php if( isset($admin_access->controller_realestate_details) ) { ?>
			<div class="tab-content tab-content-realestate_details" style="display:none"><div id="list-view-realestate_details" class="list-view">
<div class="panel panel-default panel-realestate_details">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_details->can_add) && ($admin_access->controller_realestate_details->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_details">Add Detail</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="30%">Detail Key<span data-linked='attributes' data-key="attr_label" data-table="realestate_details" id="list_search_button_attr_label" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_details" title="Search Detail Key">
		<i class="fa fa-search"></i></span></th><th width="">Value<span  data-key="re_d_value" data-table="realestate_details" id="list_search_button_re_d_value" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_details" title="Search Value">
		<i class="fa fa-search"></i></span></th><th width="10%">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_details -->
</div>
		<?php if( isset($admin_access->controller_realestate_details->can_add) && ($admin_access->controller_realestate_details->can_add == 1) ) { ?>
		<div id="add-view-realestate_details" style="display:none">
<div class="panel panel-default add-panel-realestate_details">
                        <div class="panel-heading"><h3 class="panel-title">Add Detail</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="add_realestate_details_re_id" class="add_realestate_details_re_id realestate_details-input  table-realestate_details add-table-realestate_details hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_re_d_key">Detail Key</label> 
			<select name="re_d_key" id="add_realestate_details_re_d_key" class="selectpicker form-control add_realestate_details_re_d_key realestate_details-input  table-realestate_details add-table-realestate_details dropdown text dropdown-table" placeholder="Detail Key" data-live-search="true"  data-type="dropdown" data-label="Detail Key" data-field="re_d_key" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="realestate_details" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Detail Key - -</option>
</select></div>
<div class="form-group">
<label for="add_realestate_details_re_d_value">Value</label>
<textarea rows="10" data-type="textarea" data-wysiwyg="0" type="text" name="re_d_value" id="add_realestate_details_re_d_value" class="form-control add_realestate_details_re_d_value realestate_details-input  table-realestate_details add-table-realestate_details textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group">
<label for="add_realestate_details_re_d_label">Label</label> 
<input data-type="text" type="text" name="re_d_label" id="add_realestate_details_re_d_label" class="form-control add_realestate_details_re_d_label realestate_details-input  table-realestate_details add-table-realestate_details text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="add_realestate_details_re_d_order">Order</label> 
<input data-type="text" type="text" name="re_d_order" id="add_realestate_details_re_d_order" class="form-control add_realestate_details_re_d_order realestate_details-input  table-realestate_details add-table-realestate_details text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_d_active" id="add_realestate_details_re_d_active" class="add_realestate_details_re_d_active realestate_details-input  table-realestate_details add-table-realestate_details checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_details">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_details">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_details -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_details->can_edit) && ($admin_access->controller_realestate_details->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_details" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_details">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Detail</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_d_id" id="edit_realestate_details_re_d_id" class="edit_realestate_details_re_d_id realestate_details-input  table-realestate_details edit-table-realestate_details hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_details_re_id" class="edit_realestate_details_re_id realestate_details-input  table-realestate_details edit-table-realestate_details hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_re_d_key">Detail Key</label> 
			<select name="re_d_key" id="edit_realestate_details_re_d_key" class="selectpicker form-control edit_realestate_details_re_d_key realestate_details-input  table-realestate_details edit-table-realestate_details dropdown text dropdown-table" placeholder="Detail Key" data-live-search="true"  data-type="dropdown" data-label="Detail Key" data-field="re_d_key" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="realestate_details" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Detail Key - -</option>
</select></div>
<div class="form-group">
<label for="edit_realestate_details_re_d_value">Value</label>
<textarea rows="10" data-type="textarea" data-wysiwyg="0" type="text" name="re_d_value" id="edit_realestate_details_re_d_value" class="form-control edit_realestate_details_re_d_value realestate_details-input  table-realestate_details edit-table-realestate_details textarea text" placeholder="Value" value="" /></textarea>
</div>
<div class="form-group">
<label for="edit_realestate_details_re_d_label">Label</label> 
<input data-type="text" type="text" name="re_d_label" id="edit_realestate_details_re_d_label" class="form-control edit_realestate_details_re_d_label realestate_details-input  table-realestate_details edit-table-realestate_details text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="edit_realestate_details_re_d_order">Order</label> 
<input data-type="text" type="text" name="re_d_order" id="edit_realestate_details_re_d_order" class="form-control edit_realestate_details_re_d_order realestate_details-input  table-realestate_details edit-table-realestate_details text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_d_active" id="edit_realestate_details_re_d_active" class="edit_realestate_details_re_d_active realestate_details-input  table-realestate_details edit-table-realestate_details checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_details">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_details" id="update-back-realestate_details">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_details -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_details -->
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_features) ) { ?>
			<div class="tab-content tab-content-realestate_features" style="display:none"><div id="list-view-realestate_features" class="list-view">
<div class="panel panel-default panel-realestate_features">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_features->can_add) && ($admin_access->controller_realestate_features->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_features">Add Feature</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Features<span data-linked='attributes' data-key="attr_label" data-table="realestate_features" id="list_search_button_attr_label" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_features" title="Search Features">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_features -->
</div>
		<?php if( isset($admin_access->controller_realestate_features->can_add) && ($admin_access->controller_realestate_features->can_add == 1) ) { ?>
		<div id="add-view-realestate_features" style="display:none">
<div class="panel panel-default add-panel-realestate_features">
                        <div class="panel-heading"><h3 class="panel-title">Add Feature</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="add_realestate_features_re_id" class="add_realestate_features_re_id realestate_features-input  table-realestate_features add-table-realestate_features hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_attr_id">Features</label> 
			<select name="attr_id" id="add_realestate_features_attr_id" class="selectpicker form-control add_realestate_features_attr_id realestate_features-input  table-realestate_features add-table-realestate_features dropdown text dropdown-table" placeholder="Features" data-live-search="true"  data-type="dropdown" data-label="Features" data-field="attr_id" data-table="attributes" data-key="attr_id" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="re_features" data-order="1" data-order-by="attr_label" data-order-sort="ASC">
			<option value="">- - Select Features - -</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_f_active" id="add_realestate_features_re_f_active" class="add_realestate_features_re_f_active realestate_features-input  table-realestate_features add-table-realestate_features checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_features">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_features">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_features -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_features->can_edit) && ($admin_access->controller_realestate_features->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_features" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_features">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Feature</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_f_id" id="edit_realestate_features_re_f_id" class="edit_realestate_features_re_f_id realestate_features-input  table-realestate_features edit-table-realestate_features hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_features_re_id" class="edit_realestate_features_re_id realestate_features-input  table-realestate_features edit-table-realestate_features hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_attr_id">Features</label> 
			<select name="attr_id" id="edit_realestate_features_attr_id" class="selectpicker form-control edit_realestate_features_attr_id realestate_features-input  table-realestate_features edit-table-realestate_features dropdown text dropdown-table" placeholder="Features" data-live-search="true"  data-type="dropdown" data-label="Features" data-field="attr_id" data-table="attributes" data-key="attr_id" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="re_features" data-order="1" data-order-by="attr_label" data-order-sort="ASC">
			<option value="">- - Select Features - -</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_f_active" id="edit_realestate_features_re_f_active" class="edit_realestate_features_re_f_active realestate_features-input  table-realestate_features edit-table-realestate_features checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_features">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_features" id="update-back-realestate_features">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_features -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_features -->
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_media) ) { ?>
			<div class="tab-content tab-content-realestate_media" style="display:none"><div id="list-view-realestate_media" class="list-view">
<div class="panel panel-default panel-realestate_media">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_media->can_add) && ($admin_access->controller_realestate_media->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_media">Add Media</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="re_med_group" data-table="realestate_media">Group <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Media File<span data-linked='media_uploads' data-key="file_name" data-table="realestate_media" id="list_search_button_file_name" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_media" title="Search Media File">
		<i class="fa fa-search"></i></span></th><th width="None">Media Type</th><th width="None">Caption</th><th width="None">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_media -->
</div>
		<?php if( isset($admin_access->controller_realestate_media->can_add) && ($admin_access->controller_realestate_media->can_add == 1) ) { ?>
		<div id="add-view-realestate_media" style="display:none">
<div class="panel panel-default add-panel-realestate_media">
                        <div class="panel-heading"><h3 class="panel-title">Add Media</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="add_realestate_media_re_id" class="add_realestate_media_re_id realestate_media-input  table-realestate_media add-table-realestate_media hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_re_med_group">Group</label> 
			<select name="re_med_group" id="add_realestate_media_re_med_group" class="selectpicker form-control add_realestate_media_re_med_group realestate_media-input  table-realestate_media add-table-realestate_media dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="gallery">Gallery</option>
<option value="general">General Media</option>
<option value="slider">Slider</option>
</select></div>
<div class="form-group">
<label for="add_realestate_media_media_id">Media File</label> 
<input data-type="text" type="hidden" name="media_id" id="add_realestate_media_media_id" class="form-control add_realestate_media_media_id realestate_media-input  table-realestate_media add-table-realestate_media text  text-searchable-key-media_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="add"  class="text-searchable-list media_id" data-toggle="modal" data-target="#add-text-searchable-box-media_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_id" class="form-control add text-searchable media_id" placeholder="Search Media File" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-media_id" tabindex="-1" role="dialog" aria-labelledby="Media File" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Media File List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_realestate_media_media_thumb">Thumbnail</label> 
<input data-type="text" type="hidden" name="media_thumb" id="add_realestate_media_media_thumb" class="form-control add_realestate_media_media_thumb realestate_media-input  table-realestate_media add-table-realestate_media text text text-searchable-key-media_thumb  add text-searchable-key" />
<a href="javascript:void(0)" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="add"  class="text-searchable-list media_thumb" data-toggle="modal" data-target="#add-text-searchable-box-media_thumb"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_thumb" class="form-control add text-searchable media_thumb" placeholder="Search Thumbnail" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-media_thumb" tabindex="-1" role="dialog" aria-labelledby="Thumbnail" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Thumbnail List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_media_type">Media Type</label> 
			<select name="media_type" id="add_realestate_media_media_type" class="selectpicker form-control add_realestate_media_media_type realestate_media-input  table-realestate_media add-table-realestate_media dropdown text" placeholder="Media Type" data-live-search="true" >
			<option value="">- - Select Media Type - -</option>
<option value="image">Image</option>
<option value="video">Video</option>
</select></div>
<div class="form-group">
<label for="add_realestate_media_media_caption">Caption</label> 
<input data-type="text" type="text" name="media_caption" id="add_realestate_media_media_caption" class="form-control add_realestate_media_media_caption realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Caption" value=""/>
</div>
<div class="form-group">
<label for="add_realestate_media_media_link">Link</label> 
<input data-type="text" type="text" name="media_link" id="add_realestate_media_media_link" class="form-control add_realestate_media_media_link realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="add_realestate_media_re_med_order">Order</label> 
<input data-type="text" type="text" name="re_med_order" id="add_realestate_media_re_med_order" class="form-control add_realestate_media_re_med_order realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Order" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_med_active" id="add_realestate_media_re_med_active" class="add_realestate_media_re_med_active realestate_media-input  table-realestate_media add-table-realestate_media checkbox text" placeholder="Active" value="1" />Set Active</label></div></div>
<div class="form-group">
<label for="add_realestate_media_media_name">Name</label> 
<input data-type="text" type="text" name="media_name" id="add_realestate_media_media_name" class="form-control add_realestate_media_media_name realestate_media-input  table-realestate_media add-table-realestate_media text text" placeholder="Name" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_media">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_media">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_media -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_media->can_edit) && ($admin_access->controller_realestate_media->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_media" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_media">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Media</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_med_id" id="edit_realestate_media_re_med_id" class="edit_realestate_media_re_med_id realestate_media-input  table-realestate_media edit-table-realestate_media hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_media_re_id" class="edit_realestate_media_re_id realestate_media-input  table-realestate_media edit-table-realestate_media hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_re_med_group">Group</label> 
			<select name="re_med_group" id="edit_realestate_media_re_med_group" class="selectpicker form-control edit_realestate_media_re_med_group realestate_media-input  table-realestate_media edit-table-realestate_media dropdown text" placeholder="Group" data-live-search="true" >
			<option value="">- - Select Group - -</option>
<option value="gallery">Gallery</option>
<option value="general">General Media</option>
<option value="slider">Slider</option>
</select></div>
<div class="form-group">
<label for="edit_realestate_media_media_id">Media File</label> 
<input data-type="text" type="hidden" name="media_id" id="edit_realestate_media_media_id" class="form-control edit_realestate_media_media_id realestate_media-input  table-realestate_media edit-table-realestate_media text  text-searchable-key-media_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="edit"  class="text-searchable-list media_id" data-toggle="modal" data-target="#edit-text-searchable-box-media_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_id" class="form-control edit text-searchable media_id" placeholder="Search Media File" data-field="media_id"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-media_id" tabindex="-1" role="dialog" aria-labelledby="Media File" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Media File List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="edit_realestate_media_media_thumb">Thumbnail</label> 
<input data-type="text" type="hidden" name="media_thumb" id="edit_realestate_media_media_thumb" class="form-control edit_realestate_media_media_thumb realestate_media-input  table-realestate_media edit-table-realestate_media text text text-searchable-key-media_thumb  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="edit"  class="text-searchable-list media_thumb" data-toggle="modal" data-target="#edit-text-searchable-box-media_thumb"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="media_thumb" class="form-control edit text-searchable media_thumb" placeholder="Search Thumbnail" data-field="media_thumb"  data-table="media_uploads" data-key="media_id" data-value="file_name" data-display="file_name2" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-media_thumb" tabindex="-1" role="dialog" aria-labelledby="Thumbnail" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Thumbnail List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_media_type">Media Type</label> 
			<select name="media_type" id="edit_realestate_media_media_type" class="selectpicker form-control edit_realestate_media_media_type realestate_media-input  table-realestate_media edit-table-realestate_media dropdown text" placeholder="Media Type" data-live-search="true" >
			<option value="">- - Select Media Type - -</option>
<option value="image">Image</option>
<option value="video">Video</option>
</select></div>
<div class="form-group">
<label for="edit_realestate_media_media_caption">Caption</label> 
<input data-type="text" type="text" name="media_caption" id="edit_realestate_media_media_caption" class="form-control edit_realestate_media_media_caption realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Caption" value=""/>
</div>
<div class="form-group">
<label for="edit_realestate_media_media_link">Link</label> 
<input data-type="text" type="text" name="media_link" id="edit_realestate_media_media_link" class="form-control edit_realestate_media_media_link realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Link" value=""/>
</div>
<div class="form-group">
<label for="edit_realestate_media_re_med_order">Order</label> 
<input data-type="text" type="text" name="re_med_order" id="edit_realestate_media_re_med_order" class="form-control edit_realestate_media_re_med_order realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Order" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_med_active" id="edit_realestate_media_re_med_active" class="edit_realestate_media_re_med_active realestate_media-input  table-realestate_media edit-table-realestate_media checkbox text" placeholder="Active" value="1" />Set Active</label></div></div>
<div class="form-group">
<label for="edit_realestate_media_media_name">Name</label> 
<input data-type="text" type="text" name="media_name" id="edit_realestate_media_media_name" class="form-control edit_realestate_media_media_name realestate_media-input  table-realestate_media edit-table-realestate_media text text" placeholder="Name" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_media">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_media" id="update-back-realestate_media">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_media -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_media -->
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_location) ) { ?>
			<div class="tab-content tab-content-realestate_location" style="display:none"><div id="list-view-realestate_location" class="list-view">
<div class="panel panel-default panel-realestate_location">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_location->can_add) && ($admin_access->controller_realestate_location->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_location">Add Location</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Location<span data-linked='locations' data-key="loc_name_re" data-table="realestate_location" id="list_search_button_loc_name_re" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_location" title="Search Location">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_location -->
</div>
		<?php if( isset($admin_access->controller_realestate_location->can_add) && ($admin_access->controller_realestate_location->can_add == 1) ) { ?>
		<div id="add-view-realestate_location" style="display:none">
<div class="panel panel-default add-panel-realestate_location">
                        <div class="panel-heading"><h3 class="panel-title">Add Location</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="add_realestate_location_re_id" class="add_realestate_location_re_id realestate_location-input  table-realestate_location add-table-realestate_location hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_realestate_location_loc_id">Location</label> 
<input data-type="text" type="hidden" name="loc_id" id="add_realestate_location_loc_id" class="form-control add_realestate_location_loc_id realestate_location-input  table-realestate_location add-table-realestate_location text text text-searchable-key-loc_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_re" data-action="add"  class="text-searchable-list loc_id" data-toggle="modal" data-target="#add-text-searchable-box-loc_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="loc_id" class="form-control add text-searchable loc_id" placeholder="Search Location" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_re" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-loc_id" tabindex="-1" role="dialog" aria-labelledby="Location" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Location List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_loc_active" id="add_realestate_location_re_loc_active" class="add_realestate_location_re_loc_active realestate_location-input  table-realestate_location add-table-realestate_location checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_location">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_location">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_location -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_location->can_edit) && ($admin_access->controller_realestate_location->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_location" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_location">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Location</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_location_re_id" class="edit_realestate_location_re_id realestate_location-input  table-realestate_location edit-table-realestate_location hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="edit_realestate_location_loc_id">Location</label> 
<input data-type="text" type="hidden" name="loc_id" id="edit_realestate_location_loc_id" class="form-control edit_realestate_location_loc_id realestate_location-input  table-realestate_location edit-table-realestate_location text text text-searchable-key-loc_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_re" data-action="edit"  class="text-searchable-list loc_id" data-toggle="modal" data-target="#edit-text-searchable-box-loc_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="loc_id" class="form-control edit text-searchable loc_id" placeholder="Search Location" data-field="loc_id"  data-table="locations" data-key="loc_id" data-value="loc_name" data-display="loc_name_re" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-loc_id" tabindex="-1" role="dialog" aria-labelledby="Location" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Location List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_loc_active" id="edit_realestate_location_re_loc_active" class="edit_realestate_location_re_loc_active realestate_location-input  table-realestate_location edit-table-realestate_location checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_location">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_location" id="update-back-realestate_location">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_location -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_location -->
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_meta) ) { ?>
			<div class="tab-content tab-content-realestate_meta" style="display:none"><div id="list-view-realestate_meta" class="list-view">
<div class="panel panel-default panel-realestate_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_meta->can_add) && ($admin_access->controller_realestate_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="30%">Meta Key<span  data-key="meta_key" data-table="realestate_meta" id="list_search_button_meta_key" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_meta" title="Search Meta Key">
		<i class="fa fa-search"></i></span></th><th width="">Meta Value<span  data-key="meta_value" data-table="realestate_meta" id="list_search_button_meta_value" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_meta" title="Search Meta Value">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_meta -->
</div>
		<?php if( isset($admin_access->controller_realestate_meta->can_add) && ($admin_access->controller_realestate_meta->can_add == 1) ) { ?>
		<div id="add-view-realestate_meta" style="display:none">
<div class="panel panel-default add-panel-realestate_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="add_realestate_meta_re_id" class="add_realestate_meta_re_id realestate_meta-input  table-realestate_meta add-table-realestate_meta hidden text" placeholder="Real Estate ID" value="" />
<div class="form-group">
<label for="add_realestate_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_realestate_meta_meta_key" class="form-control add_realestate_meta_meta_key realestate_meta-input  table-realestate_meta add-table-realestate_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="add_realestate_meta_meta_value">Meta Value</label>
<textarea rows="20" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_realestate_meta_meta_value" class="form-control add_realestate_meta_meta_value realestate_meta-input  table-realestate_meta add-table-realestate_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_m_active" id="add_realestate_meta_re_m_active" class="add_realestate_meta_re_m_active realestate_meta-input  table-realestate_meta add-table-realestate_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_meta->can_edit) && ($admin_access->controller_realestate_meta->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_m_id" id="edit_realestate_meta_re_m_id" class="edit_realestate_meta_re_m_id realestate_meta-input  table-realestate_meta edit-table-realestate_meta hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_meta_re_id" class="edit_realestate_meta_re_id realestate_meta-input  table-realestate_meta edit-table-realestate_meta hidden text" placeholder="Real Estate ID" value="" />
<div class="form-group">
<label for="edit_realestate_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_realestate_meta_meta_key" class="form-control edit_realestate_meta_meta_key realestate_meta-input  table-realestate_meta edit-table-realestate_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="edit_realestate_meta_meta_value">Meta Value</label>
<textarea rows="20" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_realestate_meta_meta_value" class="form-control edit_realestate_meta_meta_value realestate_meta-input  table-realestate_meta edit-table-realestate_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_m_active" id="edit_realestate_meta_re_m_active" class="edit_realestate_meta_re_m_active realestate_meta-input  table-realestate_meta edit-table-realestate_meta checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_meta" id="update-back-realestate_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_meta -->
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_ancestors) ) { ?>
			<div class="tab-content tab-content-realestate_ancestors" style="display:none"><div id="list-view-realestate_ancestors" class="list-view">
<div class="panel panel-default panel-realestate_ancestors">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_ancestors->can_add) && ($admin_access->controller_realestate_ancestors->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_ancestors">Add Ancestor</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="10%">ID</th><th width="">Parent<span data-linked='realestate_data' data-key="re_title_2" data-table="realestate_ancestors" id="list_search_button_re_title_2" class="btn btn-primary btn-xs pull-right btn-search list-search-realestate_ancestors" title="Search Parent">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_ancestors -->
</div>
		<?php if( isset($admin_access->controller_realestate_ancestors->can_add) && ($admin_access->controller_realestate_ancestors->can_add == 1) ) { ?>
		<div id="add-view-realestate_ancestors" style="display:none">
<div class="panel panel-default add-panel-realestate_ancestors">
                        <div class="panel-heading"><h3 class="panel-title">Add Ancestor</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="add_realestate_ancestors_re_id" class="add_realestate_ancestors_re_id realestate_ancestors-input  table-realestate_ancestors add-table-realestate_ancestors hidden text" placeholder="Title" value="" />
<div class="form-group">
<label for="add_realestate_ancestors_ancestor_id">Parent</label> 
<input data-type="text" type="hidden" name="ancestor_id" id="add_realestate_ancestors_ancestor_id" class="form-control add_realestate_ancestors_ancestor_id realestate_ancestors-input  table-realestate_ancestors add-table-realestate_ancestors text text text-searchable-key-ancestor_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="ancestor_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_2" data-action="add"  class="text-searchable-list ancestor_id" data-toggle="modal" data-target="#add-text-searchable-box-ancestor_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="ancestor_id" class="form-control add text-searchable ancestor_id" placeholder="Search Parent" data-field="ancestor_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_2" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-ancestor_id" tabindex="-1" role="dialog" aria-labelledby="Parent" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Parent List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_anc_active" id="add_realestate_ancestors_re_anc_active" class="add_realestate_ancestors_re_anc_active realestate_ancestors-input  table-realestate_ancestors add-table-realestate_ancestors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_ancestors -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_ancestors->can_edit) && ($admin_access->controller_realestate_ancestors->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_ancestors" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_ancestors">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Ancestor</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_anc_id" id="edit_realestate_ancestors_re_anc_id" class="edit_realestate_ancestors_re_anc_id realestate_ancestors-input  table-realestate_ancestors edit-table-realestate_ancestors hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_ancestors_re_id" class="edit_realestate_ancestors_re_id realestate_ancestors-input  table-realestate_ancestors edit-table-realestate_ancestors hidden text" placeholder="Title" value="" />
<div class="form-group">
<label for="edit_realestate_ancestors_ancestor_id">Parent</label> 
<input data-type="text" type="hidden" name="ancestor_id" id="edit_realestate_ancestors_ancestor_id" class="form-control edit_realestate_ancestors_ancestor_id realestate_ancestors-input  table-realestate_ancestors edit-table-realestate_ancestors text text text-searchable-key-ancestor_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="ancestor_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_2" data-action="edit"  class="text-searchable-list ancestor_id" data-toggle="modal" data-target="#edit-text-searchable-box-ancestor_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="ancestor_id" class="form-control edit text-searchable ancestor_id" placeholder="Search Parent" data-field="ancestor_id"  data-table="realestate_data" data-key="re_id" data-value="re_title" data-display="re_title_2" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-ancestor_id" tabindex="-1" role="dialog" aria-labelledby="Parent" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Parent List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_anc_active" id="edit_realestate_ancestors_re_anc_active" class="edit_realestate_ancestors_re_anc_active realestate_ancestors-input  table-realestate_ancestors edit-table-realestate_ancestors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_ancestors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_ancestors" id="update-back-realestate_ancestors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_ancestors -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_ancestors -->
			<?php } ?>
			<?php if( isset($admin_access->controller_realestate_types) ) { ?>
			<div class="tab-content tab-content-realestate_types" style="display:none"><div id="list-view-realestate_types" class="list-view">
<div class="panel panel-default panel-realestate_types">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_realestate_types->can_add) && ($admin_access->controller_realestate_types->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-realestate_types">Add Property Type</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="type_name" data-table="realestate_types">Type Name <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-realestate_types -->
</div>
		<?php if( isset($admin_access->controller_realestate_types->can_add) && ($admin_access->controller_realestate_types->can_add == 1) ) { ?>
		<div id="add-view-realestate_types" style="display:none">
<div class="panel panel-default add-panel-realestate_types">
                        <div class="panel-heading"><h3 class="panel-title">Add Property Type</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_id" id="add_realestate_types_re_id" class="add_realestate_types_re_id realestate_types-input  table-realestate_types add-table-realestate_types hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_type_name">Type Name</label> 
			<select name="type_name" id="add_realestate_types_type_name" class="selectpicker form-control add_realestate_types_type_name realestate_types-input  table-realestate_types add-table-realestate_types dropdown text dropdown-table" placeholder="Type Name" data-live-search="true"  data-type="dropdown" data-label="Type Name" data-field="type_name" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="realestate_property_type" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Type Name - -</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_type_active" id="add_realestate_types_re_type_active" class="add_realestate_types_re_type_active realestate_types-input  table-realestate_types add-table-realestate_types checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-realestate_types">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-realestate_types">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_types -->
</div>
<?php } ?><?php if( isset($admin_access->controller_realestate_types->can_edit) && ($admin_access->controller_realestate_types->can_edit == 1) ) { ?>
		<div id="edit-view-realestate_types" style="display:none">
		
		<div class="panel panel-default edit-panel-realestate_types">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Property Type</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="re_type_id" id="edit_realestate_types_re_type_id" class="edit_realestate_types_re_type_id realestate_types-input  table-realestate_types edit-table-realestate_types hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="re_id" id="edit_realestate_types_re_id" class="edit_realestate_types_re_id realestate_types-input  table-realestate_types edit-table-realestate_types hidden text" placeholder="Real Estate" value="" />
<div class="form-group">
<label for="add_lessons_type_name">Type Name</label> 
			<select name="type_name" id="edit_realestate_types_type_name" class="selectpicker form-control edit_realestate_types_type_name realestate_types-input  table-realestate_types edit-table-realestate_types dropdown text dropdown-table" placeholder="Type Name" data-live-search="true"  data-type="dropdown" data-label="Type Name" data-field="type_name" data-table="attributes" data-key="attr_name" data-value="attr_label" data-filter="1" data-filter-key="attr_group" data-filter-value="realestate_property_type" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Type Name - -</option>
</select></div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="re_type_active" id="edit_realestate_types_re_type_active" class="edit_realestate_types_re_type_active realestate_types-input  table-realestate_types edit-table-realestate_types checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-realestate_types">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-realestate_types" id="update-back-realestate_types">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-realestate_types -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-realestate_types -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'realestate_data',
		tables : { 
		<?php if( isset($admin_access->controller_realestate_data) ) { ?>
		
'realestate_data' : { label : 'Real Estate',
fields : ["user_id","re_id","re_type","re_title","re_slug","re_active","re_added","re_parent","re_status"],
add_fields : ["re_type","re_title","re_active"],
edit_fields : ["re_id","re_type","re_title","re_active"],
list_limit : 20,
list_fields : ["re_type","re_title","re_status"],
order_by : 're_added',
order_sort : 'DESC',
filters : {"re_type":{"type":"table","anchor":0,"table":"attributes","key":"attr_name","value":"attr_label", "filter" : 1, "filter_key" : "attr_group", "filter_value" : "realestate_property_type", "order" : 1, "order_by" : "attr_label", "order_sort" : "DESC" },"re_status":{"type":"manual","anchor":0}},
primary_key : 're_id',
primary_title : 're_title',
active_key : 're_active',
actual_values : {"re_type" : "attr_label"},
actions_edit : <?php echo ($admin_access->controller_realestate_data->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_data->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_details) ) { ?>
		
'realestate_details' : { label : 'Property Detail',
fields : ["re_d_id","re_id","re_d_key","re_d_value","re_d_label","re_d_order","re_d_active"],
add_fields : ["re_id","re_d_key","re_d_value","re_d_label","re_d_order","re_d_active"],
edit_fields : ["re_d_id","re_id","re_d_key","re_d_value","re_d_label","re_d_order","re_d_active"],
list_limit : 20,
list_fields : ["attr_label","re_d_value","re_d_order"],
order_by : 're_d_id',
order_sort : 'ASC',
filters : {"re_d_key":{"type":"table","anchor":0,"table":"attributes","key":"attr_name","value":"attr_label", "filter" : 1, "filter_key" : "attr_group", "filter_value" : "realestate_details", "order" : 1, "order_by" : "attr_name", "order_sort" : "ASC" }},
primary_key : 're_d_id',
primary_title : 're_d_key',
active_key : 're_d_active',
actual_values : {"re_d_key" : "attr_label"},
required_key : 're_id',
required_value : 're_id',
required_table : 'realestate_data',
actions_edit : <?php echo ($admin_access->controller_realestate_details->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_details->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_features) ) { ?>
		
'realestate_features' : { label : 'Feature',
fields : ["re_f_id","re_id","attr_id","re_f_active"],
add_fields : ["re_id","attr_id","re_f_active"],
edit_fields : ["re_f_id","re_id","attr_id","re_f_active"],
list_limit : 20,
list_fields : ["attr_label"],
order_by : 're_f_id',
order_sort : 'ASC',
filters : {"attr_id":{"type":"table","anchor":0,"table":"attributes","key":"attr_id","value":"attr_label", "filter" : 1, "filter_key" : "attr_group", "filter_value" : "re_features", "order" : 1, "order_by" : "attr_label", "order_sort" : "ASC" }},
primary_key : 're_f_id',
active_key : 're_f_active',
actual_values : {"attr_id" : "attr_label"},
required_key : 're_id',
required_value : 're_id',
required_table : 'realestate_data',
actions_edit : <?php echo ($admin_access->controller_realestate_features->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_features->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_media) ) { ?>
		
'realestate_media' : { label : 'Real Estate Media',
fields : ["re_med_id","re_id","re_med_group","media_id","media_thumb","media_type","media_caption","media_link","re_med_order","re_med_active","media_name"],
add_fields : ["re_id","re_med_group","media_id","media_thumb","media_type","media_caption","media_link","re_med_order","re_med_active","media_name"],
edit_fields : ["re_med_id","re_id","re_med_group","media_id","media_thumb","media_type","media_caption","media_link","re_med_order","re_med_active","media_name"],
list_limit : 20,
list_fields : ["re_med_group","file_name","media_type","media_caption","re_med_order"],
order_by : 're_med_order',
order_sort : 'ASC',
filters : {"re_med_group":{"type":"manual","anchor":0}},
primary_key : 're_med_id',
active_key : 're_med_active',
actual_values : {"re_id" : "re_title_m","media_id" : "file_name"},
required_key : 're_id',
required_value : 're_id',
required_table : 'realestate_data',
actions_edit : <?php echo ($admin_access->controller_realestate_media->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_media->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_location) ) { ?>
		
'realestate_location' : { label : 'Location',
fields : ["re_loc_id","re_id","loc_id","re_loc_active"],
add_fields : ["re_id","loc_id","re_loc_active"],
edit_fields : ["re_id","loc_id","re_loc_active"],
list_limit : 20,
list_fields : ["loc_name_re"],
order_by : 're_loc_id',
order_sort : 'ASC',
primary_key : 're_loc_id',
primary_title : 'loc_id',
active_key : 're_loc_active',
actual_values : {"re_id" : "re_title","loc_id" : "loc_name_re"},
required_key : 're_id',
required_value : 're_id',
required_table : 'realestate_data',
actions_edit : <?php echo ($admin_access->controller_realestate_location->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_location->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_meta) ) { ?>
		
'realestate_meta' : { label : 'Meta Value',
fields : ["re_m_id","re_id","meta_key","meta_value","re_m_active"],
add_fields : ["re_id","meta_key","meta_value","re_m_active"],
edit_fields : ["re_m_id","re_id","meta_key","meta_value","re_m_active"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 're_m_id',
order_sort : 'ASC',
primary_key : 're_m_id',
primary_title : 'meta_key',
required_key : 're_id',
required_value : 're_id',
required_table : 'realestate_data',
actions_edit : <?php echo ($admin_access->controller_realestate_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_ancestors) ) { ?>
		
'realestate_ancestors' : { label : 'Ancestor',
fields : ["re_anc_primary","re_anc_id","re_id","ancestor_id","re_anc_active"],
add_fields : ["re_id","ancestor_id","re_anc_active"],
edit_fields : ["re_anc_id","re_id","ancestor_id","re_anc_active"],
list_limit : 20,
list_fields : ["re_anc_id","re_title_2"],
order_by : 're_anc_id',
order_sort : 'ASC',
primary_key : 're_anc_id',
primary_title : 'ancestor_id',
active_key : 're_anc_active',
actual_values : {"ancestor_id" : "re_title_2"},
required_key : 're_id',
required_value : 're_id',
required_table : 'realestate_data_1',
actions_edit : <?php echo ($admin_access->controller_realestate_ancestors->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_ancestors->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_realestate_types) ) { ?>
		
'realestate_types' : { label : 'Property Type',
fields : ["re_type_id","re_id","type_name","re_type_active"],
add_fields : ["re_id","type_name","re_type_active"],
edit_fields : ["re_type_id","re_id","type_name","re_type_active"],
list_limit : 20,
list_fields : ["type_name"],
order_by : 're_type_id',
order_sort : 'DESC',
filters : {"type_name":{"type":"table","anchor":0,"table":"attributes","key":"attr_name","value":"attr_label", "filter" : 1, "filter_key" : "attr_group", "filter_value" : "realestate_property_type", "order" : 1, "order_by" : "attr_name", "order_sort" : "ASC" }},
primary_key : 're_type_id',
active_key : 're_type_active',
actual_values : {"re_id" : "re_title","type_name" : "attr_label"},
required_key : 're_id',
required_value : 're_id',
required_table : 'realestate_data',
actions_edit : <?php echo ($admin_access->controller_realestate_types->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_realestate_types->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"re_f_active": {"1": "Active"}, "re_med_group": {"slider": "Slider", "gallery": "Gallery", "general": "General Media"}, "re_active": {"1": "Active"}, "re_m_active": {"1": "Active"}, "re_loc_active": {"1": "Active"}, "re_status": {"draft": "Draft", "publish": "Published", "private": "Private", "user": "Users Only"}, "re_anc_active": {"1": "Active"}, "re_d_active": {"1": "Active"}, "media_type": {"image": "Image", "video": "Video"}, "re_med_active": {"1": "Set Active"}, "re_type_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>