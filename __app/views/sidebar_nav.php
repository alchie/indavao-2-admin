<li <?php echo ($main_page == 'dashboard') ? 'class="active"' : ''; ?>>
    <a id="menu-dashboard" href="<?php echo site_url('dashboard'); ?>" title="Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>
<?php if( isset($admin_access->controller_realestate_data) || isset($admin_access->controller_directory_data) || isset($admin_access->controller_media_uploads) || isset($admin_access->controller_contact_messages) ) { ?>

<li <?php echo ($main_page == 'content') ? 'class="active"' : ''; ?>>
    <a id="menu-content" href="#content" title="content" class=""><i class="fa fa-book fa-fw"></i> Contents<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_realestate_data) ) { ?>
					<li <?php echo ($sub_page == 'realestate_data') ? 'class="active"' : ''; ?>>
		<a id="menu-realestate_data" href="<?php echo site_url('realestate_data'); ?>" title="Real Estate" class=""><i class="fa fa-home fa-fw"></i> Real Estate</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_directory_data) ) { ?>
					<li <?php echo ($sub_page == 'directory_data') ? 'class="active"' : ''; ?>>
		<a id="menu-directory_data" href="<?php echo site_url('directory_data'); ?>" title="Directory" class=""><i class="fa fa-cutlery fa-fw"></i> Directory</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_media_uploads) ) { ?>
					<li <?php echo ($sub_page == 'media_uploads') ? 'class="active"' : ''; ?>>
		<a id="menu-media_uploads" href="<?php echo site_url('media_uploads'); ?>" title="Media Uploads" class=""><i class="fa fa-file-image-o fa-fw"></i> Media Uploads</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_contact_messages) ) { ?>
					<li <?php echo ($sub_page == 'contact_messages') ? 'class="active"' : ''; ?>>
		<a id="menu-contact_messages" href="<?php echo site_url('contact_messages'); ?>" title="Messages" class=""><i class="fa fa-envelope fa-fw"></i> Messages</a>
	</li>
	<?php } ?></ul></li>
<?php } ?>
<?php if( isset($admin_access->controller_tags) || isset($admin_access->controller_taxonomies) || isset($admin_access->controller_locations) ) { ?>

<li <?php echo ($main_page == 'taxonomies') ? 'class="active"' : ''; ?>>
    <a id="menu-taxonomies" href="#taxonomies" title="taxonomies" class=""><i class="fa fa-bars fa-fw"></i> Taxonomies<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_tags) ) { ?>
					<li <?php echo ($sub_page == 'tags') ? 'class="active"' : ''; ?>>
		<a id="menu-tags" href="<?php echo site_url('tags'); ?>" title="Tags" class=""><i class="glyphicon glyphicon-tags"></i> Tags</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_taxonomies) ) { ?>
					<li <?php echo ($sub_page == 'taxonomies') ? 'class="active"' : ''; ?>>
		<a id="menu-taxonomies" href="<?php echo site_url('taxonomies'); ?>" title="Taxonomies" class=""><i class="fa fa-sitemap fa-fw"></i> Taxonomies</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_locations) ) { ?>
					<li <?php echo ($sub_page == 'locations') ? 'class="active"' : ''; ?>>
		<a id="menu-locations" href="<?php echo site_url('locations'); ?>" title="Locations" class=""><i class="fa fa-map-marker fa-fw"></i> Locations</a>
	</li>
	<?php } ?></ul></li>
<?php } ?>
<?php if( isset($admin_access->controller_users) || isset($admin_access->controller_admins) ) { ?>

<li <?php echo ($main_page == 'accounts') ? 'class="active"' : ''; ?>>
    <a id="menu-accounts" href="#accounts" title="accounts" class=""><i class="fa fa-group fa-fw"></i> Accounts<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_users) ) { ?>
					<li <?php echo ($sub_page == 'users') ? 'class="active"' : ''; ?>>
		<a id="menu-users" href="<?php echo site_url('users'); ?>" title="Users" class=""><i class="fa fa-user fa-fw"></i> Users</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_admins) ) { ?>
					<li <?php echo ($sub_page == 'admins') ? 'class="active"' : ''; ?>>
		<a id="menu-admins" href="<?php echo site_url('admins'); ?>" title="Admins" class=""><i class="fa fa-bug fa-fw"></i> Admins</a>
	</li>
	<?php } ?></ul></li>
<?php } ?>
<?php if( isset($admin_access->controller_settings) || isset($admin_access->controller_attributes) || isset($admin_access->controller_tasks_urls) ) { ?>

<li <?php echo ($main_page == 'settings') ? 'class="active"' : ''; ?>>
    <a id="menu-settings" href="#settings" title="settings" class=""><i class="fa fa-cogs fa-fw"></i> Settings<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_settings) ) { ?>
					<li <?php echo ($sub_page == 'settings') ? 'class="active"' : ''; ?>>
		<a id="menu-settings" href="<?php echo site_url('settings'); ?>" title="Settings" class=""><i class="fa fa-cog fa-fw"></i> Settings</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_attributes) ) { ?>
					<li <?php echo ($sub_page == 'attributes') ? 'class="active"' : ''; ?>>
		<a id="menu-attributes" href="<?php echo site_url('attributes'); ?>" title="Attributes" class=""><i class="fa fa-info fa-fw"></i> Attributes</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_tasks_urls) ) { ?>
					<li <?php echo ($sub_page == 'tasks_urls') ? 'class="active"' : ''; ?>>
		<a id="menu-tasks_urls" href="<?php echo site_url('tasks_urls'); ?>" title="Tasks" class=""><i class="fa fa-tasks fa-fw"></i> Tasks</a>
	</li>
	<?php } ?></ul></li>
<?php } ?><?php if( isset($admin_access->controller_media_data) ) { ?>
<li <?php echo ($main_page == 'media_data') ? 'class="active"' : ''; ?>>
    <a id="menu-media_data" href="<?php echo site_url('media_data'); ?>" title="media_data" class=""><i class="fa fa-bars fa-fw"></i> Media<span class="fa arrow"></span></a>
</li>
<?php } ?>