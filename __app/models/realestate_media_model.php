<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Realestate_media_model Class
 *
 * Manipulates `realestate_media` table on database

CREATE TABLE `realestate_media` (
  `re_med_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `media_id` bigint(20) NOT NULL,
  `media_thumb` bigint(20) DEFAULT NULL,
  `media_type` varchar(10) DEFAULT NULL,
  `media_caption` text,
  `media_link` text,
  `media_name` varchar(100) DEFAULT NULL,
  `re_med_group` varchar(100) DEFAULT NULL,
  `re_med_order` int(10) DEFAULT '0',
  `re_med_active` int(1) NOT NULL,
  PRIMARY KEY (`re_med_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `REMD_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

 * @package			Model
 * @project			InDavao Network
 * @project_link	http://www.indavao.net
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com
 */
 
class Realestate_media_model extends CI_Model {

	protected $re_med_id;
	protected $re_id;
	protected $media_id;
	protected $media_thumb;
	protected $media_type;
	protected $media_caption;
	protected $media_link;
	protected $media_name;
	protected $re_med_group;
	protected $re_med_order = '0';
	protected $re_med_active = '0';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: re_med_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_med_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReMedId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_med_id = ( ($value == '') && ($this->re_med_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.re_med_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_med_id';
		}
		return $this;
	}

	/** 
	* Get the value of `re_med_id` variable
	* @access public
	* @return String;
	*/

	public function getReMedId() {
		return $this->re_med_id;
	}

	/**
	* Get row by `re_med_id`
	* @param re_med_id
	* @return QueryResult
	**/

	public function getByReMedId() {
		if($this->re_med_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.re_med_id' => $this->re_med_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_med_id`
	**/

	public function updateByReMedId() {
		if($this->re_med_id != '') {
			$this->setExclude('re_med_id');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.re_med_id' => $this->re_med_id ) );
			}
		}
	}


	/**
	* Delete row by `re_med_id`
	**/

	public function deleteByReMedId() {
		if($this->re_med_id != '') {
			return $this->db->delete('realestate_media', array('realestate_media.re_med_id' => $this->re_med_id ) );
		}
	}

	/**
	* Increment row by `re_med_id`
	**/

	public function incrementByReMedId() {
		if($this->re_med_id != '' && $this->countField != '') {
			$this->db->where('re_med_id', $this->re_med_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `re_med_id`
	**/

	public function decrementByReMedId() {
		if($this->re_med_id != '' && $this->countField != '') {
			$this->db->where('re_med_id', $this->re_med_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_med_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_id = ( ($value == '') && ($this->re_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.re_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_id';
		}
		return $this;
	}

	/** 
	* Get the value of `re_id` variable
	* @access public
	* @return String;
	*/

	public function getReId() {
		return $this->re_id;
	}

	/**
	* Get row by `re_id`
	* @param re_id
	* @return QueryResult
	**/

	public function getByReId() {
		if($this->re_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.re_id' => $this->re_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_id`
	**/

	public function updateByReId() {
		if($this->re_id != '') {
			$this->setExclude('re_id');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.re_id' => $this->re_id ) );
			}
		}
	}


	/**
	* Delete row by `re_id`
	**/

	public function deleteByReId() {
		if($this->re_id != '') {
			return $this->db->delete('realestate_media', array('realestate_media.re_id' => $this->re_id ) );
		}
	}

	/**
	* Increment row by `re_id`
	**/

	public function incrementByReId() {
		if($this->re_id != '' && $this->countField != '') {
			$this->db->where('re_id', $this->re_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `re_id`
	**/

	public function decrementByReId() {
		if($this->re_id != '' && $this->countField != '') {
			$this->db->where('re_id', $this->re_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: media_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `media_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMediaId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->media_id = ( ($value == '') && ($this->media_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.media_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'media_id';
		}
		return $this;
	}

	/** 
	* Get the value of `media_id` variable
	* @access public
	* @return String;
	*/

	public function getMediaId() {
		return $this->media_id;
	}

	/**
	* Get row by `media_id`
	* @param media_id
	* @return QueryResult
	**/

	public function getByMediaId() {
		if($this->media_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.media_id' => $this->media_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `media_id`
	**/

	public function updateByMediaId() {
		if($this->media_id != '') {
			$this->setExclude('media_id');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.media_id' => $this->media_id ) );
			}
		}
	}


	/**
	* Delete row by `media_id`
	**/

	public function deleteByMediaId() {
		if($this->media_id != '') {
			return $this->db->delete('realestate_media', array('realestate_media.media_id' => $this->media_id ) );
		}
	}

	/**
	* Increment row by `media_id`
	**/

	public function incrementByMediaId() {
		if($this->media_id != '' && $this->countField != '') {
			$this->db->where('media_id', $this->media_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `media_id`
	**/

	public function decrementByMediaId() {
		if($this->media_id != '' && $this->countField != '') {
			$this->db->where('media_id', $this->media_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: media_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: media_thumb
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `media_thumb` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMediaThumb($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->media_thumb = ( ($value == '') && ($this->media_thumb != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.media_thumb';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'media_thumb';
		}
		return $this;
	}

	/** 
	* Get the value of `media_thumb` variable
	* @access public
	* @return String;
	*/

	public function getMediaThumb() {
		return $this->media_thumb;
	}

	/**
	* Get row by `media_thumb`
	* @param media_thumb
	* @return QueryResult
	**/

	public function getByMediaThumb() {
		if($this->media_thumb != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.media_thumb' => $this->media_thumb), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `media_thumb`
	**/

	public function updateByMediaThumb() {
		if($this->media_thumb != '') {
			$this->setExclude('media_thumb');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.media_thumb' => $this->media_thumb ) );
			}
		}
	}


	/**
	* Delete row by `media_thumb`
	**/

	public function deleteByMediaThumb() {
		if($this->media_thumb != '') {
			return $this->db->delete('realestate_media', array('realestate_media.media_thumb' => $this->media_thumb ) );
		}
	}

	/**
	* Increment row by `media_thumb`
	**/

	public function incrementByMediaThumb() {
		if($this->media_thumb != '' && $this->countField != '') {
			$this->db->where('media_thumb', $this->media_thumb);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `media_thumb`
	**/

	public function decrementByMediaThumb() {
		if($this->media_thumb != '' && $this->countField != '') {
			$this->db->where('media_thumb', $this->media_thumb);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: media_thumb
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: media_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `media_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMediaType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->media_type = ( ($value == '') && ($this->media_type != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.media_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'media_type';
		}
		return $this;
	}

	/** 
	* Get the value of `media_type` variable
	* @access public
	* @return String;
	*/

	public function getMediaType() {
		return $this->media_type;
	}

	/**
	* Get row by `media_type`
	* @param media_type
	* @return QueryResult
	**/

	public function getByMediaType() {
		if($this->media_type != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.media_type' => $this->media_type), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `media_type`
	**/

	public function updateByMediaType() {
		if($this->media_type != '') {
			$this->setExclude('media_type');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.media_type' => $this->media_type ) );
			}
		}
	}


	/**
	* Delete row by `media_type`
	**/

	public function deleteByMediaType() {
		if($this->media_type != '') {
			return $this->db->delete('realestate_media', array('realestate_media.media_type' => $this->media_type ) );
		}
	}

	/**
	* Increment row by `media_type`
	**/

	public function incrementByMediaType() {
		if($this->media_type != '' && $this->countField != '') {
			$this->db->where('media_type', $this->media_type);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `media_type`
	**/

	public function decrementByMediaType() {
		if($this->media_type != '' && $this->countField != '') {
			$this->db->where('media_type', $this->media_type);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: media_type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: media_caption
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `media_caption` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMediaCaption($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->media_caption = ( ($value == '') && ($this->media_caption != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.media_caption';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'media_caption';
		}
		return $this;
	}

	/** 
	* Get the value of `media_caption` variable
	* @access public
	* @return String;
	*/

	public function getMediaCaption() {
		return $this->media_caption;
	}

	/**
	* Get row by `media_caption`
	* @param media_caption
	* @return QueryResult
	**/

	public function getByMediaCaption() {
		if($this->media_caption != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.media_caption' => $this->media_caption), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `media_caption`
	**/

	public function updateByMediaCaption() {
		if($this->media_caption != '') {
			$this->setExclude('media_caption');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.media_caption' => $this->media_caption ) );
			}
		}
	}


	/**
	* Delete row by `media_caption`
	**/

	public function deleteByMediaCaption() {
		if($this->media_caption != '') {
			return $this->db->delete('realestate_media', array('realestate_media.media_caption' => $this->media_caption ) );
		}
	}

	/**
	* Increment row by `media_caption`
	**/

	public function incrementByMediaCaption() {
		if($this->media_caption != '' && $this->countField != '') {
			$this->db->where('media_caption', $this->media_caption);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `media_caption`
	**/

	public function decrementByMediaCaption() {
		if($this->media_caption != '' && $this->countField != '') {
			$this->db->where('media_caption', $this->media_caption);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: media_caption
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: media_link
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `media_link` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMediaLink($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->media_link = ( ($value == '') && ($this->media_link != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.media_link';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'media_link';
		}
		return $this;
	}

	/** 
	* Get the value of `media_link` variable
	* @access public
	* @return String;
	*/

	public function getMediaLink() {
		return $this->media_link;
	}

	/**
	* Get row by `media_link`
	* @param media_link
	* @return QueryResult
	**/

	public function getByMediaLink() {
		if($this->media_link != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.media_link' => $this->media_link), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `media_link`
	**/

	public function updateByMediaLink() {
		if($this->media_link != '') {
			$this->setExclude('media_link');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.media_link' => $this->media_link ) );
			}
		}
	}


	/**
	* Delete row by `media_link`
	**/

	public function deleteByMediaLink() {
		if($this->media_link != '') {
			return $this->db->delete('realestate_media', array('realestate_media.media_link' => $this->media_link ) );
		}
	}

	/**
	* Increment row by `media_link`
	**/

	public function incrementByMediaLink() {
		if($this->media_link != '' && $this->countField != '') {
			$this->db->where('media_link', $this->media_link);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `media_link`
	**/

	public function decrementByMediaLink() {
		if($this->media_link != '' && $this->countField != '') {
			$this->db->where('media_link', $this->media_link);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: media_link
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: media_name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `media_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMediaName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->media_name = ( ($value == '') && ($this->media_name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.media_name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'media_name';
		}
		return $this;
	}

	/** 
	* Get the value of `media_name` variable
	* @access public
	* @return String;
	*/

	public function getMediaName() {
		return $this->media_name;
	}

	/**
	* Get row by `media_name`
	* @param media_name
	* @return QueryResult
	**/

	public function getByMediaName() {
		if($this->media_name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.media_name' => $this->media_name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `media_name`
	**/

	public function updateByMediaName() {
		if($this->media_name != '') {
			$this->setExclude('media_name');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.media_name' => $this->media_name ) );
			}
		}
	}


	/**
	* Delete row by `media_name`
	**/

	public function deleteByMediaName() {
		if($this->media_name != '') {
			return $this->db->delete('realestate_media', array('realestate_media.media_name' => $this->media_name ) );
		}
	}

	/**
	* Increment row by `media_name`
	**/

	public function incrementByMediaName() {
		if($this->media_name != '' && $this->countField != '') {
			$this->db->where('media_name', $this->media_name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `media_name`
	**/

	public function decrementByMediaName() {
		if($this->media_name != '' && $this->countField != '') {
			$this->db->where('media_name', $this->media_name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: media_name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_med_group
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_med_group` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReMedGroup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_med_group = ( ($value == '') && ($this->re_med_group != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.re_med_group';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_med_group';
		}
		return $this;
	}

	/** 
	* Get the value of `re_med_group` variable
	* @access public
	* @return String;
	*/

	public function getReMedGroup() {
		return $this->re_med_group;
	}

	/**
	* Get row by `re_med_group`
	* @param re_med_group
	* @return QueryResult
	**/

	public function getByReMedGroup() {
		if($this->re_med_group != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.re_med_group' => $this->re_med_group), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_med_group`
	**/

	public function updateByReMedGroup() {
		if($this->re_med_group != '') {
			$this->setExclude('re_med_group');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.re_med_group' => $this->re_med_group ) );
			}
		}
	}


	/**
	* Delete row by `re_med_group`
	**/

	public function deleteByReMedGroup() {
		if($this->re_med_group != '') {
			return $this->db->delete('realestate_media', array('realestate_media.re_med_group' => $this->re_med_group ) );
		}
	}

	/**
	* Increment row by `re_med_group`
	**/

	public function incrementByReMedGroup() {
		if($this->re_med_group != '' && $this->countField != '') {
			$this->db->where('re_med_group', $this->re_med_group);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `re_med_group`
	**/

	public function decrementByReMedGroup() {
		if($this->re_med_group != '' && $this->countField != '') {
			$this->db->where('re_med_group', $this->re_med_group);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_med_group
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_med_order
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_med_order` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReMedOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_med_order = ( ($value == '') && ($this->re_med_order != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.re_med_order';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_med_order';
		}
		return $this;
	}

	/** 
	* Get the value of `re_med_order` variable
	* @access public
	* @return String;
	*/

	public function getReMedOrder() {
		return $this->re_med_order;
	}

	/**
	* Get row by `re_med_order`
	* @param re_med_order
	* @return QueryResult
	**/

	public function getByReMedOrder() {
		if($this->re_med_order != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.re_med_order' => $this->re_med_order), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_med_order`
	**/

	public function updateByReMedOrder() {
		if($this->re_med_order != '') {
			$this->setExclude('re_med_order');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.re_med_order' => $this->re_med_order ) );
			}
		}
	}


	/**
	* Delete row by `re_med_order`
	**/

	public function deleteByReMedOrder() {
		if($this->re_med_order != '') {
			return $this->db->delete('realestate_media', array('realestate_media.re_med_order' => $this->re_med_order ) );
		}
	}

	/**
	* Increment row by `re_med_order`
	**/

	public function incrementByReMedOrder() {
		if($this->re_med_order != '' && $this->countField != '') {
			$this->db->where('re_med_order', $this->re_med_order);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `re_med_order`
	**/

	public function decrementByReMedOrder() {
		if($this->re_med_order != '' && $this->countField != '') {
			$this->db->where('re_med_order', $this->re_med_order);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_med_order
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_med_active
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_med_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReMedActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_med_active = ( ($value == '') && ($this->re_med_active != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_media.re_med_active';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_med_active';
		}
		return $this;
	}

	/** 
	* Get the value of `re_med_active` variable
	* @access public
	* @return String;
	*/

	public function getReMedActive() {
		return $this->re_med_active;
	}

	/**
	* Get row by `re_med_active`
	* @param re_med_active
	* @return QueryResult
	**/

	public function getByReMedActive() {
		if($this->re_med_active != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_media', array('realestate_media.re_med_active' => $this->re_med_active), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_med_active`
	**/

	public function updateByReMedActive() {
		if($this->re_med_active != '') {
			$this->setExclude('re_med_active');
			if( $this->getData() ) {
				return $this->db->update('realestate_media', $this->getData(), array('realestate_media.re_med_active' => $this->re_med_active ) );
			}
		}
	}


	/**
	* Delete row by `re_med_active`
	**/

	public function deleteByReMedActive() {
		if($this->re_med_active != '') {
			return $this->db->delete('realestate_media', array('realestate_media.re_med_active' => $this->re_med_active ) );
		}
	}

	/**
	* Increment row by `re_med_active`
	**/

	public function incrementByReMedActive() {
		if($this->re_med_active != '' && $this->countField != '') {
			$this->db->where('re_med_active', $this->re_med_active);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_media');
		}
	}

	/**
	* Decrement row by `re_med_active`
	**/

	public function decrementByReMedActive() {
		if($this->re_med_active != '' && $this->countField != '') {
			$this->db->where('re_med_active', $this->re_med_active);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_media');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_med_active
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('realestate_media', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('realestate_media');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('realestate_media', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('realestate_media', $this->getData() ) === TRUE ) {
				$this->re_med_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('realestate_media', $this->getData() ) === TRUE ) {
				$this->re_med_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('realestate_media');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('realestate_media');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='re_med_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('realestate_media');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'realestate_media.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("re_med_id","re_id","media_id","media_thumb","media_type","media_caption","media_link","media_name","re_med_group","re_med_order","re_med_active");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/

        protected function __setCondition($condition, $key, $value, $priority) {
		switch( $condition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'realestate_media'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('realestate_media');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('realestate_media');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('realestate_media', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file realestate_media_model.php */
/* Location: ./application/models/realestate_media_model.php */
