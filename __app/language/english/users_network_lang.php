<?php

// users_network

$lang['users_network_id'] = 'Id';
$lang['users_network_user_id'] = 'User Id';
$lang['users_network_referrer_id'] = 'Referrer Id';
$lang['users_network_assignment_id'] = 'Assignment Id';
$lang['users_network_assignment_wing'] = 'Assignment Wing';

/* End of file users_network_lang.php */

/* Location: ./application/language/english/users_network_lang.php */
