<?php

// directory_details

$lang['directory_details_dir_d_id'] = 'ID';
$lang['directory_details_dir_id'] = 'Directory';
$lang['directory_details_dir_d_key'] = 'Detail Key';
$lang['directory_details_dir_d_value'] = 'Value';
$lang['directory_details_dir_d_label'] = 'Label';
$lang['directory_details_dir_d_active'] = 'Active';
$lang['directory_details_dir_d_order'] = 'Order';

/* End of file directory_details_lang.php */

/* Location: ./application/language/english/directory_details_lang.php */
