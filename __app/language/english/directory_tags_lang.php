<?php

// directory_tags

$lang['directory_tags_dir_tag_id'] = 'ID';
$lang['directory_tags_dir_id'] = 'Directory';
$lang['directory_tags_tag_id'] = 'Tag';
$lang['directory_tags_dir_tag_active'] = 'Active';

/* End of file directory_tags_lang.php */

/* Location: ./application/language/english/directory_tags_lang.php */
