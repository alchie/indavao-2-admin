<?php

// directory_category

$lang['directory_category_dir_cat_id'] = 'ID';
$lang['directory_category_dir_id'] = 'Directory';
$lang['directory_category_tax_id'] = 'Taxonomy';
$lang['directory_category_dir_cat_active'] = 'Active';

/* End of file directory_category_lang.php */

/* Location: ./application/language/english/directory_category_lang.php */
