<?php

// users_shares

$lang['users_shares_share_id'] = 'ID';
$lang['users_shares_user_id'] = 'User ID';
$lang['users_shares_object_id'] = 'Object ID';
$lang['users_shares_object_type'] = 'Object';
$lang['users_shares_fb_post_id'] = 'FB Post ID';
$lang['users_shares_likes'] = 'Liked';

/* End of file users_shares_lang.php */

/* Location: ./application/language/english/users_shares_lang.php */
