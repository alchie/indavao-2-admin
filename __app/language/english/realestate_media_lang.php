<?php

// realestate_media

$lang['realestate_media_re_med_id'] = 'ID';
$lang['realestate_media_re_id'] = 'Real Estate';
$lang['realestate_media_media_id'] = 'Media File';
$lang['realestate_media_media_thumb'] = 'Thumbnail';
$lang['realestate_media_media_type'] = 'Media Type';
$lang['realestate_media_media_caption'] = 'Caption';
$lang['realestate_media_media_link'] = 'Link';
$lang['realestate_media_media_name'] = 'Name';
$lang['realestate_media_re_med_group'] = 'Group';
$lang['realestate_media_re_med_order'] = 'Order';
$lang['realestate_media_re_med_active'] = 'Active';

/* End of file realestate_media_lang.php */

/* Location: ./application/language/english/realestate_media_lang.php */
