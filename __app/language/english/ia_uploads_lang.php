<?php

// ia_uploads

$lang['ia_uploads_media_id'] = 'Media ID';
$lang['ia_uploads_file_name'] = 'Filename';
$lang['ia_uploads_file_type'] = 'File Type';
$lang['ia_uploads_file_path'] = 'File Path';
$lang['ia_uploads_full_path'] = 'Full Path';
$lang['ia_uploads_raw_name'] = 'Raw Name';
$lang['ia_uploads_orig_name'] = 'Original Name';
$lang['ia_uploads_client_name'] = 'Client Name';
$lang['ia_uploads_file_ext'] = 'File Extension';
$lang['ia_uploads_file_size'] = 'File Size';
$lang['ia_uploads_is_image'] = 'Is Image';
$lang['ia_uploads_image_width'] = 'Image Width';
$lang['ia_uploads_image_height'] = 'Image Height';
$lang['ia_uploads_image_type'] = 'Image Type';
$lang['ia_uploads_image_size_str'] = 'Image Size String';

/* End of file ia_uploads_lang.php */

/* Location: ./application/language/english/ia_uploads_lang.php */
