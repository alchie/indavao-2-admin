<?php

// tasks_urls

$lang['tasks_urls_tasks_url_id'] = 'ID';
$lang['tasks_urls_page_id'] = 'Page ID';
$lang['tasks_urls_page_name'] = 'Page Name';
$lang['tasks_urls_page_url'] = 'Page URL';
$lang['tasks_urls_object_type'] = 'Object Type';
$lang['tasks_urls_active'] = 'Active';

/* End of file tasks_urls_lang.php */

/* Location: ./application/language/english/tasks_urls_lang.php */
