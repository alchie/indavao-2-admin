<?php

// tasks_verified

$lang['tasks_verified_tasks_ver_id'] = 'Tasks Ver Id';
$lang['tasks_verified_user_id'] = 'User Id';
$lang['tasks_verified_tasks_url_id'] = 'Tasks Url Id';
$lang['tasks_verified_object_id'] = 'Object Id';
$lang['tasks_verified_object_type'] = 'Object Type';
$lang['tasks_verified_post_url'] = 'Post URL';
$lang['tasks_verified_date_verified'] = 'Date Verified';

/* End of file tasks_verified_lang.php */

/* Location: ./application/language/english/tasks_verified_lang.php */
