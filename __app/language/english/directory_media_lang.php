<?php

// directory_media

$lang['directory_media_dir_med_id'] = 'ID';
$lang['directory_media_dir_id'] = 'Directory';
$lang['directory_media_media_id'] = 'Media';
$lang['directory_media_media_thumb'] = 'Thumbnail';
$lang['directory_media_media_type'] = 'Type';
$lang['directory_media_media_caption'] = 'Caption';
$lang['directory_media_media_link'] = 'Link';
$lang['directory_media_media_name'] = 'Name';
$lang['directory_media_dir_med_group'] = 'Group';
$lang['directory_media_dir_med_order'] = 'Order';
$lang['directory_media_dir_med_active'] = 'Active';

/* End of file directory_media_lang.php */

/* Location: ./application/language/english/directory_media_lang.php */
