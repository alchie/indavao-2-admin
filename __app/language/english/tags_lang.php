<?php

// tags

$lang['tags_tag_id'] = 'ID';
$lang['tags_tag_name'] = 'Name';
$lang['tags_tag_slug'] = 'Slug';
$lang['tags_tag_count'] = 'Count';

/* End of file tags_lang.php */

/* Location: ./application/language/english/tags_lang.php */
