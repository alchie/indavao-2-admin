<?php

// users_permissions

$lang['users_permissions_permission_id'] = 'ID';
$lang['users_permissions_user_id'] = 'User';
$lang['users_permissions_permission'] = 'Permission';
$lang['users_permissions_active'] = 'Active';

/* End of file users_permissions_lang.php */

/* Location: ./application/language/english/users_permissions_lang.php */
