<?php

// realestate_meta

$lang['realestate_meta_re_m_id'] = 'ID';
$lang['realestate_meta_re_id'] = 'Real Estate ID';
$lang['realestate_meta_meta_key'] = 'Meta Key';
$lang['realestate_meta_meta_value'] = 'Meta Value';
$lang['realestate_meta_re_m_active'] = 'Active';

/* End of file realestate_meta_lang.php */

/* Location: ./application/language/english/realestate_meta_lang.php */
