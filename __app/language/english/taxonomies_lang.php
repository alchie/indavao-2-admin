<?php

// taxonomies

$lang['taxonomies_tax_id'] = 'ID';
$lang['taxonomies_tax_name'] = 'Name';
$lang['taxonomies_tax_label'] = 'Label';
$lang['taxonomies_tax_type'] = 'Type';
$lang['taxonomies_tax_active'] = 'Active';

/* End of file taxonomies_lang.php */

/* Location: ./application/language/english/taxonomies_lang.php */
