<?php

// users_meta

$lang['users_meta_user_meta_id'] = 'ID';
$lang['users_meta_user_id'] = 'User';
$lang['users_meta_meta_key'] = 'Key';
$lang['users_meta_meta_value'] = 'Value';
$lang['users_meta_user_meta_active'] = 'Active';

/* End of file users_meta_lang.php */

/* Location: ./application/language/english/users_meta_lang.php */
