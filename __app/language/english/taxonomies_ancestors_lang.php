<?php

// taxonomies_ancestors

$lang['taxonomies_ancestors_tax_anc_id'] = 'ID';
$lang['taxonomies_ancestors_tax_id'] = 'Taxonomy';
$lang['taxonomies_ancestors_tax_parent'] = 'Parent Taxonomy';
$lang['taxonomies_ancestors_tax_anc_active'] = 'Active';

/* End of file taxonomies_ancestors_lang.php */

/* Location: ./application/language/english/taxonomies_ancestors_lang.php */
