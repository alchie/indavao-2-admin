<?php

// directory_data

$lang['directory_data_dir_id'] = 'ID';
$lang['directory_data_dir_name'] = 'Name';
$lang['directory_data_dir_slug'] = 'Slug';
$lang['directory_data_dir_branch'] = 'Branch';
$lang['directory_data_dir_active'] = 'Active';
$lang['directory_data_dir_verified'] = 'Verified';
$lang['directory_data_dir_added'] = 'Date Added';
$lang['directory_data_user_id'] = 'User';
$lang['directory_data_dir_status'] = 'Status';

/* End of file directory_data_lang.php */

/* Location: ./application/language/english/directory_data_lang.php */
