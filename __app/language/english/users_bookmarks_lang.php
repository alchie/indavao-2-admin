<?php

// users_bookmarks

$lang['users_bookmarks_bookmark_id'] = 'ID';
$lang['users_bookmarks_user_id'] = 'User';
$lang['users_bookmarks_object_id'] = 'Object ID';
$lang['users_bookmarks_object_type'] = 'Object Type';
$lang['users_bookmarks_date_added'] = 'Date Added';

/* End of file users_bookmarks_lang.php */

/* Location: ./application/language/english/users_bookmarks_lang.php */
