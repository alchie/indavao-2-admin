<?php

// realestate_ancestors

$lang['realestate_ancestors_re_anc_id'] = 'ID';
$lang['realestate_ancestors_re_id'] = 'Title';
$lang['realestate_ancestors_ancestor_id'] = 'Parent';
$lang['realestate_ancestors_re_anc_primary'] = 'Re Anc Primary';
$lang['realestate_ancestors_re_anc_active'] = 'Active';

/* End of file realestate_ancestors_lang.php */

/* Location: ./application/language/english/realestate_ancestors_lang.php */
