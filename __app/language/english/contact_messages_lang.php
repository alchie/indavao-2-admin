<?php

// contact_messages

$lang['contact_messages_msg_id'] = 'ID';
$lang['contact_messages_object_id'] = 'Object ID';
$lang['contact_messages_object_type'] = 'Object Type';
$lang['contact_messages_name'] = 'Name';
$lang['contact_messages_phone'] = 'Phone';
$lang['contact_messages_email'] = 'Email';
$lang['contact_messages_message'] = 'Message';
$lang['contact_messages_date_sent'] = 'Date Sent';
$lang['contact_messages_user_id'] = 'User';
$lang['contact_messages_msg_type'] = 'Type';

/* End of file contact_messages_lang.php */

/* Location: ./application/language/english/contact_messages_lang.php */
