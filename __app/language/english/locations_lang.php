<?php

// locations

$lang['locations_loc_id'] = 'ID';
$lang['locations_loc_name'] = 'Name';
$lang['locations_loc_slug'] = 'Slug';
$lang['locations_loc_type'] = 'Type';
$lang['locations_loc_order'] = 'Order';
$lang['locations_loc_active'] = 'Active';

/* End of file locations_lang.php */

/* Location: ./application/language/english/locations_lang.php */
