<?php

// users_points_withdrawals

$lang['users_points_withdrawals_withdrawal_id'] = 'ID';
$lang['users_points_withdrawals_user_id'] = 'User';
$lang['users_points_withdrawals_date_withdrawn'] = 'Date Withdrawn';
$lang['users_points_withdrawals_points_withdrawn'] = 'Points Withdrawn';
$lang['users_points_withdrawals_withdrawal_reason'] = 'Withdrawal Reason';

/* End of file users_points_withdrawals_lang.php */

/* Location: ./application/language/english/users_points_withdrawals_lang.php */
