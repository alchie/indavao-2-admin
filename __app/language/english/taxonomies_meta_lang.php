<?php

// taxonomies_meta

$lang['taxonomies_meta_tax_m_id'] = 'ID';
$lang['taxonomies_meta_tax_id'] = 'Taxonomy';
$lang['taxonomies_meta_meta_key'] = 'Meta Key';
$lang['taxonomies_meta_meta_value'] = 'Meta Value';
$lang['taxonomies_meta_tax_m_active'] = 'Active';

/* End of file taxonomies_meta_lang.php */

/* Location: ./application/language/english/taxonomies_meta_lang.php */
