<?php

// realestate_types

$lang['realestate_types_re_type_id'] = 'ID';
$lang['realestate_types_re_id'] = 'Real Estate';
$lang['realestate_types_type_name'] = 'Type Name';
$lang['realestate_types_re_type_active'] = 'Active';

/* End of file realestate_types_lang.php */

/* Location: ./application/language/english/realestate_types_lang.php */
