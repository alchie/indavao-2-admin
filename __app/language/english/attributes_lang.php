<?php

// attributes

$lang['attributes_attr_id'] = 'ID';
$lang['attributes_attr_name'] = 'Name';
$lang['attributes_attr_label'] = 'Label';
$lang['attributes_attr_value'] = 'Default Value';
$lang['attributes_attr_group'] = 'Group';
$lang['attributes_attr_active'] = 'Active';

/* End of file attributes_lang.php */

/* Location: ./application/language/english/attributes_lang.php */
