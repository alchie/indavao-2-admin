<?php

// realestate_details

$lang['realestate_details_re_d_id'] = 'ID';
$lang['realestate_details_re_id'] = 'Real Estate';
$lang['realestate_details_re_d_key'] = 'Detail Key';
$lang['realestate_details_re_d_value'] = 'Value';
$lang['realestate_details_re_d_label'] = 'Label';
$lang['realestate_details_re_d_active'] = 'Active';
$lang['realestate_details_re_d_order'] = 'Order';

/* End of file realestate_details_lang.php */

/* Location: ./application/language/english/realestate_details_lang.php */
