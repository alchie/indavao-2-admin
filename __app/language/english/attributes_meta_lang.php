<?php

// attributes_meta

$lang['attributes_meta_attr_m_id'] = 'ID';
$lang['attributes_meta_attr_id'] = 'Attribute';
$lang['attributes_meta_meta_key'] = 'Key';
$lang['attributes_meta_meta_value'] = 'Value';
$lang['attributes_meta_active'] = 'Active';

/* End of file attributes_meta_lang.php */

/* Location: ./application/language/english/attributes_meta_lang.php */
