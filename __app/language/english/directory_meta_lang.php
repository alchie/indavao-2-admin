<?php

// directory_meta

$lang['directory_meta_dir_m_id'] = 'ID';
$lang['directory_meta_dir_id'] = 'Directory';
$lang['directory_meta_meta_key'] = 'Meta Key';
$lang['directory_meta_meta_value'] = 'Meta Value';
$lang['directory_meta_active'] = 'Active';

/* End of file directory_meta_lang.php */

/* Location: ./application/language/english/directory_meta_lang.php */
