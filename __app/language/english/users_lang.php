<?php

// users

$lang['users_user_id'] = 'User ID';
$lang['users_api'] = 'Api';
$lang['users_email'] = 'Email';
$lang['users_first_name'] = 'First Name';
$lang['users_last_name'] = 'Last Name';
$lang['users_gender'] = 'Gender';
$lang['users_name'] = 'Full Name';
$lang['users_verified'] = 'Verified';
$lang['users_link'] = 'Link';
$lang['users_referrer'] = 'Referrer';
$lang['users_add_points'] = 'Points';
$lang['users_date_joined'] = 'Date Joined';
$lang['users_last_login'] = 'Last Login';
$lang['users_manager'] = 'Manager';

/* End of file users_lang.php */

/* Location: ./application/language/english/users_lang.php */
