<?php

// locations_meta

$lang['locations_meta_loc_m_id'] = 'ID';
$lang['locations_meta_loc_id'] = 'Location';
$lang['locations_meta_meta_key'] = 'Meta Key';
$lang['locations_meta_meta_value'] = 'Meta Value';
$lang['locations_meta_loc_m_active'] = 'Active';

/* End of file locations_meta_lang.php */

/* Location: ./application/language/english/locations_meta_lang.php */
