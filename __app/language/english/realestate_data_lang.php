<?php

// realestate_data

$lang['realestate_data_re_id'] = 'ID';
$lang['realestate_data_re_title'] = 'Title';
$lang['realestate_data_re_slug'] = 'Slug';
$lang['realestate_data_re_type'] = 'Property Type';
$lang['realestate_data_re_active'] = 'Active';
$lang['realestate_data_re_added'] = 'Date Added';
$lang['realestate_data_re_parent'] = 'Parent';
$lang['realestate_data_user_id'] = 'User Id';
$lang['realestate_data_re_status'] = 'Status';

/* End of file realestate_data_lang.php */

/* Location: ./application/language/english/realestate_data_lang.php */
