<?php

// users_points

$lang['users_points_points_id'] = 'ID';
$lang['users_points_user_id'] = 'User';
$lang['users_points_object_id'] = 'Object ID';
$lang['users_points_object_type'] = 'Object Type';
$lang['users_points_points_type'] = 'Points Code';
$lang['users_points_points_credited'] = 'Points Credited';
$lang['users_points_date_added'] = 'Date Credited';
$lang['users_points_points_claimed'] = 'Claimed';

/* End of file users_points_lang.php */

/* Location: ./application/language/english/users_points_lang.php */
