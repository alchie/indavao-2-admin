<?php

// locations_ancestors

$lang['locations_ancestors_loc_anc_id'] = 'ID';
$lang['locations_ancestors_loc_id'] = 'Location';
$lang['locations_ancestors_loc_parent'] = 'Parent Location';
$lang['locations_ancestors_loc_anc_active'] = 'Active';

/* End of file locations_ancestors_lang.php */

/* Location: ./application/language/english/locations_ancestors_lang.php */
