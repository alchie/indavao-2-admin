-- Table structure for table `admins` 

CREATE TABLE `admins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username` (`username`)
);

-- Table structure for table `admins_access` 

CREATE TABLE `admins_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `add` int(1) DEFAULT '0',
  `edit` int(1) DEFAULT '0',
  `delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `AA_ADMIN_ID` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `admins_sessions` 

CREATE TABLE `admins_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
);

-- Table structure for table `attributes` 

CREATE TABLE `attributes` (
  `attr_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attr_name` varchar(100) NOT NULL,
  `attr_label` varchar(255) NOT NULL,
  `attr_value` text,
  `attr_group` varchar(100) DEFAULT NULL,
  `attr_active` int(1) DEFAULT '0',
  PRIMARY KEY (`attr_id`)
);

-- Table structure for table `attributes_ancestors` 

CREATE TABLE `attributes_ancestors` (
  `attr_anc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attr_id` bigint(20) NOT NULL,
  `ancestor_id` bigint(20) NOT NULL,
  `attr_anc_active` int(1) DEFAULT '0',
  PRIMARY KEY (`attr_anc_id`),
  KEY `attr_id` (`attr_id`),
  CONSTRAINT `AA_ATTR_ID` FOREIGN KEY (`attr_id`) REFERENCES `attributes` (`attr_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `attributes_meta` 

CREATE TABLE `attributes_meta` (
  `attr_m_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attr_id` bigint(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` longtext NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attr_m_id`),
  KEY `attr_id` (`attr_id`),
  CONSTRAINT `AM_ATTR_ID` FOREIGN KEY (`attr_id`) REFERENCES `attributes` (`attr_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `contact_messages` 

CREATE TABLE `contact_messages` (
  `msg_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id` bigint(20) DEFAULT NULL,
  `object_type` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `message` text,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` bigint(20) DEFAULT NULL,
  `msg_type` varchar(100) NOT NULL,
  PRIMARY KEY (`msg_id`)
);

-- Table structure for table `directory_ancestors` 

CREATE TABLE `directory_ancestors` (
  `dir_anc_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `ancestor_id` bigint(20) NOT NULL,
  `dir_anc_active` int(1) DEFAULT '0',
  PRIMARY KEY (`dir_anc_id`),
  KEY `dir_id` (`dir_id`),
  CONSTRAINT `DA_DIR_ID` FOREIGN KEY (`dir_id`) REFERENCES `directory_data` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `directory_category` 

CREATE TABLE `directory_category` (
  `dir_cat_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `tax_id` bigint(20) NOT NULL,
  `dir_cat_active` int(1) DEFAULT '0',
  PRIMARY KEY (`dir_cat_id`),
  KEY `dir_id` (`dir_id`),
  CONSTRAINT `DC_DIR_ID` FOREIGN KEY (`dir_id`) REFERENCES `directory_data` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `directory_data` 

CREATE TABLE `directory_data` (
  `dir_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir_name` varchar(200) NOT NULL,
  `dir_slug` varchar(200) NOT NULL,
  `dir_branch` int(1) NOT NULL DEFAULT '0',
  `dir_active` int(1) NOT NULL DEFAULT '0',
  `dir_verified` int(1) NOT NULL DEFAULT '0',
  `dir_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` bigint(20) DEFAULT NULL,
  `dir_status` varchar(20) NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`dir_id`)
);

-- Table structure for table `directory_details` 

CREATE TABLE `directory_details` (
  `dir_d_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `dir_d_key` varchar(100) NOT NULL,
  `dir_d_value` longtext NOT NULL,
  `dir_d_label` varchar(100) DEFAULT NULL,
  `dir_d_active` int(1) DEFAULT '0',
  `dir_d_order` int(4) DEFAULT '0',
  PRIMARY KEY (`dir_d_id`),
  KEY `dir_id` (`dir_id`),
  CONSTRAINT `DIR_ID` FOREIGN KEY (`dir_id`) REFERENCES `directory_data` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `directory_location` 

CREATE TABLE `directory_location` (
  `dir_loc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `loc_id` bigint(20) NOT NULL,
  `dir_loc_primary` int(1) NOT NULL DEFAULT '0',
  `dir_loc_active` int(1) DEFAULT '0',
  PRIMARY KEY (`dir_loc_id`),
  KEY `dir_id` (`dir_id`),
  CONSTRAINT `DL_DIR_ID` FOREIGN KEY (`dir_id`) REFERENCES `directory_data` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `directory_media` 

CREATE TABLE `directory_media` (
  `dir_med_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `media_id` bigint(20) NOT NULL,
  `media_thumb` bigint(20) DEFAULT NULL,
  `media_type` varchar(10) DEFAULT NULL,
  `media_caption` text,
  `media_link` text,
  `media_name` varchar(100) DEFAULT NULL,
  `dir_med_group` varchar(100) DEFAULT NULL,
  `dir_med_order` int(10) DEFAULT '0',
  `dir_med_active` int(1) NOT NULL,
  PRIMARY KEY (`dir_med_id`),
  KEY `dir_id` (`dir_id`),
  CONSTRAINT `DM_DIR_ID` FOREIGN KEY (`dir_id`) REFERENCES `directory_data` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `directory_meta` 

CREATE TABLE `directory_meta` (
  `dir_m_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` longtext NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dir_m_id`),
  KEY `dir_id` (`dir_id`),
  CONSTRAINT `DME_DIR_ID` FOREIGN KEY (`dir_id`) REFERENCES `directory_data` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `directory_tags` 

CREATE TABLE `directory_tags` (
  `dir_tag_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `dir_tag_active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dir_tag_id`),
  KEY `dir_id` (`dir_id`,`tag_id`)
);

-- Table structure for table `locations` 

CREATE TABLE `locations` (
  `loc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `loc_name` varchar(100) NOT NULL,
  `loc_slug` varchar(200) NOT NULL,
  `loc_type` varchar(255) DEFAULT NULL,
  `loc_order` int(3) NOT NULL DEFAULT '0',
  `loc_active` int(1) DEFAULT '0',
  PRIMARY KEY (`loc_id`)
);

-- Table structure for table `locations_ancestors` 

CREATE TABLE `locations_ancestors` (
  `loc_anc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `loc_id` bigint(20) NOT NULL,
  `loc_parent` bigint(20) NOT NULL,
  `loc_anc_active` int(1) DEFAULT '0',
  PRIMARY KEY (`loc_anc_id`),
  KEY `loc_id` (`loc_id`),
  CONSTRAINT `LA_LOC_ID` FOREIGN KEY (`loc_id`) REFERENCES `locations` (`loc_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `locations_meta` 

CREATE TABLE `locations_meta` (
  `loc_m_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `loc_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext NOT NULL,
  `loc_m_active` int(1) DEFAULT '0',
  PRIMARY KEY (`loc_m_id`),
  KEY `loc_id` (`loc_id`),
  CONSTRAINT `LM_LOC_ID` FOREIGN KEY (`loc_id`) REFERENCES `locations` (`loc_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `media_uploads` 

CREATE TABLE `media_uploads` (
  `media_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `raw_name` varchar(255) DEFAULT NULL,
  `orig_name` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `is_image` int(1) DEFAULT NULL,
  `image_width` int(10) DEFAULT NULL,
  `image_height` int(10) DEFAULT NULL,
  `image_type` varchar(100) DEFAULT NULL,
  `image_size_str` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`media_id`)
);

-- Table structure for table `realestate_ancestors` 

CREATE TABLE `realestate_ancestors` (
  `re_anc_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `ancestor_id` bigint(20) NOT NULL,
  `re_anc_primary` int(1) NOT NULL DEFAULT '0',
  `re_anc_active` int(1) DEFAULT '0',
  PRIMARY KEY (`re_anc_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `REA_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `realestate_data` 

CREATE TABLE `realestate_data` (
  `re_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `re_title` varchar(255) NOT NULL,
  `re_slug` varchar(255) DEFAULT NULL,
  `re_type` varchar(10) DEFAULT 'IND',
  `re_active` int(1) DEFAULT '0',
  `re_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `re_parent` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `re_status` varchar(20) NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`re_id`)
);

-- Table structure for table `realestate_details` 

CREATE TABLE `realestate_details` (
  `re_d_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `re_d_key` varchar(100) NOT NULL,
  `re_d_value` longtext NOT NULL,
  `re_d_label` varchar(100) DEFAULT NULL,
  `re_d_active` int(1) DEFAULT '0',
  `re_d_order` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`re_d_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `RED_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `realestate_features` 

CREATE TABLE `realestate_features` (
  `re_f_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `attr_id` bigint(20) NOT NULL,
  `re_f_active` int(1) DEFAULT '0',
  PRIMARY KEY (`re_f_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `REF_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `realestate_location` 

CREATE TABLE `realestate_location` (
  `re_loc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `loc_id` bigint(20) NOT NULL,
  `re_loc_active` int(1) DEFAULT '0',
  PRIMARY KEY (`re_loc_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `REL_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `realestate_media` 

CREATE TABLE `realestate_media` (
  `re_med_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `media_id` bigint(20) NOT NULL,
  `media_thumb` bigint(20) DEFAULT NULL,
  `media_type` varchar(10) DEFAULT NULL,
  `media_caption` text,
  `media_link` text,
  `media_name` varchar(100) DEFAULT NULL,
  `re_med_group` varchar(100) DEFAULT NULL,
  `re_med_order` int(10) DEFAULT '0',
  `re_med_active` int(1) NOT NULL,
  PRIMARY KEY (`re_med_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `REMD_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `realestate_meta` 

CREATE TABLE `realestate_meta` (
  `re_m_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext NOT NULL,
  `re_m_active` int(1) DEFAULT '0',
  PRIMARY KEY (`re_m_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `REMT_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `realestate_permissions` 

CREATE TABLE `realestate_permissions` (
  `re_per_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `access_level` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`re_per_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `REP_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `realestate_types` 

CREATE TABLE `realestate_types` (
  `re_type_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `re_id` bigint(20) NOT NULL,
  `type_name` varchar(100) NOT NULL,
  `re_type_active` int(1) DEFAULT '0',
  PRIMARY KEY (`re_type_id`),
  KEY `re_id` (`re_id`),
  CONSTRAINT `RET_RE_ID_CASCADE` FOREIGN KEY (`re_id`) REFERENCES `realestate_data` (`re_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `settings` 

CREATE TABLE `settings` (
  `setting_id` int(20) NOT NULL AUTO_INCREMENT,
  `setting_group` varchar(50) NOT NULL,
  `setting_name` varchar(50) NOT NULL,
  `setting_description` varchar(200) NOT NULL,
  `setting_value` text NOT NULL,
  PRIMARY KEY (`setting_id`)
);

-- Table structure for table `tags` 

CREATE TABLE `tags` (
  `tag_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(200) NOT NULL,
  `tag_slug` varchar(200) NOT NULL,
  `tag_count` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tag_name` (`tag_name`,`tag_slug`)
);

-- Table structure for table `tasks_urls` 

CREATE TABLE `tasks_urls` (
  `tasks_url_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `page_id` bigint(20) NOT NULL,
  `page_name` varchar(200) NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `object_type` varchar(100) NOT NULL,
  `active` int(1) DEFAULT '0',
  PRIMARY KEY (`tasks_url_id`)
);

-- Table structure for table `tasks_verified` 

CREATE TABLE `tasks_verified` (
  `tasks_ver_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `tasks_url_id` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `object_type` varchar(100) NOT NULL,
  `post_url` text NOT NULL,
  `date_verified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tasks_ver_id`)
);

-- Table structure for table `taxonomies` 

CREATE TABLE `taxonomies` (
  `tax_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(200) NOT NULL,
  `tax_label` varchar(200) NOT NULL,
  `tax_type` varchar(100) NOT NULL,
  `tax_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tax_id`),
  UNIQUE KEY `tax_name` (`tax_name`)
);

-- Table structure for table `taxonomies_ancestors` 

CREATE TABLE `taxonomies_ancestors` (
  `tax_anc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tax_id` bigint(20) NOT NULL,
  `tax_parent` bigint(20) NOT NULL,
  `tax_anc_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tax_anc_id`)
);

-- Table structure for table `taxonomies_meta` 

CREATE TABLE `taxonomies_meta` (
  `tax_m_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tax_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext NOT NULL,
  `tax_m_active` int(1) DEFAULT '0',
  PRIMARY KEY (`tax_m_id`),
  KEY `tax_id` (`tax_id`)
);

-- Table structure for table `users` 

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `api` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `verified` int(1) DEFAULT '0',
  `link` varchar(255) DEFAULT NULL,
  `referrer` bigint(20) DEFAULT NULL,
  `add_points` int(1) DEFAULT '0',
  `date_joined` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL,
  `manager` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
);

-- Table structure for table `users_bookmarks` 

CREATE TABLE `users_bookmarks` (
  `bookmark_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `object_type` varchar(100) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bookmark_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `UB_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `users_fb_friends` 

CREATE TABLE `users_fb_friends` (
  `ufbf_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `friend_id` bigint(20) NOT NULL,
  PRIMARY KEY (`ufbf_id`),
  KEY `user_id` (`user_id`)
);

-- Table structure for table `users_meta` 

CREATE TABLE `users_meta` (
  `user_meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext NOT NULL,
  `user_meta_active` int(1) DEFAULT '0',
  PRIMARY KEY (`user_meta_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `users_permissions` 

CREATE TABLE `users_permissions` (
  `permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `permission` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `UP_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `users_points` 

CREATE TABLE `users_points` (
  `points_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `object_type` varchar(100) NOT NULL,
  `points_type` varchar(100) NOT NULL,
  `points_credited` int(10) NOT NULL DEFAULT '0',
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `points_claimed` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`points_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `UPO_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `users_points_withdrawals` 

CREATE TABLE `users_points_withdrawals` (
  `withdrawal_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `date_withdrawn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `points_withdrawn` int(10) NOT NULL,
  `withdrawal_reason` varchar(255) NOT NULL,
  PRIMARY KEY (`withdrawal_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `UPOW_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Table structure for table `users_sessions` 

CREATE TABLE `users_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
);

-- Table structure for table `users_shares` 

CREATE TABLE `users_shares` (
  `share_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `object_type` varchar(100) NOT NULL,
  `fb_post_id` bigint(20) NOT NULL,
  `likes` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`share_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `US_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

